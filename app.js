var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var dbManager = require('./app/dbManager')

var apicache  = require('apicache')



var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.disable('view cache');
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));



app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({ extended: true,limit: '50mb' }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


//assuming app is express Object.

app.set('view engine', 'jade');


// session.
var flash    = require('connect-flash');
var express  = require('express');
var session  = require('express-session');





var MySQLStore = require('express-mysql-session')(session);

var fs = require('fs');
var data =  fs.readFileSync('bin/settings.conf');
var settings = JSON.parse(data);
var dbSettings = settings.dataBase;

var options = {
    host: dbSettings.host,
    port: dbSettings.port,
    user: dbSettings.user,
    password: dbSettings.password,
    database: dbSettings.database,
    checkExpirationInterval: 900000,// How frequently expired sessions will be cleared; milliseconds.
    expiration: 86400000,// The maximum age of a valid session; milliseconds.
    createDatabaseTable: true,// Whether or not to create the sessions database table, if one does not already exist.
    connectionLimit: 1,// Number of connections when creating a connection pool
    schema: {
        tableName: 'sessions',
        columnNames: {
            session_id: 'session_id',
            expires: 'expires',
            data: 'data'
        }
    }
};

var sessionStore = new MySQLStore(options);

app.use(session({
    key: 'session_cookie_nameadsfab234adsf',
    secret: 'vidyapathaisalwaysrunning',
    store: sessionStore,
    resave: true,
    saveUninitialized: false
} )); // session secret

var passport = require('passport');
require('./app/passport')(passport); // pass passport for configuration


app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions

app.use(flash()); // use connect-flash for flash messages stored in session

//set routing
var index = require('./routes/index')(app,passport,apicache);
app.use('/', index);

var api = require('./routes/api')(app,passport,apicache);
app.use('/api', api);

var admin = require('./routes/admin')(app,passport,apicache);
app.use('/admin', admin );

var pushes = require('./routes/pushes')(app,passport,apicache);
app.use('/admin/pushes', pushes );

var feedback = require('./routes/feedback')(app,passport,apicache);
app.use('/api/feedback', feedback );

var pushSheduler = require('./app/pushSheduler');
pushSheduler.run();

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});


// Initialize Passport and restore authentication state, if any, from the


// app.use(passport.session());

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
