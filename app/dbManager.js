/**
 * Created by v.Triputin on 01.09.2017.
 */






var fs = require('fs');
var data =  fs.readFileSync('bin/settings.conf');
var settings = JSON.parse(data);
var dbSettings = settings.dataBase;


var mysql = require('promise-mysql');
var pool  = mysql.createPool(dbSettings);

// var dbquery = function (query, func){
//
//         return pool.getConnection(function (err, connection) {
//             // Use the connection
//             try {
//             connection.query(query, function (error, results) {
//
//                 connection.release();
//                 func(error, results);
//
//
//                 // And done with the connection.
//
//
//                 // Handle error after the release.
//                 // if (error) throw error;
//
//                 // Don't use the connection here, it has been returned to the pool.
//             });
//             } catch (err1){
//                 try {
//                     connection.release();
//                 }catch(err) {}
//
//                 func(err1);
//
//
//             }
//         });
//
//
// };


var dbquery = function (query, func){
    pool.query(query, false,func);
};
var dbparamquery = function (query,param, func){
    pool.query(query, param,func);
};



var dbqueryAsync = async function (query){
    return new Promise ((relese,reject)=> {
        pool.query(query, false, (err, data) => {
            if (err) {
                reject(err)
            } else {
                relese(data);
            }
        });
    })

};


var dbparamqueryAsync = async function (query,param){
    return new Promise ((relese,reject)=> {
        pool.query(query, param, (err, data) => {
            if (err) {
                reject(err)
            } else {
                relese(data);
            }
        });

    })
};



// var dbqueryAsync = async function (query){
//     return new Promise ((relese,reject)=> {
//
//
//         pool.getConnection(function (err, connection) {
//             // Use the connection
//             try {
//                 connection.query(query, function (error, results) {
//
//                     // And done with the connection.
//                     if (error){
//                         connection.release();
//                         reject(error)
//                     }else{
//                         connection.release();
//                         relese(results);
//                     }
//
//                     // Handle error after the release.
//                     // if (error) throw error;
//
//                     // Don't use the connection here, it has been returned to the pool.
//                 });
//             } catch (err1) {
//                 try {
//                     connection.release();
//                 }catch(err) {}
//                 reject(err1)
//             }
//         });
//     })
//
// };

// var dbqueryAsync = async function (query){
//     return new Promise ((relese,reject)=> {
//
//
//         pool.getConnection(function (err, connection) {
//             // Use the connection
//             try {
//                 connection.query(query, function (error, results) {
//
//                     // And done with the connection.
//                     if (error){
//                         connection.release();
//                         reject(error)
//                     }else{
//                         connection.release();
//                         relese(results);
//                     }
//
//                     // Handle error after the release.
//                     // if (error) throw error;
//
//                     // Don't use the connection here, it has been returned to the pool.
//                 });
//             } catch (err1) {
//                 try {
//                     connection.release();
//                 }catch(err) {}
//                 reject(err1)
//             }
//         });
//     })
//
// };





// var dbparamquery = function (query,param, func){
//
//     return pool.getConnection(function(err, connection) {
//         // Use the connection
//         try {
//             connection.query(query,param, function (error, results) {
//                 // And done with the connection.
//                 connection.release();
//                 func(error,results);
//
//
//
//                 // Handle error after the release.
//                 // if (error) throw error;
//
//                 // Don't use the connection here, it has been returned to the pool.
//             });
//         } catch (err1){
//             try {
//                 connection.release();
//             }catch(err) {}
//             func(err1);
//
//         }
//     });
//
//
// };

exports.dbparamquery = dbparamquery;
exports.dbquery = dbquery;
exports.dbqueryAsync = dbqueryAsync;
exports.dbparamqueryAsync = dbparamqueryAsync;
var DBManager = function (){

};



exports.addStory = function (story,callback) {
    console.log(story.availableAt)
    const newstory = {
        name: story.name,
        image : story.image,
        availableAt : story.availableAt,
        hidden : story.hidden,
        tags : JSON.stringify(story.tags),
        minuserlevel : story.minuserlevel,
        backgroundcolor:story.backgroundcolor,
        foregroundcolor:story.foregroundcolor
    };

    dbparamquery('INSERT INTO stories SET ?', newstory, callback)
};

exports.addEpisode = function (episode,callback) {

    var text = {
       people: episode.people,
        messages: episode.messages
    };
    const newepisode= {
        storyId: episode.storyId,
        number : episode.number,
        description : episode.description,
        textjson : JSON.stringify(text),
        availableAt : episode.availableAt
    };

    //console.log(newepisode.textjson);
    dbparamquery('INSERT INTO episodes SET ?', newepisode, callback)
};

exports.getStories = function(callback)
{

            var sql = "SELECT  * FROM stories s WHERE s.hidden != 1";
         return   dbquery(sql, callback);

}

exports.getStoriesAllForUsers = function(locale,isTest,version,callback)
{
    var isTestData;
    if (isTest==1){
        isTestData = "') ORDER BY s.rank DESC, s.availableAt DESC;"
    }else{
    isTestData = "' AND isTest = 0) ORDER BY s.rank DESC, s.availableAt DESC;"
    }
    var sql = "";
    if (version>=19){
        sql ="SELECT s.id, s.name, s.image, s.tags, s.theme, s.minuserlevel, s.backgroundcolor,s.branching, s.isNew, s.foregroundcolor FROM spookytalks.stories s where (hidden = 0 AND locale='" + locale + isTestData;
    }else{
        sql ="SELECT s.id, s.name, s.image, s.tags, s.theme, s.minuserlevel, s.backgroundcolor,s.branching, s.isNew, s.foregroundcolor FROM spookytalks.stories s where (hidden = 0 AND branching = 0 AND locale='" + locale + isTestData;
    }

    //console.log(sql);
    return   dbquery(sql, callback);

}



exports.getSroriesEpisodesbylocale = function(storyId,locale,isTest,version,callback)
{
    var isTestData;
    if (isTest==1){
        isTestData = "') ORDER BY id DESC;"
    }else{
        isTestData = "' AND s.isTest = 0) ORDER BY id DESC;"
    }


    var sql = "";
    if (version>=19){
        sql = "SELECT ep.storyId,ep.id,ep.number,ep.description,ep.availableAt FROM episodes ep " +
            "JOIN stories s ON ep.storyId=s.id WHERE (s.hidden = 0 AND s.locale='" + locale + isTestData;
    }else{
        sql = "SELECT ep.storyId,ep.id,ep.number,ep.description,ep.availableAt FROM episodes ep " +
            "JOIN stories s ON ep.storyId=s.id WHERE (s.hidden = 0 AND branching = 0 AND s.locale='" + locale + isTestData;
    }

    return dbquery(sql, callback)

};

exports.getSroriesEpisodes = function(storyId,callback)
{

    var sql = "SELECT ep.storyId,ep.id,ep.number,ep.description,ep.availableAt FROM episodes ep " +
        "JOIN stories s ON ep.storyId=s.id WHERE s.hidden!=1";


    return dbquery(sql, callback)

};


exports.getEpisodesByWord = function(word,callback)
{

    var sql = "SELECT ep.storyId, ep.id,  ep.number, ep.description, ep.availableAt  FROM episodes ep " +
        "JOIN stories s ON s.id = ep.storyId WHERE ep.textjson like \"%"+word+"%\";"

    return dbquery(sql, callback)

}

exports.getSroriesEpisodesAll = function(storyId,callback)
{

    var sql = "SELECT ep.storyId,ep.id,ep.number,ep.description,ep.availableAt,ep.started_ios,ep.ended_ios FROM episodes ep " +
        "JOIN stories s ON ep.storyId=s.id";


    return dbquery(sql, callback)

}

exports.getEpisode = function(episodeId,callback)
{

    var sql = "SELECT ep.textjson FROM episodes ep  WHERE ep.id="+episodeId+ " LIMIT 1";


    return dbquery(sql, callback)


}


exports.registerUser = function(user,callback)
{

    var query = 'INSERT INTO spookytalks.users SET ?';
    var param = user;

    pool.getConnection(function(err, connection) {
        // Use the connection
        connection.query(query,param, function (error, results, fields) {

            if (error )
            {
                callback(error,results,fields);
            }
            else
            {
                callback(error,results,fields)
            }

            // And done with the connection.
            connection.release();

            // Handle error after the release.
            // if (error) throw error;

            // Don't use the connection here, it has been returned to the pool.
        });
    });
}

exports.updateUser = function(userId,userData,callback)
{
    var query = 'UPDATE users SET ? WHERE id = ?';
    var param = userData;
    dbparamquery(query, [param,userId] , callback)
};


exports.getUser = function(userId,callback)
{
    var query = "SELECT * FROM users  WHERE id = ? LIMIT 1";
    dbparamquery(query,userId, callback)
};

exports.getById = function(name,id,callback){
    var query = "SELECT * FROM " + name + "  WHERE id = ? LIMIT 1";
    dbparamquery(query,id, callback)
};

exports.getByPropertiesAsObject = function(name,properties,callback){

    var query = "SELECT * FROM " + name + "  WHERE";
    var keys = Object.keys(properties);
    for (var index in keys){
        if (index == 0){
            query = query +" " + keys[index] + " = " + "'" + properties[keys[index]] + "'";
            continue
        }
        query = query +" AND " + keys[index] + " = " + "'" + properties[keys[index]]  + "'";
    }

    dbquery(query, callback);
};

exports.getByUsersPropertiesAsObjectForPush = function(properties,callback){

    var query = "SELECT * FROM users WHERE";
    var keys = Object.keys(properties);
    for (var index in keys){
        if (index == 0){
            query = query +" " + keys[index] + " = " + "'" + properties[keys[index]] + "'";
            continue
        }
        query = query +" AND " + keys[index] + " = " + "'" + properties[keys[index]]  + "' AND pushToken IS NOT NULL";
    }

    dbquery(query, callback);
};

exports.getAll = function(name,callback){
    var query = "SELECT * FROM " + name;
    dbquery(query, callback)
};

exports.update = function(name,id,params,callback){
    var query = 'UPDATE ' + name + ' SET ? WHERE id = ?';
    dbparamquery(query, [params,id] , callback)
};

exports.remove = function(name,id,callback){

    var query = 'DELETE FROM ' + name + ' WHERE id = ?';
    dbparamquery(query, id , callback)
};


exports.add = function(name,params,callback){

    var query = 'INSERT INTO '+name+' SET ?';

    dbparamquery(query,params, callback);
};

exports.getByParam = function(name,param,callback){

    var query = "SELECT * FROM " + name + "  WHERE ?";

    dbparamquery(query,param, callback);
};


exports.test = function(callback){
    var query = "INSERT INTO scenaries set payoptions = 1; SELECT LAST_INSERT_ID();"
    dbquery(query, callback)
};
//module.exports = DBManager;