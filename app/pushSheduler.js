var schedule = require('node-schedule');

var ps = require("./controllers/pushes");
var Pushes = new ps();

var us = require("./controllers/users");
var Users = new us();

var ids = [
    "bbf7a990-d364-11e7-9c5d-43aeb7ba4d6c",
    "7faa7360-c93b-11e7-8e1f-a54f3056fefb",
    "f0b4db00-c93a-11e7-82bd-9f6a3275906f",
    "de40bc00-c93a-11e7-82bd-9f6a3275906f"
]

async function test1() {
    for(const id of ids){
        try {
            var data =await Pushes.sendPushesToUserById({
                    title: "title",
                    body: "body",
                    storyId: 322,
                    id: id
                },
                id
            );
            console.log(data)
        }catch (err){
            console.log(err)
        }
    }
}


async function test() {

        try {
            await Pushes.sendPushesToUsersWhichGetFromDB({
                    title: "title",
                    body: "body",
                    storyId: 322,

                }

            );

        }catch (err){
            console.log(err)
        }

}




module.exports.run = function(){
//send pushes
    schedule.scheduleJob('0 * * * *', function(){
        var d = new Date();
        console.log(d.toISOString());
        Pushes.sendPushes();

    });

//verify users
    if (process.env.NODE_ENV == 'production') {
        schedule.scheduleJob('0 * * *', function () {
            var d = new Date();
            console.log('----------Verify All Users schedule-------------');
            console.log(d.toISOString());
            Users.verifyAllUsers();
        });
    }
};