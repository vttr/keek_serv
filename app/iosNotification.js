var apn = require('apn');

var fs = require('fs');
var data = fs.readFileSync('bin/settings.conf');
var settings = JSON.parse(data);
var certPath;
if (settings.production){
    certPath = 'bin/pushKey.p8'
}else{
    certPath = 'bin/pushKey.p8'
}


var production = process.env.NODE_ENV === 'production'? true : false;

var options = {
    token: {
        key: certPath,
        keyId: "G8HN8L2HZ4",
        teamId: "LX3UWM36VY"
    },
    production: production
};

var apnProvider = new apn.Provider(options);


var sendPush = async function (params, pushToken)
{
    var note = new apn.Notification();
    note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
    note.badge = 1;

    if (params.storyId){
        note.payload = {
            "id":params.storyId
        };
    }

    note.alert = {
        title: params.title,
        body: params.body
    };

    note.topic = "com.logic-way.dafuq";


    var result = await apnProvider.send(note, pushToken);

//console.log(result);
    return result


};

module.exports.sendPush = sendPush;
//	sendPush("46c6b8ff2e25aeba33f79cdc65ece90fa39e5df379e5ab9068047b58751de451", "test push", i.toString());


//}

//var end = new Date();
//console.log(end.getTime() - start.getTime())