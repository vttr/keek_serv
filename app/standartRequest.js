module.exports = function(res,data,err,message) {
    if (err === undefined) {
        err = false;
    }
    if (message === undefined) {
        message = "";
    }
    var errData = {
        error: err,
        data: data,
        message: message
    };

    if (err == false)
    {
        res.send(data)
    }else{
        res.status(400);
        res.send(errData)
    }
};