var dbObject = require("./dbObject");
var dbManager = require("../dbManager");
var standartRequest = require("../standartRequest");
var apicache  = require('apicache')
var Episodes = function () {
    this.name = "episodes";
};

var extend = function(Child, Parent) {
    var F = function() { };
    F.prototype = Parent.prototype;
    Child.prototype = new F();
    Child.prototype.constructor = Child;
    Child.superclass = Parent.prototype
};
extend(Episodes, dbObject);

Episodes.prototype.saveEpisode = function(episodenum,episodeid,storyid)
{
    dbManager.getEpisode(episodeid,function (err, resp) {
        if (err !== null) {
            (standartRequest(self,null, true,  err));
            return
        }

        if (resp.length == 0) {
            (standartRequest(self,null, true, "not found"));
            return
        }
        console.log(storyid , episodenum);

        var episode = JSON.parse(decodeURI(resp[0].textjson))

        for (var messageIndex in episode.messages)
        {
            var message = episode.messages[messageIndex]
            if (message.image) {
                var startImagePath = path.resolve(__dirname, '../public/content/images/'+ storyid + "/" + message.image);

                var endImagePath = path.resolve(__dirname, '../public/content/ExportStory/' + storyid + "/" + message.image);
                helpers.copyImage(startImagePath, endImagePath);
            }
            if (message.obscuredImage) {
                var startImagePath = path.resolve(__dirname, '../public/content/images/'+ storyid + "/"+ message.obscuredImage);

                var endImagePath = path.resolve(__dirname, '../public/content/ExportStory/' + storyid + "/" + message.obscuredImage);
                helpers.copyImage(startImagePath, endImagePath);
            }
        }

        fs.writeFile(path.resolve(__dirname, '../public/content/ExportStory/' + storyid + '/' + episodenum + '.json'), JSON.stringify(episode, null, "\t"), function(err){})

    });
}


Episodes.prototype.getById = function (req, res, next)
{
    var self = res;
    var self = res;
    var reqEpisode = req.body;
    console.log(req.body)
    if (reqEpisode.id===undefined)
    {
        (standartRequest(res,null,true,"invalid Json"));
        return
    }

    dbManager.getEpisode(reqEpisode.id,function (err, resp) {
        if (err !== null) {
            (standartRequest(self,null, true,  err));
            return
        }

        if (resp.length == 0) {
            (standartRequest(self,null, true, "not found"));
            return
        }
        //console.log(decodeURIComponent(resp[0].textjson));
        res.setHeader('Content-Type', 'application/json');
        standartRequest(res,resp[0].textjson)
    });

};


Episodes.prototype.getByIdGet = function (req, res, next)
{
    var self = res;
    var reqEpisode = req.query;
    if (reqEpisode.id===undefined)
    {
        (standartRequest(res,null,true,"invalid Json"));
        return
    }

    var query = "SELECT ep.textjson FROM episodes ep  WHERE (ep.id="+reqEpisode.id+ ") LIMIT 1";
    dbManager.dbquery(query,function (err, resp) {
        if (err !== null) {
            (standartRequest(self,null, true,  err));
            return
        }

        if (resp.length == 0) {
            (standartRequest(self,null, true, "not found"));
            return
        }
        //console.log(decodeURIComponent(resp[0].textjson));
        res.setHeader('Content-Type', 'application/json');
        standartRequest(res,resp[0].textjson)
    });

};

Episodes.prototype.add = function (req, res, next)
{

    var self = res;
    var reqEpisode = req.body;
    if (!isObject(reqEpisode))
    {

        (standartRequest(res,null,true,"invalid Json"));
        return
    }

    console.log(reqEpisode.storyId);
    reqEpisode.availableAt = new Date().toMysqlFormat();
    dbManager.addEpisode(reqEpisode,function (err, resp) {
        if (err !== null) {
            console.log( reqEpisode)
            (standartRequest(self,null, true, "invalid Json: " + err));
            return
        }


        var txt = "" + resp.insertId;
        (standartRequest(self,txt));
        return;

    })

};

Episodes.prototype.startedIos = function (req, res, next)
{
    if (process.env.NODE_ENV!=='production'){
        res.send("ok");
        return
    }
    var episodeId= req.body.id;
    if (!episodeId)
    {
        (standartRequest(res,null,true,"invalid id"));
        return
    }

    console.log(episodeId);
    var query = "UPDATE episodes SET started_ios = started_ios + 1 WHERE id="+episodeId+";";
    dbManager.dbquery(query,function (err, resp) {
        if (err !== null) {
            console.log( reqEpisode);
            standartRequest(res,null, err);
            return
        }

        standartRequest(res,"ok");
        return;
    })
};


Episodes.prototype.endedIos = function (req, res, next)
{
    if (process.env.NODE_ENV!=='production'){
        res.send("ok");
        return
    }
    var episodeId= req.body.id;
    if (!episodeId)
    {
        (standartRequest(res,null,true,"invalid id"));
        return
    }

    console.log(episodeId);
    var query = "UPDATE episodes SET ended_ios = ended_ios + 1 WHERE id="+episodeId+";";
    dbManager.dbquery(query,function (err, resp) {
        if (err !== null) {
            console.log( reqEpisode);
            standartRequest(res,null, err);
            return
        }

        standartRequest(res,"ok");
        return;
    })
};

Episodes.prototype.recalculateNewLinesToMessageBiId= function(id){
    var self = this;
    try {
        self.getByIdDB(id,function (err,reqEpisode) {
            if (err !== null) {
                console.log( reqEpisode);
                return
            }
            var episode;
            if(reqEpisode.length > 0) {
                episode = self.recalculateNewLinesToMessage(reqEpisode[0])
            }else{
                return
            }
            delete episode.id;
            var data = {
                id:id,
                data:episode
            };
            self.updateDB(data,function (err,data1) {

            })



        })

    } catch (err) {
        console.log(err)
    }
};


Episodes.prototype.recalculateNewLinesToMessage = function(episode){
    var self = this;

        if (episode) {

            try {
                episode.body = JSON.parse(episode.textjson);
                delete episode.body._id;
                delete episode.body.storyId;

                var newMessages = [];
                for (var messageIndex in episode.body.messages) {

                    var message = episode.body.messages[messageIndex];
                    if (!message.body) {
                        newMessages.push(message);
                        continue;
                    }

                    while (message.body.indexOf("\n") != -1) {

                        var newMessageText = message.body.substr(0, message.body.indexOf("\n"));
                        message.body = message.body.substr(message.body.indexOf("\n")+1, message.body.length);
                        var newMessage = {};
                        if (message.personId !== undefined) {
                            newMessage.personId = message.personId;
                        }
                        if (message.typing !== undefined) {
                            newMessage.typing = message.typing;
                        }
                        message.typing = false;
                        newMessage.body = newMessageText;
                        newMessages.push(newMessage)
                    }

                    newMessages.push(message)

                }

                episode.body.messages = newMessages;
                episode.textjson = JSON.stringify(episode.body);
                delete episode.body

            } catch (err) {
              console.log(err)
            }
        }
return episode;
};

module.exports = Episodes;
