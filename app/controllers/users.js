var dbObject = require("./dbObject");
var dbManager = require("../dbManager");
var standartRequest = require("../standartRequest");
var Purchases = require("../purchases");
const path = require('path');

const AppsFlyerService = require('../appsFlyerService').AppsFlyerService;
const appsFlyerService = new AppsFlyerService();

const AppsFlyerData = require('../appsFlyerService').AppsFlyerData;


const EventValue = require('../appsFlyerService').EventValue;


var fs = require('fs');

var testFile = 'ABTest.json'
var settingsFile = 'settings.json'
if (process.env.NODE_ENV!=='production')
{
    testFile = 'test_ABTest.json'
    settingsFile = 'test_settings.json'
}


var ABTest = JSON.parse(fs.readFileSync(path.join(__dirname, '../settings/'+testFile), 'utf8'));
var Settings = JSON.parse(fs.readFileSync(path.join(__dirname, '../settings/'+settingsFile), 'utf8'));


const uuidv1 = require('uuid/v1');

function twoDigits(d) {
    if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
}

Date.prototype.toMysqlFormat2 = function() {
    return this.getFullYear() + "-" + twoDigits(1 + this.getMonth()) + "-" + twoDigits(this.getUTCDate()) + "T" + twoDigits(this.getUTCHours()) + ":" + twoDigits(this.getUTCMinutes()) + ":" + twoDigits(this.getUTCSeconds())+".000Z";
};

var getRandomArbitrary = function(min, max) {
    return Math.floor(Math.random() * (max  - min) + min);
};

var getTestData = function()
{
    var testData = {};
    for(var key in ABTest)
    {
        testData[key] = ABTest[key][getRandomArbitrary(0, ABTest[key].length)]
    }

    return testData
};

var Users = function () {
    this.name = "users";
};

var extend = function(Child, Parent) {
    var F = function() { };
    F.prototype = Parent.prototype;
    Child.prototype = new F();
    Child.prototype.constructor = Child;
    Child.superclass = Parent.prototype
};
extend(Users, dbObject);

Users.prototype.update = function (req, res)
{
    var id = req.body.id;
    var data = req.body.data;

    if (!data||!id)
    {
        (standartRequest(res,null, true, "invalid id or data"));
        return
    }
    var userData = data;

    dbManager.updateUser(id,userData, function (err, result) {
        if (err !== null) {
            (standartRequest(res,null, true, err));
            return
        }
        // console.log(result);
        else (standartRequest(res,"ok"));
    });
};




Users.prototype.adjustVariable = function (req, res)
{
    var adjustVariable = Settings.adjustVariable


       standartRequest(res,adjustVariable);

};

var iosVerification = require("../iosVerification")

var UsersVerificationStatus = false;
var UsersVerificationStatusCount = 0;
var UsersVerificationStatusEnded = 0;
Users.prototype.getVerificationStatus = function () {
    if (!UsersVerificationStatus) return false
    else return Math.round(UsersVerificationStatusEnded/UsersVerificationStatusCount*100)
};

Users.prototype.addAdjustData = async function(req,res,next) {
    let query = 'UPDATE users SET ? WHERE id = ?'
    let params = {
        adjustData:JSON.stringify(JSON.parse(req.body.adjustData))
    };
    await dbManager.dbparamqueryAsync(query,[params,req.body.id]);
    res.send("ok")
}


Users.prototype.verifyAllUsers = async function() {
    console.log("hello")
    var self = this;
    //var query = "select * FROM users where isFree=0;"
    var query = "select * FROM users where appleToken IS NOT NULL;"
    const data = await dbManager.dbqueryAsync (query);
    var free = 0, nonFree = 0;
    var len = 0;

    var promises = [];

    UsersVerificationStatus = true;
    UsersVerificationStatusCount = data.length;
    UsersVerificationStatusEnded = 0;
    const count = 0;
    for (var user of data){


        if(promises.length == 25){
            try{
                await Promise.all(promises);
                for (const promise of promises){
                    promise.then(data=>{
                        UsersVerificationStatusEnded++
                        data.isFree ? free++ : nonFree++
                    })

                }
            }catch (err){
                console.log(err)
            }

            promises = [];
            }


        const ret =  self.verifyUser(user);
        promises.push(ret)
        len++;

       // console.log(len,"/",data.length);
        //console.log(user.id);


    }


    for (const promise of promises){
        promise.then(data=>{
            UsersVerificationStatusEnded++
            data.isFree ? free++ : nonFree++
        })

    }
    await Promise.all(promises);
    UsersVerificationStatus = false;
    return{free : free, nonFree : nonFree}
}

Users.prototype.syncPurchases = async function (user,purchase,environment){
    let purchid = "Sync_"+purchase.productId
    if (purchase.isTrialPeriod === "true") purchid = `Trial_${purchid}`;
    if (environment==="Sandbox") purchid = `Sandbox_${purchid}`;
    let eventValue = new EventValue(Purchases[purchase.productId] ? Purchases[purchase.productId] : 0);
    let appsFlyerData = new AppsFlyerData(user.appsFlyerId,user.deviceId,purchid,eventValue);
    return appsFlyerService.onEvent(appsFlyerData);
}

Users.prototype.verifyUser = async function (user){
    let self = this;
    let data = await iosVerification.verifyAsync(user);

    let purchases = data.purchases.sort(function (a, b) {
       return (a.expirationDate - b.expirationDate)
    });

    if (purchases.length > 0){

        let isFree = purchases[purchases.length-1].ignoreExpired ? 0 :  purchases[purchases.length-1].expirationDate - Date.now()  > 0 ? 0 : 1;
        let subscriptionFrom = purchases[purchases.length-1].purchaseDate.substr(0, purchases[purchases.length-1].purchaseDate.length - 8);
        let subscriptionUntil = purchases[purchases.length-1].expiresDate.substr(0, purchases[purchases.length-1].expiresDate.length - 8);
        let productId = purchases[purchases.length-1].productId;
        let appleToken = user.appleToken;

        let paidSumm = 0;
        let purchaseCount = 0;

        if (data.environment!=="Sandbox") {
            for (var ind in purchases) {
                var purchase = purchases[ind];
                if (purchase.isTrialPeriod !== "true") {
                    var price = Purchases[purchase.productId];
                    purchaseCount += purchase.quantity;
                    if (!isNaN(price)||price !== undefined || purchase.quantity != undefined) {
                        paidSumm += price * purchase.quantity;
                    } else {
                        console.error("Undefined Price: " + purchase.productId)
                    }
                }
            }
        }

        let query =  'UPDATE users SET ? WHERE id = ?';
        let params = {
            subscriptionFrom:subscriptionFrom,
            subscriptionUntil:subscriptionUntil,
            productId: productId,
            appleToken: appleToken,
            isFree: isFree,
            paidSumm:paidSumm,
            purchaseCount:purchaseCount
        };
        await dbManager.dbparamqueryAsync(query,[params,user.id]);
        try{
            if (user.appsFlyerId!==null && purchases.length - user.syncPurchases > 0) {
                var promises = [];
                console.log(`sync ${user.syncPurchases-purchases.length}`);
                for (let ind = user.syncPurchases; ind < purchases.length; ind++) {
                    promises.push(self.syncPurchases(user, purchases[ind], data.environment))
                }
                await Promise.all(promises);
                console.log(`synced`)
                let query = 'UPDATE users SET ? WHERE id = ?';
                let params = {
                    syncPurchases: purchases.length
                };
                await dbManager.dbparamqueryAsync(query, [params, user.id]);
            }
        }catch (err){
        console.log(err)
        }
        //console.log("isFree " + isFree)
        return {isFree: isFree}
    }
    //console.log("isFree " + false)
    return{isFree:true}
};

Users.prototype.verifyStatus = function (req, res, next)
{
    var self = this;
    var id = req.body.id;
    console.log(req.body)
    if (!id)
    {
        (standartRequest(res,null, true, "invalid id"));
        return
    }

    //get info from db
    this.getByIdDB(id,function (err, result) {
        if (err !== null) {
            (standartRequest(res,null, true, err));
            console.log(result);
            return
        }
        else {

            if (result[0])
            {
                var subscription = result[0].subscriptionUntil;
                var isFree = result[0].isFree;
                var appleToken = result[0].appleToken;

                //if is Free
                if (isFree==0 && appleToken){
                    if (subscription){
                        var date = new Date(Date.parse(subscription))
                        if (Date.now()<date.getTime()){
                            standartRequest(res,{"isFree" : false});
                            return
                        } else {
                                self.verifyUser(result[0])
                                    .then((data)=>{
                                    res.send({isFree:data.isFree ? true : false})
                                        return;
                                },(error)=>{
                                    next(error)
                                        return;
                                    })
                        }
                    }
                }else{
                    standartRequest(res,{"isFree" : true});
                    return
                }
            }
            else {
                console.log("invalid id")
                standartRequest(res,null, true, "invalid id")
                return
            }

        }
    });
};


Users.prototype.updateActivity = function (req, res, next)
{
    var id = req.body.userId?req.body.userId:req.body.id;
    if (!id){
        res.send("invalidId");
        return;
    }
    var curDate = new Date().toMysqlFormat();
    var query = 'UPDATE users SET ? WHERE id = ?';

    dbManager.dbparamqueryAsync(query,[{lastActivity:curDate},id])
        .then(()=>res.send("ok"))
        .catch((err)=>res.send(err));

};

Users.prototype.setToken = function (req, res)
{
    var id = req.body.id;
    var token = req.body.appleToken;
    if (!token||!id)
    {
        (standartRequest(res,null, true, "invalid token or id"));
        return
    }
    var data = {
        appleToken : token
    };
    dbManager.updateUser(id,data, function (err, result) {
        if (err !== null) {
            (standartRequest(res,null, true, err));
            return
        }
        // console.log(result);
        else (standartRequest(res,"ok"));
    });
};


Users.prototype.registerFunc = function(data,callback){
    var curDate = new Date().toMysqlFormat();
    var id = uuidv1();
    var locale = data.locale;
    var timeZone = data.timeZone;

    if (locale == undefined)
    {
        locale = 'eng';
    }

    if (timeZone == undefined)
    {
        timeZone = 13;
    }

    var isTest;
    if (process.env.NODE_ENV=='production')
    {
        isTest=0
    }else{
        isTest=1
    }

    var idfa = null;
    if (data.idfa){
        idfa =  data.idfa
    }

    var country = data.country;

    var appsFlyerId = data.appsflyerId  ? data.appsflyerId  : null ;
    var deviceId = data.deviceId ? data.deviceId : null ;

    var userData = getTestData();
    var user = {
        id : id,
        scenarioId : null,
        testData: JSON.stringify(userData),
        register: curDate,
        lastActivity: curDate,
        isFree: true,
        appleToken: null,
        locale: locale,
        isTest:isTest,
        timeZone:timeZone,
        idfa:idfa,
        country:country,
        appsFlyerId:appsFlyerId,
        deviceId:deviceId

    };

    var aftercheck = function() {

        dbManager.registerUser(user, function (err, resp) {
            if (err !== null) {
                callback(err);
                return
            }

            callback(null,user);
            return

        });
    };

    if (idfa!="nil"){
        this.getByParamDB({idfa:idfa},function (err,results) {
            if (err !== null) {
                callback(err);
                return
            }else{
                if (results.length>0){
                    callback(null,results[0]);
                    return;
                }else{
                    aftercheck();
                }
            }
        })
    }else{
        aftercheck();
    }
};

Users.prototype.register = function (req, res,next)
{
    try {
        this.registerFunc(req.body,function (err,user) {
            if(err){
                next(err)
            }else{
                standartRequest(res,{id:user.id})
            }
        })
    }catch (err){
        next(err)
    }
};


Users.prototype.getSettingsData = function(){
    return Settings
};

Users.prototype.getTestData = function(){
    return ABTest
};

Users.prototype.setSettingsData = function(settingsData){
    Settings = settingsData;

    fs.writeFileSync(path.join(__dirname, '../settings/'+settingsFile),JSON.stringify(Settings));
    return Settings
};

Users.prototype.setTestData = function(testData){
    ABTest = testData;
    fs.writeFileSync(path.join(__dirname, '../settings/'+testFile),JSON.stringify(testData));
    return ABTest
};


Users.prototype.updateUser = function(req, res, next){

    //console.log(req.body.id)
    var id = req.body.id;
    if (!id){
        standartRequest(res,null, true, "invalid token or id")
        return
    }
    console.log(id);
    var userData = {};
    if (req.body.data.pushToken !== undefined){
        userData.pushToken = req.body.data.pushToken;
    };
    if (req.body.data.locale!==undefined){
        userData.locale = req.body.data.locale;
    };

    if (req.body.data.timeZone!==undefined){
        console.log(req.body.data.timeZone)
        userData.timeZone = req.body.data.timeZone;
    };

    if (req.body.data.storiesEnded!==undefined){
        console.log(req.body.data.storiesEnded)
        userData.storiesEnded = req.body.data.storiesEnded;
    };

    userData.lastActivity = new Date().toMysqlFormat();
    userData.isUnregistered = 0;

    this.updateDB({id:id, data:userData},function (err,data) {
        if (err !== null) {
            (standartRequest(res,null, true, err));
            return
        }else{
            res.send("ok");
            return
        }
    });

};


// Warn if overriding existing method
if(Array.prototype.equals)
    console.warn("Overriding existing Array.prototype.equals. Possible causes: New API defines the method, there's a framework conflict or you've got double inclusions in your code.");
// attach the .equals method to Array's prototype to call it on any array
Array.prototype.equals = function (array) {
    // if the other array is a falsy value, return
    if (!array)
        return false;

    // compare lengths - can save a lot of time
    if (this.length != array.length)
        return false;

    for (var i = 0, l=this.length; i < l; i++) {
        // Check if we have nested arrays
        if (this[i] instanceof Array && array[i] instanceof Array) {
            // recurse into the nested arrays
            if (!this[i].equals(array[i]))
                return false;
        }
        else if (this[i] != array[i]) {
            // Warning - two different object instances will never be equal: {x:20} != {x:20}
            return false;
        }
    }
    return true;
}
// Hide method from for-in loops
Object.defineProperty(Array.prototype, "equals", {enumerable: false});

Users.prototype.compareTestData = function (id, testData) {
    //check keys
    var isNeedToRecalculate = false;
    for (let key in testData) {
        if (ABTest[key]===undefined){
            delete testData[key]
            isNeedToRecalculate = true;
        }
    }
    for (let key in ABTest) {
        if (testData[key] === undefined) {
            testData[key] = ABTest[key][getRandomArbitrary(0, ABTest[key].length)]
            isNeedToRecalculate = true;
        } else {
            var isFound = false;
            //if array
            //console.log(typeof testData[key])
            if (Array.isArray(testData[key])) {

                //console.log(testData[key])
                for (var dataKey in ABTest[key]) {
                    if (ABTest[key][dataKey].equals(testData[key]) === true) {
                        isFound = true;
                        break;
                    }
                }
            } else if (typeof testData[key] === 'object') {

                for (let dataKey in ABTest[key]) {
                    //console.log(ABTest[key][dataKey], typeof  ABTest[key][dataKey])
                    let isNotCompire = false;
                    for (let idx in ABTest[key][dataKey]) {
                        //console.log(idx,"ok")
                        //console.log(ABTest[key][dataKey][idx],testData[key][idx]);
                        if (ABTest[key][dataKey][idx] !== testData[key][idx]) {
                            isNotCompire = true;
                        }

                    }
                    if (!isNotCompire) {
                        isFound = true;
                        break;
                    }
                }
            } else {
                //if not array
                if (ABTest[key].indexOf(testData[key]) != -1) {
                    isFound = true;
                    continue;
                }

            }
            if (isFound === false) {
                testData[key] = ABTest[key][getRandomArbitrary(0, ABTest[key].length)]
                isNeedToRecalculate = true;
            }
        }
    }
    if (isNeedToRecalculate === true) {
        console.log('recalculate');
        this.updateDB({id: id, data: {testData: JSON.stringify(testData)}}, function (err, data) {

        });
    }
    return testData

};


Users.prototype.getSettings = function (req, res, next)
{
    var self = this;
    var id = req.body.id;
    if (!id)
    {
        (standartRequest(res,null, true, "invalid id"));
        return
    }

    dbManager.getById(this.name,id,function (err, resp) {
        if (err !== null) {
            (standartRequest(res, null, true, err));
            return
        }
        if(!resp[0]){
            (standartRequest(res,null, true, "invalid id"));
            return
        }
        if(!resp[0].testData){
            (standartRequest(res,null, true, "invalid id"));
            return
        }

        var testData = JSON.parse(resp[0].testData);
        if (!testData){
            (standartRequest(res,null, true, "invalid id"));
            return
        }

        testData = self.compareTestData(id,testData);
        var result = {}
        Object.assign(result,Settings,testData);

        standartRequest(res,result
        );

    })
};

Users.prototype.getSettingsByUserIdAndTestData = function (userId,testData,callback) {
    if (!testData){
        console.error("invalidTestData");
        callback("invalidTestData");
        return
    }
    testData = this.compareTestData(userId,testData);
    var result = {};
    Object.assign(result,Settings,testData);
    callback(null,result)
};


var St = require("./stories");
var Stories = new St();

Users.prototype.updateUserDB = function(userData,callback)
{

    var updData = {};


    if (userData.locale)
    {
        updData.locale = userData.locale;
    }

    if (userData.timeZone)
    {
        updData.timeZone = userData.timeZone;
    }


    if (userData.idfa)
    {
        updData.idfa = userData.idfa;
    }

    if (userData.country )
    {
        updData.country  = userData.country ;
    }

    if (Object.keys(updData).length>0)
    {
        updData.isDeleted = 0;
        updData.lastActivity = new Date().toMysqlFormat();
        this.updateDB({id:userData.id,data:updData},callback)
    }else{
        callback(false,[])
    }


};

var saveDeeplinks = async(userId, data,isAdjust)=>{
    console.log("data", data)
    if (!data || data=="") {
        console.log("no deeplink")
       return
    }

    var time = new Date().toMysqlFormat();
    return dbManager.dbparamqueryAsync('INSERT INTO deeplinks SET ?', [{
        time:time,
        data:data,
        userId:userId,
        isAdjust:isAdjust
    }]);
};
Users.prototype.loadSettings = function (req, res, next)
{
    try {
        console.log(JSON.stringify(req.body));

        var version =  Number(req.body.version);
        var self = this;
        var id = req.body.id;

        if (!id) id = req.body.userId;
        if (id=='1' || id=='0' || id=='null' || id === null ){
            console.log("invalid id",req.body);
            id=false;
        }
        if (id){
            if (id.length==1){
                id =false;
            }
        }
        var locale = req.body.locale;
        var idfa = req.body.idfa;
        var deeplink = req.body.deeplink ? req.body.deeplink : "" ;
        var adjustInfo = req.body.adjustInfo;
        var isAdjust = 0;

        if (adjustInfo && adjustInfo!="") {
            deeplink = `${deeplink}[adjustInfo]${adjustInfo}`
            isAdjust = 1;
        }
        var adjustInfo = req.body.adjustInfo;
        if (!id) {
            this.registerFunc(req.body, function (err, user) {
                if (err) {
                    console.log(err)
                    next(err)
                } else {
                    saveDeeplinks(user.id,deeplink,isAdjust);
                    var testData = JSON.parse(user.testData);

                    ////
                    self.getSettingsByUserIdAndTestData(user.id,testData,function (err,settings) {
                        if (err){
                            console.log(err)
                            next(err)
                            return
                        }
                        ////
                        Stories.getStoriesLocale(!locale ? user.locale : locale,version, function (err, stories) {
                            if (err) {
                                console.log(err);
                                next(err);
                                return
                            }
                            console.log("ret user id:",user.id);
                            standartRequest(res, {
                                userId: user.id,
                                settings: settings,
                                stories: stories.stories,
                                newStories: stories.newStories,
                                isFree:user.isFree

                            })
                        })
                        ////

                    })
                    ////
                }
            });
        }else{
            self.getByIdDB(id,function (err, userData) {
                if (err){
                    console.log(err)
                    next(err)
                    return
                }

                if(userData.length==0){
                    console.log("invalid Id")
                    next("invalid Id")
                    return
                }

                var user = userData[0];
                saveDeeplinks(user.id,deeplink,isAdjust);
                //update user
                var userUpdData = req.body;
                userUpdData.id = id;
                self.updateUserDB(userUpdData,function (err,data) {
                    if (err){
                        console.error(err)
                    }
                });

                var testData = JSON.parse(user.testData);

                ////
                self.getSettingsByUserIdAndTestData(user.id,testData,function (err,settings) {
                    if (err){
                        next(err)
                        return
                    }
                    ////
                    Stories.getStoriesLocale(!locale ? user.locale : locale,version, function (err, stories) {
                        if (err) {
                            next(err)
                            return
                        }
                        console.log("ret user id:",user.id)
                        standartRequest(res, {
                            userId: user.id,
                            settings: settings,
                            stories: stories.stories,
                            newStories: stories.newStories,
                            isFree:user.isFree

                        })
                    })
                    ////

                })
                ////
            })
        }



    }catch (err){

        console.log(err)

        next(err)
    }
};

module.exports = Users;