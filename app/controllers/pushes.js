var dbObject = require("./dbObject");
var dbManager = require("../dbManager");
var standartRequest = require("../standartRequest");



function twoDigits(d) {
    if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
}

Date.prototype.toMysqlFormat2 = function() {
    return this.getFullYear() + "-" + twoDigits(1 + this.getMonth()) + "-" + twoDigits(this.getUTCDate()) + "T" + twoDigits(this.getUTCHours()) + ":" + twoDigits(this.getUTCMinutes()) + ":" + twoDigits(this.getUTCSeconds())+".000Z";
};

Date.prototype.toMysqlToday = function() {
    return this.getFullYear() + "-" + twoDigits(1 + this.getMonth()) + "-" + twoDigits(this.getUTCDate());
};

Date.prototype.toMysqlTomorrow = function() {
    var yesterday = this.getUTCDate();
    this.setDate(this.getUTCDate()+1);
    return this.getFullYear() + "-" + twoDigits(1 + this.getMonth()) + "-" + twoDigits(this.getUTCDate());
};


var Pushes = function () {
    this.name = "pushes";
};

var extend = function(Child, Parent) {
    var F = function() { };
    F.prototype = Parent.prototype;
    Child.prototype = new F();
    Child.prototype.constructor = Child;
    Child.superclass = Parent.prototype
};
extend(Pushes, dbObject);


Pushes.prototype.getToday = function(callback){
    var dateStart = new Date().toMysqlToday();
    var dateEnd = new Date().toMysqlTomorrow();

    var query = "select * from pushes "+
    "where (isSend=1 and pushTimeAt between'" + dateStart + "' and '" + dateEnd + "');"
    return dbManager.dbquery(query,callback);
};



Pushes.prototype.getMysqlDate = function(date) {
    return date.getUTCFullYear() + "-" + twoDigits(1 + date.getUTCMonth()) + "-" + twoDigits(date.getUTCDate() + " " + twoDigits(date.getUTCHours()));

};

Pushes.prototype.getDates = function() {
    var self=this;
    var next = new Date();
    var before = new Date();
    var current = new Date();
    next.setUTCHours(current.getUTCHours()+12);
    before.setUTCHours(current.getUTCHours()-12);
return[self.getMysqlDate(before),self.getMysqlDate(next)]
};

Pushes.prototype.getTodayPushes = async function(){

    return  new Promise((resolve , reject) => {
        var dates = this.getDates();
        var query = "select * from pushes " +
            "where (isSend=1 and pushTimeAt between'" + dates[0] + "' and '" + dates[1] + "');"
        dbManager.dbquery(query, (err, data) => {
            if (err) {
                reject(err);
                //console.log(err);
                return
            } else {

                resolve(data)
                return;

            }
        })

    })
};

// Pushes.prototype.getCurTimeZone = function(date){
//     console.log(date)
//     var  nullTimeHourse = new Date().getUTCHours();
//     if(Math.abs(date - nullTimeHourse)<13){
//         expectedTimeZone = date - nullTimeHourse;
//
//     }else return;
// };



Pushes.prototype.getTimeZone = function(date) {

    var d = new Date();
    d.setTime(Date.parse(date + " GMT"));
    var hours= d.getUTCHours() - new Date().getUTCHours();
    if(Math.abs(hours)<13){
        return hours
    }else return false;

};

Pushes.prototype.checkStatus = async function(data,ret){
    var self = this;


    if ( data.failed){

        if (data.failed.length>0){

            if (data.failed[0].status==="410")
            {

                self.updateUnregistered(data.failed[0].device);
            }
            ret.fail++;
            return
        }

    }

    if (data.sent) ret.success++;
    else ret.fail++;

    return;
};

Pushes.prototype.updateUnregistered  = async function(pushToken){
    var query = "UPDATE users SET isUnregistered=1 where pushToken='"+pushToken+"'";

    return  new Promise((resolve , reject) => {
        dbManager.dbquery(query,(err,data)=>{
            if (err){
                console.log("cantUpdate unregistered",err);
                reject(err);
                return
            }else{
                resolve();
            }
        })
    });
};

Pushes.prototype.sendPushesToUsersWhichGetFromDB = async function(pushData){
    var params ={};
    console.log(pushData)
    if (pushData.curTimeZone===undefined) return;
    var query = "SELECT * FROM users WHERE timeZone=" + pushData.curTimeZone;


    if (pushData.isFree !== 2){
        query += " and isFree=" + pushData.isFree
    }
    if (pushData.storyId){
        params.storyId = pushData.storyId;
        query += " and isDeleted=0"
    }
    if (pushData.isTest){
        query += " and isTest=" + pushData.isTest;
    }

        query += " and locale='" +pushData.locale +"'"

    //console.log(JSON.stringify(pushData),query);
    //var query = "SELECT pushToken FROM users WHERE id in ('bbf7a990-d364-11e7-9c5d-43aeb7ba4d6c' , '7faa7360-c93b-11e7-8e1f-a54f3056fefb','f0b4db00-c93a-11e7-82bd-9f6a3275906f','de40bc00-c93a-11e7-82bd-9f6a3275906f') GROUP BY pushToken;";
    console.log("query",query)
    params.title = pushData.title;
    params.body = pushData.body;
    var self = this;
    return  new Promise((resolve , reject) => {
    dbManager.dbquery(query,(err,data)=>{
        if (err){
            reject(err);
            return
        }else{

            resolve(self.sendPushesToUsers(params,data));
            //resolve(console.log(data.length));
        }
    })
    });
};

var iosNotify = require("../iosNotification");

Pushes.prototype.sendPushesToUsers = async function(pushParams,users){
    var promises = [];
    var self = this;
    var ret = {
        fail:0,
        success:0
    };

    for (var userInd in users){
            if(promises.length == 25) {
                try {
                    await Promise.all(promises);
                    for (const promise of promises) {
                        promise.then(function (data) {
                            self.checkStatus(data, ret);
                        })

                    }
            }catch (err){
            console.log(err);
        }
                promises = [];
            }
            promises.push(iosNotify.sendPush(pushParams,users[userInd].pushToken))
    }
    if(promises.length >0){
        try {
            await Promise.all(promises);
            for (const promise of promises){
                promise.then(function (data) {
                    self.checkStatus(data,ret);

                })

            }
        }catch (err){
            console.log(err);
        }

    }

    return ret;
};

var isTestServer = process.env.NODE_ENV === 'production'? 0 : 1;
Pushes.prototype.sendPushesToUserById = async function(pushParams,userId){
return  new Promise((resolve , reject) => {
    var query = "select * from users where id="+"'"+userId+"'";
    dbManager.dbquery(query,(err,data)=>{
        if (err){
            console.log(err);
           reject(err);

            return
        }else{
            if (data[0]){
                resolve(iosNotify.sendPush(pushParams,data[0].pushToken))
                return;
            }
            resolve("")
        }
    })
});



};

Pushes.prototype.sendPushes = async function(){
    var self = this;
    var pushes = await self.getTodayPushes()
        console.log("--------start send pushes----------");
        for (var pushIndex in pushes){
            var pushData = {
                curTimeZone : self.getTimeZone(pushes[pushIndex].pushTimeAt),
                isFree : pushes[pushIndex].isFree,
                title : pushes[pushIndex].title,
                body : pushes[pushIndex].body,
                storyId : pushes[pushIndex].storyId,
                locale : pushes[pushIndex].locale,
                isTest : pushes[pushIndex].isTest,
            };
            if (isTestServer!=pushData.isTest) continue;
        console.log("   \t---send Push---");
            var ret = await self.sendPushesToUsersWhichGetFromDB(pushData);


            console.log("   ",JSON.stringify(pushData));
            console.log("   success: " + ret.success);
            console.log("   fail: " + ret.fail +"\n\t---end send Push---");
        }
    console.log("--------end send pushes------------");

};



module.exports = Pushes;