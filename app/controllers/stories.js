var dbObject = require("./dbObject");
var dbManager = require("../dbManager");
var standartRequest = require("../standartRequest");
var Ep = require("./episodes");
var Episodes = new Ep();
var apicache  = require('apicache')
var Stories = function () {
    this.name = "stories";
};

var extend = function(Child, Parent) {
    var F = function() { };
    F.prototype = Parent.prototype;
    Child.prototype = new F();
    Child.prototype.constructor = Child;
    Child.superclass = Parent.prototype
};

extend(Stories, dbObject);



Stories.getStoryEpisodes = (storyId,callback)=>{

}

class storiesTemplate {
    constructor(){
        this.stories = [];
        this.newStories = [];
    }
};

let curRusStories = new storiesTemplate();

let curEngStories = new storiesTemplate();

let curRusStoriesBranch = new storiesTemplate();

let curEngStoriesBranch = new storiesTemplate();



Stories.prototype.refreshAll = function(){
    console.log("clear")
    curRusStories = new storiesTemplate();

    curEngStories = new storiesTemplate();

    curRusStoriesBranch = new storiesTemplate();

    curEngStoriesBranch = new storiesTemplate();
};

Stories.prototype.refreshAndSaveStories = function(locale,version,curVariable,callback){
    self =this;
    if (curVariable.stories.length==0 || process.env.NODE_ENV!=='production') {
        self.refreshStories(locale,version, function (err, stories) {
            if (err) {
                callback(err);
                return;
            }

            curVariable.stories = stories.stories;
            curVariable.newStories = stories.newStories;
            callback(null, curVariable)
        })
    }else callback(null,curVariable)
};

Stories.prototype.getStoriesLocale = function(locale,version,callback){
    var self = this;
if (locale=="rus")
   {
       if (version>=19){
           self.refreshAndSaveStories(locale, version, curRusStoriesBranch, callback)
       }else{
           self.refreshAndSaveStories(locale, version, curRusStories, callback)
       }
    }


    if (locale=="eng")
    {
        if (version>=19){
            self.refreshAndSaveStories(locale, version, curEngStoriesBranch, callback)
        }else{
            self.refreshAndSaveStories(locale, version, curEngStories, callback)
        }
    }
};


Stories.prototype.getNewStories = async function(locale,isTest,version,callback)
{
    var isTestData;
    if (isTest==1){
        isTestData = "') ORDER BY availableAt DESC;"
    }else{
        isTestData = "' AND isTest = 0) ORDER BY availableAt DESC;"
    }


    var sql = "";
    if (version>=19){
        sql = "SELECT id FROM stories WHERE (hidden = 0 AND locale='" + locale + isTestData;
    }else{
        sql = "SELECT id FROM stories WHERE (hidden = 0 AND branching = 0 AND locale='" + locale + isTestData;
    }

    return dbManager.dbqueryAsync(sql)

};


Stories.prototype.refreshStories = function(locale,version,callback){
    var isTest;
    if (process.env.NODE_ENV == 'production'){
        isTest = 0;
    }else{
        isTest = 1
    }
    let self=this;
    dbManager.getStoriesAllForUsers(locale,isTest,version,function (err, resp) {
        if (err !== null) {
            callback(err);
            return
        }
        if (resp.length==0)
        {
            callback(null,[]);
            return
        }

        //create dict
        var dict = {}
        for (var ind in resp) {
            resp[ind].episodes = [];
            dict[resp[ind].id] = ind
        }

        dbManager.getSroriesEpisodesbylocale(resp[ind].id, locale, isTest,version, function (err, result) {
            if (err !== null) {
                callback(err);
                return
            }
            // console.log(result);
            for (var ind in result) {
                var storyId = result[ind].storyId;
                delete result[ind].storyId;
                resp[dict[storyId]].episodes.push(result[ind]);
            }

            self.getNewStories(locale,isTest,version).then(
                (data)=>{

                    let newStories = data.map( function(item){ return item.id; } );
                    callback(null,{
                        stories:resp,
                        newStories:newStories
                    })
                },
                (error)=>{
                    callback(error);
                    return
                });


            });
        });
        return;
};

Stories.prototype.getAll = function (req, res,next)
{

    try{
        req.apicacheGroup = "getAll";
        console.log(JSON.stringify(req.query))
    var self = this;
    var locale = req.query.locale;

    if (locale) {
        var isTest;
        if (process.env.NODE_ENV == 'production'){
            isTest = 0;
        }else{
            isTest = 1
        }
        var version =  Number(req.query.version);
        self.getStoriesLocale(!locale ? user.locale : locale,version, function (err, stories) {
            if (err) {
                next(err)
                return
            }

           if (version<20){
               standartRequest(res, stories.stories)
           }else{
               standartRequest(res, {
                   stories: stories.stories,
                   newStories: stories.newStories,
               })
           }

        })
    }else {
        standartRequest(res, null, "notFound")
        return
    }
    }catch (err){
    next(err)
    }

};

const fs =require('fs')
const path =require('path')
const mkdirp = require('mkdirp');
const helpers = require('../helpers')
Stories.prototype.add = function (req, res, next)
{
    var self = res;
    var reqStory = req.body;
    console.log(reqStory.id);
    if (!isObject(reqStory))
    {
        (standartRequest(res,null,true,"invalid Json"));
        return
    }
    try{
    reqStory.availableAt = new Date().toMysqlFormat();
    dbManager.dbqueryAsync(`SELECT * FROM stories where name="${reqStory.name}";`).then(
        (data)=>{
            if(data.length==0){
                dbManager.addStory(reqStory,function (err, resp) {
                    if (err!==null)
                    {
                        (standartRequest(self,null,true,"invalid Json: "+err));
                        return
                    }

                    try
                    {


                        if (reqStory.image==undefined)
                        {
                            console.log("no image")
                            var txt = "" + resp.insertId;
                            (standartRequest(self,txt));
                            return
                        }



                        var startImagePath;
                        var endImagePath;
                        var idSection;

                        dbManager.dbqueryAsync('SELECT MAX(id) FROM stories;').then(
                            (data)=>{
                                let id = data[0]['MAX(id)']

                                if (reqStory.id != undefined) {
                                    idSection =  reqStory.id + "/"
                                }
                                else idSection="";

                                var txt = "" + resp.insertId;
                                (standartRequest(self,txt));
                                return;
                            })


                    }
                    catch (e) {(standartRequest(self,null,true,"invalid Image File: "+e)); return}


                })
            }else{
                standartRequest(self,{data:"skip"});
                return;
            }

        },(error)=>{
            console.log(error)
        })
    }catch (err){
        next(err);
    }

};

Stories.prototype.like = function (req, res, next)
{
 try {
    var storyId = req.body.storyId;
     if (process.env.NODE_ENV!=='production'){
         res.send("ok");
         return
     }
    if (!storyId)
    {
        (standartRequest(res,null,true,"invalid storyId"));
        return
    }

    var query = "UPDATE stories SET rating = rating + 1 WHERE id = '" + storyId +"'";
     dbManager.dbquery(query, (err,result)=>{
         if(err){
             next(err)
         }else{
             res.send("ok");
         }
     })


}catch (err){
     next(err);
}
};

Stories.prototype.rateStoryLike = function (req, res, next)
{
    try {
        if (process.env.NODE_ENV!=='production'){
            res.send("ok");
            return
        }
        var storyId = req.body.storyId;

        if (!storyId)
        {
            (standartRequest(res,null,true,"invalid storyId"));
            return
        }

        var query = "UPDATE stories SET rateStoryLike = rateStoryLike + 1 WHERE id = '" + storyId +"'";
        dbManager.dbquery(query, (err,result)=>{
            if(err){
                next(err)
            }else{
                res.send("ok");
    }
    })


    }catch (err){
        next(err);
    }
};

Stories.prototype.rateStoryDislike = function (req, res, next)
{
    try {
        var storyId = req.body.storyId;
        if (process.env.NODE_ENV!=='production'){
            res.send("ok");
            return
        }
        if (!storyId)
        {
            (standartRequest(res,null,true,"invalid storyId"));
            return
        }

        var query = "UPDATE stories SET rateStoryDislike = rateStoryDislike + 1 WHERE id = '" + storyId +"'";
        dbManager.dbquery(query, (err,result)=>{
            if(err){
                next(err)
            }else{
                res.send("ok");
    }
    })


    }catch (err){
        next(err);
    }
};

var Theme = require("../theme");
Stories.prototype.recalculateColors = function(storyId,callback){
    var self = this;
    try {
        if (storyId){

            var id = storyId;
            var isBlock = 0;
            self.getByIdDB(id,function (err,stories) {
                if (err !== null) {
                    callback (err);
                    return
                }

                if (stories.length>0)
                {
                    var story = stories[0];
                    Episodes.getByParamDB({storyId:id},function (err,episodes) {
                        if (err !== null) {
                            callback(err);
                            return
                        }
                        if (episodes.length>0) {

                            var theme = Theme[story.theme];
                            if (theme == undefined) theme= Theme.light;

                            var storyUpdateData = {
                                backgroundcolor : theme.backgroundcolor,
                                foregroundcolor : theme.foregroundcolor
                            };

                            self.updateDB({id:id,data:storyUpdateData},function (err,res) {

                            });

                            //convert all textjsons from episodes to objects
                            var checkEpisodes = [];
                            var people = [];


                            for (var epIndex in episodes)
                            {

                                var episode = episodes[epIndex];
                                try{
                                    episode.body = JSON.parse(episode.textjson);
                                    delete episode.body._id;
                                    delete episode.body.storyId;

                                    if(episode.body.people.length>0){

                                        for (var personIndex in episode.body.people){


                                            var person = episode.body.people[personIndex];
                                            if (person.personId){
                                                person.id = person.personId;
                                                delete person.personId;
                                            }
                                            var oldId = person.id;

                                            var result = people.filter(function( obj ) {
                                                return obj.name === person.name;
                                            });
                                            if(result.length==0){
                                                var curPersonIndex = people.length;
                                                var bubleIndex = curPersonIndex % theme.chatColors.length;
                                                var chatColors = theme.chatColors[bubleIndex];
                                                person.id = curPersonIndex;
                                                person.bubbleColor = chatColors.bubbleColor;
                                                person.textColor = chatColors.textColor;
                                                person.nameColor = chatColors.nameColor;
                                                person.left = chatColors.left;
                                                people.push(person)
                                            }else{
                                                person = result[0];
                                            }

                                            //change person id in messages

                                            var personID = person.id;
                                            episode.body.people[personIndex] = person;

                                            for (var messageIndex in episode.body.messages){

                                                if(episode.body.messages[messageIndex].personId === oldId && episode.body.messages[messageIndex].changed!=true){

                                                    episode.body.messages[messageIndex].personId = personID;
                                                    episode.body.messages[messageIndex].changed = true;
                                                }
                                            }

                                            if (episode.body.blocks){
                                                for (var blockIndex in episode.body.blocks) {
                                                    for (var messageIndex in episode.body.blocks[blockIndex].messages) {

                                                        if (episode.body.blocks[blockIndex].messages[messageIndex].personId === oldId && episode.body.blocks[blockIndex].messages[messageIndex].changed != true) {

                                                            episode.body.blocks[blockIndex].messages[messageIndex].personId = personID;
                                                            episode.body.blocks[blockIndex].messages[messageIndex].changed = true;
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        for (var messageIndex in episode.body.messages){
                                               delete episode.body.messages[messageIndex].changed
                                        }
                                        if (episode.body.blocks){
                                            if (episode.body.blocks.length>0){
                                                isBlock=1;
                                            }
                                            for (var blockIndex in episode.body.blocks) {
                                                for (var messageIndex in episode.body.blocks[blockIndex].messages) {
                                                        delete  episode.body.blocks[blockIndex].messages[messageIndex].changed
                                                    }
                                                }
                                            }

                                        checkEpisodes.push(episode);
                                    }

                                }catch (err){
                                    console.log(err)
                                }
                            }

                            //updateDB
                            if (story.branching!=isBlock){
                                self.updateDB({id : id,data:{
                                    branching:isBlock
                                }},function (err,data) {
                                    apicache.clear("getAll");
                                })
                            }


                            for (var epIndex in checkEpisodes)
                            {
                                var episode = checkEpisodes[epIndex];

                                id = episode.id;
                                var episodeUpdateData = {
                                    textjson : JSON.stringify(episode.body)
                                };

                                Episodes.updateDB({id:id,data:episodeUpdateData},function (err,data) {
                                    apicache.clear("/api/episodes/get?id="+id);

                                })
                            }

                            callback(null, "ok")
                        }else{
                            callback("notFound");
                            return
                        }


                    })

                }else{
                    callback("notFound");
                    return
                }
            })
        }else {
            callback("InvalidId");
            return
        }
    }catch (err){
        callback(err);
    }

};





module.exports = Stories;
