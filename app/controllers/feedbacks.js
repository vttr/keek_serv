var dbObject = require("./dbObject");
var dbManager = require("../dbManager");
var standartRequest = require("../standartRequest");

var Feedbacks = function () {
    this.name = "feedbacks";
};

var extend = function(Child, Parent) {
    var F = function() { };
    F.prototype = Parent.prototype;
    Child.prototype = new F();
    Child.prototype.constructor = Child;
    Child.superclass = Parent.prototype
};
extend(Feedbacks, dbObject);


module.exports = Feedbacks;