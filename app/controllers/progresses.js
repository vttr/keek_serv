/**
 * Created by v.Triputin on 15.02.2018.
 */
"use strict";
var dbObject = require("./dbObject");
var dbManager = require("../dbManager");
var standartRequest = require("../standartRequest");

var Progresses = function () {
    this.name = "progresses";
};

var extend = function (Child, Parent) {
    var F = function () {
    };
    F.prototype = Parent.prototype;
    Child.prototype = new F();
    Child.prototype.constructor = Child;
    Child.superclass = Parent.prototype;
};

extend(Progresses, dbObject);

Progresses.prototype.add = async (req, res, next) => {
    try {
        const date = new Date().toMysqlFormat();
        const params = req.body;

        const query = `INSERT INTO progresses (episodeId,userId,messageNumber,timer,date) VALUES(${params.episodeId},'${params.userId}',${params.messageNumber},${params.timer},'${date}') ON DUPLICATE KEY UPDATE ?`;

        const data = {
            messageNumber: params.messageNumber,
            timer: params.timer,
            date: date
        };

        await dbManager.dbparamqueryAsync(query, [data]);
        res.send("ok");
    } catch (err) {
        console.error(err);
        next(err);
    }

};
module.exports = Progresses;