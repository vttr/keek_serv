/**
 * Created by v.Triputin on 15.02.2018.
 */
var dbObject = require("./dbObject");
var dbManager = require("../dbManager");
var standartRequest = require("../standartRequest");

var Deeplinks = function () {
    this.name = "deeplinks";
};

var extend = function(Child, Parent) {
    var F = function() { };
    F.prototype = Parent.prototype;
    Child.prototype = new F();
    Child.prototype.constructor = Child;
    Child.superclass = Parent.prototype
};
extend(Deeplinks, dbObject);

module.exports = Deeplinks;