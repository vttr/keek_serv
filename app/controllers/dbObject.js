var dbManager = require("../dbManager");
var standartRequest = require("../standartRequest");

var BDObject = function () {
};
BDObject.prototype.getById = function (req, res, next) {
    try {
        var id = req.body.id;
        if (!id) {
            (standartRequest(res, null, true, "invalid id"));
            return
        }
        var callback = function (err, result) {
            if (err !== null) {
                (standartRequest(res, null, true, err));
                return
            }

            else {
                if (!result[0]) {
                    (standartRequest(res, null, true, "invalid id"));
                    return
                }
                (standartRequest(res, result));

            }
        };
        dbManager.getById(this.name, id, callback);
    } catch (err) {
        next(err)
    }

};


BDObject.prototype.getByIdDB = function (id, callback) {


    if (!id) {
        callback("invalid id");
        return
    }

    dbManager.getById(this.name, id, callback)
};


BDObject.prototype.getByPropertiesAsObjectDB = function (properties, callback) {

    if (!properties) {
        callback("invalid properties");
        return
    }

    dbManager.getByPropertiesAsObject(this.name, properties, callback)
};


BDObject.prototype.getByIdWithCallback = function (callback) {

    dbManager.getById(this.name, id, callback);
};

BDObject.prototype.getAll = function (req, res, next) {
    try {
        var callback = function (err, result) {
            if (err !== null) {
                (standartRequest(res, null, true, err));
                return
            }

            else {
                (standartRequest(res, result));

            }
        };
        dbManager.getAll(this.name, callback);
    } catch (err) {
        next(err)
    }
};


BDObject.prototype.getByParamDB = function (param, callback) {

    dbManager.getByParam(this.name, param, callback);
};


BDObject.prototype.getAllDB = function (callback) {

    dbManager.getAll(this.name, callback);
};




BDObject.prototype.update = function (req, res, next) {
    try {
        var id = req.body.id;
        var params = req.body.data;

        if (!params || !id) {
            (standartRequest(res, null, true, "invalid id or data"));
            return
        }


        var callback = function (err, result) {
            if (err !== null) {
                (standartRequest(res, null, true, err));
                return
            }

            else {
                (standartRequest(res, result));

            }
        };

        dbManager.update(this.name, id, params, callback)
    } catch (err) {
        next(err)
    }
};

BDObject.prototype.updateDB = function (data, callback) {

    var id = data.id;
    var params = data.data;

    if (!params || !id) {
        callback("invalid id or data");
        return
    }

    dbManager.update(this.name, id, params, callback)
};


BDObject.prototype.remove = function (req, res, next) {
    try {
        var id = req.body.id;

        if (!id) {
            (standartRequest(res, null, true, "invalid id"));
            return
        }

        var callback = function (err, result) {
            if (err !== null) {
                (standartRequest(res, null, true, err));
                return
            }

            else {
                (standartRequest(res, result));

            }
        };
        dbManager.remove(this.name, id, callback)
    } catch (err) {
        next(err)
    }
};

BDObject.prototype.add = function (req, res, next) {
    try {
        var params = req.body.data;

        if (!params) {
            (standartRequest(res, null, true, "invalid id or data"));
            return
        }


        var callback = function (err, result) {
            if (err !== null) {
                (standartRequest(res, null, "ok", err));
                return
            }

            else {
                (standartRequest(res, result));

            }
        };
        dbManager.add(this.name, params, callback)
    } catch (err) {
        next(err)
    }

};

BDObject.prototype.addDB = function (data, callback) {

    var params = data
    dbManager.add(this.name, params, callback)
};


module.exports = BDObject;