/**
 * Created by v.Triputin on 20.06.2018.
 */
const axios = require('axios');
const fs = require('fs');

class AppsFlyerData {
    constructor(appsflyerId,deviceId,eventName,eventValue) {
        this.appsflyer_id=appsflyerId;
        //this.deviceId=deviceId;
        this.eventName=eventName;
        this.eventValue = eventValue;
        //this.af_events_api = true;
    }
};

module.exports.AppsFlyerData = AppsFlyerData;

class EventValue{
    constructor(af_revenue, af_content_id) {
        //this.af_content_id = af_content_id;
        this.af_revenue = af_revenue
    }
}

module.exports.EventValue = EventValue;

class AppsFlyerService {
    constructor() {
        var data = JSON.parse(fs.readFileSync('bin/settings.conf'));
        this.url = `https://api2.appsflyer.com/inappevent/id${data.appsFlyerAppId}`;
        this.devKey = data.appsFlyerDevKey;
    }

    onEvent(data) {
        let self = this;

        var options = {
            method: 'POST',
            url: self.url,
            data: data,
            headers: {
                "authentication": self.devKey
            },
            json: true
        };


        return new Promise((resolve, reject) => {

            let reqCount = 0;
            let validateFunction = function (error) {
                if (reqCount > 5) {
                    reject(error);
                    return;
                }
                reqCount++;


                axios(options).then(
                    (data) => {
                        resolve(data)
                        return;
                    },
                    (error) => {
                        console.log("----------TryToReload-----------")
                        validateFunction(error)
                        console.log(error)
                        return;
                    })

            }
            validateFunction();
        })
    }
}

module.exports.AppsFlyerService = AppsFlyerService;