/**
 * Created by v.Triputin on 01.09.2017.
 */

var fs = require('fs')
exports.copyImage = function (from, to) {
    try{
        fs.createReadStream(from).pipe(fs.createWriteStream(to));
    }catch (e){
        console.log('no file', e)
    }

}

exports.isLoggedIn = function(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/admin/login');
};

exports.roleAuthorization = function(roles){

    return function(req, res, next){

        var user = req.user;



            if(roles.indexOf(user.role) > -1){
                return next();
            }

        res.redirect('/admin/permissions');


    }

};

exports.asyncMiddleware = fn =>
    (req, res, next) => {
        Promise.resolve(fn(req, res, next))
            .catch(next);
    };