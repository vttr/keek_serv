module.exports.light = {
   chatColors:[

//pink
    {
        "bubbleColor": "FDF3F4",
        "textColor": "000000",
        "nameColor": "E03A5B",
        "left": true
    },
    {
//blue
"bubbleColor": "EEF4FB",
    "textColor": "000000",
    "nameColor": "2C67CC",
           "left": false

    },
    {
//green
"bubbleColor": "EEFCF7",
    "textColor": "000000",
    "nameColor": "25CA8D",
           "left": true

    },
    {
//yellow
"bubbleColor": "FCF2E5",
    "textColor": "000000",
    "nameColor": "E89823",
           "left": false
    },
    {
//purple
        "bubbleColor": "F9EDFE",
        "textColor": "000000",
        "nameColor": "BA37F1",
           "left": true
    }
],
    backgroundcolor: "ffffff",
    foregroundcolor: "000000"
};

module.exports.dark = {
    chatColors:[

//pink
        {
            "bubbleColor": "1f2545",
            "textColor": "ffffff",
            "nameColor": "E03A5B",
            "left": true
        },
        {
//blue
            "bubbleColor": "1f2545",
            "textColor": "ffffff",
            "nameColor": "2C67CC",
            "left": false

        },
        {
//green
            "bubbleColor": "1f2545",
            "textColor": "ffffff",
            "nameColor": "25CA8D",
            "left": true

        },
        {
//yellow
            "bubbleColor": "1f2545",
            "textColor": "ffffff",
            "nameColor": "E89823",
            "left": false
        },
        {
//purple
            "bubbleColor": "1f2545",
            "textColor": "ffffff",
            "nameColor": "BA37F1",
            "left": true
        }
    ],
    backgroundcolor: "090b30",
    foregroundcolor: "ffffff"
};