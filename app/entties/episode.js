/**
 * Created by v.Triputin on 01.09.2017.
 */


var Episode = function(episode)
{
    if(episode===undefined) {
        this.storyId = null;
        this.number = null;
        this.messages = [];
        this.people = [];
    }
    this.storyId = episode.storyId;
    this.number = episode.storyId;
    for (var i in this.messages) {
        if (!this.messages[i].check() )return false
    }
    this.people = [];

};

Story.prototype.check = function () {

    try {
        if (this.storyId == null) return false;
        if (this.number == null) return false;
        if (this.messages.length == 0) return false;
        if (this.number.length == 0) return false;
        // for (var i in this.messages) {
        //     if (!this.messages[i].check() )return false
        //         }
        // for (var i in this.messages) {
        //     if (!this.messages[i].check() )return false
        // }
        return true;
    }
    catch (e) {return false}
};

module.exports = Episode;