/**
 * Created by v.Triputin on 08.01.2018.
 */

var GoogleSpreadsheet = require('google-spreadsheet');
var async = require('async');


var url = "https://docs.google.com/spreadsheets/d/1b41lm48cbLic1gsJr6KwrP_1qwZm1q7rVK5uop8Tuz4/edit#gid=0"
getIdFromUrl = function(url){
    var start = url.indexOf("/d/");
    var res = url.substring(start+3, url.length);
    var last = res.indexOf("/")==-1 ? url.length : res.indexOf("/");
    return res.substring(0, last)

};

function readDoc(url,callback){
try{
    // spreadsheet key is the long id in the sheets URL
    var doc = new GoogleSpreadsheet(getIdFromUrl(url));
    var sheet;
    var rowCount;
    var colCount;
    async.series([


        function setAuth(step) {
            // see notes below for authentication instructions!
            var creds = require('../bin/drive.json');
            // OR, if you cannot save the file locally (like on heroku)
            var creds_json = {
                client_email: 'yourserviceaccountemailhere@google.com',
                private_key: 'your long private key stuff here'
            };

            doc.useServiceAccountAuth(creds, step);
        },
        function getInfoAndWorksheets(step) {
            doc.getInfo(function(err, info) {
                console.log('Loaded doc: '+info.title+' by '+info.author.email);
                sheet = info.worksheets[0];
                console.log('sheet 1: '+sheet.title+' '+sheet.rowCount+'x'+sheet.colCount);
                rowCount = sheet.rowCount;
                colCount = sheet.colCount;
                step();
            });
        },
        // function workingWithRows(step) {
        //     // google provides some query options
        //
        //     console.log(colCount,rowCount)
        //     sheet.getRows({
        //         offset: 2,
        //         limit: rowCount,
        //         'return-empty': true
        //
        //     }, function( err, rows ){
        //         console.log('Read '+rows.length+' rows');
        //         console.log(rows)
        //         // the row is an object with keys set by the column headers
        //         //rows[0].colname = 'new val';
        //        // rows[0].save(); // this is async
        //
        //         // deleting a row
        //         //rows[0].del();  // this is async
        //
        //         step();
        //     });
        // },

        function workingWithCells(step) {
            // google provides some query options

            console.log(colCount,rowCount)
            sheet.getCells({
                'min-row': 1,
                'max-row': rowCount,
                'min-col': 1,
                'max-col': 2,
                rowCount: colCount,
                'return-empty': true
            }, function( err, cells ){
                var story ={};
                story.episodes = [];
                for (var i=0; i<cells.length-1;i+=2){

                    if(!story.name && cells[i]._value.indexOf("Story name")!==-1){
                        story.name=cells[i+1]._value
                        console.log(cells[i+1]._value)
                        continue;
                    }

                    if(!story.author && cells[i]._value.indexOf("Author")!==-1){
                        story.author=cells[i+1]._value
                        continue;
                    }

                    if(!story.genre && cells[i]._value.indexOf("Genre")!==-1){
                        story.genre=cells[i+1]._value
                        continue;
                    }



                }
                console.log(story)

                step();
            });
        },

    ], function(err){
        if( err ) {
            console.log('Error: '+err);
            callback(err)
            return
        }
    });
}catch (err){
    callback(err)
    return
}

};




var readSheet = function () {
    readDoc(url,function (err,data) {

    })
};

//readSheet()
//
// async.series([
//     function setAuth(step) {
//         // see notes below for authentication instructions!
//         var creds = require('../bin/drive.json');
//         // OR, if you cannot save the file locally (like on heroku)
//         var creds_json = {
//             client_email: 'yourserviceaccountemailhere@google.com',
//             private_key: 'your long private key stuff here'
//         }
//
//         doc.useServiceAccountAuth(creds, step);
//     },
//     function getInfoAndWorksheets(step) {
//         doc.getInfo(function(err, info) {
//             console.log('Loaded doc: '+info.title+' by '+info.author.email);
//             sheet = info.worksheets[0];
//             console.log('sheet 1: '+sheet.title+' '+sheet.rowCount+'x'+sheet.colCount);
//             step();
//         });
//     },
//     function workingWithRows(step) {
//         // google provides some query options
//         sheet.getRows({
//             offset: 1,
//             limit: 20,
//             orderby: 'col2'
//         }, function( err, rows ){
//             console.log('Read '+rows.length+' rows');
//
//             // the row is an object with keys set by the column headers
//             rows[0].colname = 'new val';
//             rows[0].save(); // this is async
//
//             // deleting a row
//             rows[0].del();  // this is async
//
//             step();
//         });
//     },
//     function workingWithCells(step) {
//         sheet.getCells({
//             'min-row': 1,
//             'max-row': 5,
//             'return-empty': true
//         }, function(err, cells) {
//             var cell = cells[0];
//             console.log('Cell R'+cell.row+'C'+cell.col+' = '+cell.value);
//
//             // cells have a value, numericValue, and formula
//             cell.value == '1'
//             cell.numericValue == 1;
//             cell.formula == '=ROW()';
//
//             // updating `value` is "smart" and generally handles things for you
//             cell.value = 123;
//             cell.value = '=A1+B2'
//             cell.save(); //async
//
//             // bulk updates make it easy to update many cells at once
//             cells[0].value = 1;
//             cells[1].value = 2;
//             cells[2].formula = '=A1+B1';
//             sheet.bulkUpdateCells(cells); //async
//
//             step();
//         });
//     },
//     function managingSheets(step) {
//         doc.addWorksheet({
//             title: 'my new sheet'
//         }, function(err, sheet) {
//
//             // change a sheet's title
//             sheet.setTitle('new title'); //async
//
//             //resize a sheet
//             sheet.resize({rowCount: 50, colCount: 20}); //async
//
//             sheet.setHeaderRow(['name', 'age', 'phone']); //async
//
//             // removing a worksheet
//             sheet.del(); //async
//
//             step();
//         });
//     }
// ], function(err){
//     if( err ) {
//         console.log('Error: '+err);
//     }
// });