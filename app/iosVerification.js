const iap = require('in-app-purchase');

iap.config({
    applePassword: "a5d5bb07fd264b629a15d131a90c46fa"
});

iap.setup(function (error) {
    if (error) {
        return console.error('something went wrong...');
    }
});

var Purchases = require("./purchases");

module.exports.verifyAsync = function(user){
    return new Promise((resolve,reject) => {

        let reqCount = 0;
        let validateFunction = function (error, response) {
            if (error) {
                if (reqCount>15) {
                    reject(error);
                    return;
                }
                reqCount++;
                console.log(error);
                console.log("----------TryToReload-----------")
                iap.validate(iap.APPLE, user.appleToken, validateFunction)
            }

            if (iap.isValidated(response)) {
                // get the purchased items that have not been expired ONLY
                var options = {
                    ignoreExpired: false
                };

                var purchases = iap.getPurchaseData(response, options);

                resolve({purchases: purchases,
                environment : response.environment
                })
            }
        };
        iap.validate(iap.APPLE, user.appleToken, validateFunction)
    })
};
