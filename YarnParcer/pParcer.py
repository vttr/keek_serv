#!/usr/bin/env python
# -*- coding: utf-8 -*-
import parcer


from json import JSONEncoder
import json
import logging


from bs4 import BeautifulSoup


siteUrl = "http://forum.poehali.net/"
routeUrl = "http://forum.poehali.net/index.php?board=12"




import time
from sshtunnel import SSHTunnelForwarder
import pymongo
import pprint

MONGO_HOST = "78.46.175.237"
MONGO_DB = "poehali"
MONGO_USER = "root"
MONGO_PASS = "V3682450v"

server = SSHTunnelForwarder(
    MONGO_HOST,
    ssh_username=MONGO_USER,
    ssh_password=MONGO_PASS,
    remote_bind_address=('127.0.0.1', 27017)
)



# client = pymongo.MongoClient('127.0.0.1', server.local_bind_port)  # server.local_bind_port is assigned local port
# server.start()

client = pymongo.MongoClient()
def  addRouteToDB(route):

    print route._id
    data = route.getData()


    db = client[MONGO_DB]
    routes = db.routes

    routes.replace_one({'_id':data['_id']}, data,upsert= True)


# logging.basicConfig('log.txt', level=logging.DEBUG)

def  addBanLinkBanIdToDB(linkbanId):


    data = {"_id":linkbanId}


    db = client[MONGO_DB]
    routes = db.linkban

    routes.replace_one({'_id':data['_id']}, data,upsert= True)

def  addPageToDB(link,name):


    data = {}
    data["_id"] = link
    data["name"] = name
    data["url"] = link
    db = client[MONGO_DB]
    routes = db.pages

    routes.replace_one({'_id':data['_id']}, data,upsert= True)

def  getPagesFromDB():



    db = client[MONGO_DB]
    routes = db.pages

    Items = routes.find({})
    retSet = []
    for item in Items:
        retSet.append(item)
    return retSet


def getRoutesBanSet():
    db = client[MONGO_DB]
    routes = db.routes

    Items = routes.find({})
    retSet = set()
    for item in Items:
        retSet.add(item['_id'])
    return retSet




def getLinkBanSet():
    db = client[MONGO_DB]
    routes = db.linkban

    Items = routes.find({})
    retSet = set()
    for item in Items:
        retSet.add(item['_id'])
    return retSet

class Track:
    def __init__(self):
        self.url = ''
        self.routeurl = ''
        self.name = ''
        self.data = {}
    url = ''
    routeurl = ''
    name = ''
    downloadUrl = ''
    data = {}

    def getData(self):
        data = {}
        data['name'] = self.name
        data['url'] = self.url
        data["routeurl"] =self.routeurl
        data["data"] = self.data
        if not self.downloadUrl=="":
            data["downloadUrl"] = self.downloadUrl
        return data

class Route:
    def __init__(self):
        self._id = ''
        self.tracs = []
        self.name = ''
        self.url = ''

    _id = ""
    tracs = []
    name = ''
    url = ''

    def getData(self):
        data = {}
        data['_id'] = self._id
        data['name'] = self.name
        data['url'] = self.url
        data["tracks"] =[]
        for track in self.tracs:
            data["tracks"].append(track.getData())
        return data


import xmltodict
def getTrackByURL(link):

    link = link.replace('http://www.gpsies.com/map.do',"https://www.gpsies.com/download.do")
    track = Track()
    html = parcer.get_html(link)
    data1 = ""
    try:
        data1 = xmltodict.parse(html)
    except:

        print "parce error"
        print link
        return track

    track = Track()
    track.downloadUrl = link
    trackJson = json.dumps(data1, sort_keys=False, indent=4, separators=(',', ': '))
    trackJson = trackJson.replace("\"@", "\"")


    track.data = json.loads(trackJson)

    return track


def getrouteByUrl(link):
    route = Route()
    route.url=link["url"]
    route.name = link["name"]


    html = parcer.get_html(link["url"])

    tracsSet = set()

    soup = BeautifulSoup(html, "html5lib")

    # get pages

    # print html

    # gps_track_link = soup.find_all(text="\n              GPSies \u0442\u0440\u0435\u043a(\u0438):\xa0").parent.findNext('a').href
    gps_track_link = None

    refs = soup.find_all('a', href=True)
    for ref in refs:
        if "http://www.gpsies.com" in ref["href"]:
            print link["url"]
            print ref["href"]
            gps_track_link = ref["href"]
            if gps_track_link in tracsSet: continue
            tracsSet.add(gps_track_link)
            track = getTrackByURL(gps_track_link)

            track.url = gps_track_link
            track.name = route.name
            track.routeurl = route.url
            route.tracs.append(track)

    spanPages = soup.find("span", {"class": "imgpages"})
    pagecount = 0
    if spanPages:
        ass = spanPages.find_all("a")
        if (len(ass)>0):

            pageCount = int(ass[len(ass)-2].text)
            # ;start = 15
            for i in range(1,pageCount):
                PageLink = link["url"]+";start=" + str(i*15)

                html = parcer.get_html(PageLink)
                soup = BeautifulSoup(html, "html5lib")
                refs = soup.find_all('a', href=True)
                for ref in refs:
                    if "http://www.gpsies.com" in ref["href"]:

                        gps_track_link = ref["href"]
                        if gps_track_link in tracsSet: continue
                        print PageLink
                        print ref["href"]
                        tracsSet.add(gps_track_link)
                        track = getTrackByURL(gps_track_link)
                        track.url = gps_track_link
                        track.name = route.name
                        track.routeurl = PageLink
                        route.tracs.append(track)



    if not gps_track_link: return None


    return route



def parceRoutes(startIndex,endIndex,arr,process):
    # print startIndex
    # print endIndex
    # print process
    length = endIndex-startIndex
    # banSet = getRoutesBanSet()

    routes = []
    for i in range (startIndex,endIndex):




        print "process ["+str(process)+"] "+ str(i-startIndex) + "/" + str(length) + "(" + str(i) + ")"
        link = arr[i]
        route = getrouteByUrl(link)
        if route:
            route._id = link["_id"]
            routes.append(route)
            addRouteToDB(route)
        addBanLinkBanIdToDB(link["_id"])

    return routes


from multiprocessing import Process


def getParseParameters(processCount, arr):

    length = len(arr)
    print "subdirectory count: " + str(length)
    print "process count: " + str(processCount)

    parceParametersLength = int(length/processCount)
    Parseparameters = []
    for i in range(0,processCount):
        minmax = [i*parceParametersLength,(i+1)*parceParametersLength]
        Parseparameters.append(minmax)
        print minmax

    if not (length%processCount)==0:
        minmax = [processCount*parceParametersLength,length]
        Parseparameters.append(minmax)
        print minmax
    return Parseparameters


def getLinkArray():
    banSet = getLinkBanSet()
    retArr = []
    links  = getPagesFromDB()
    for i in links:
        if not i["_id"] in banSet:
            retArr.append(i)
    return retArr


def multiProcessParsing (processCount, startItem, endItem):
    processes = []
    arr = getLinkArray()
    parseParameters = getParseParameters(processCount, arr)

    for i in range(0,len(parseParameters)):
        parcer.time.sleep(1)
        p = Process(target=parceRoutes, args=(parseParameters[i][0],parseParameters[i][1],arr,i))
        p.start()
        processes.append(p)
    k = 0
    for p in processes:
        p.join()
        print 'process ' + str(k) + ' end '
        k+=1


import sys
isMulti = False
if __name__ == "__main__":
    startInex = 0
    endIndex = 0
    processCount = 0
    ind = 0

    startTime = time.time()

    print "Example: 0 1000 10"
    for param in sys.argv:

        if ind ==1:
            startInex = int(param)
            # print path

        if ind == 2:
            # print param
            endIndex = int(param)


        if ind == 3:
            # print param
            processCount = int(param)
            multiProcessParsing(processCount, startInex, endIndex)
            isMulti = True
        ind+=1

    # if not isMulti:
    #     startItem = 1
    #     endItem = 125
    #
    #     routesidx = list(getRoutesBanSet())
    #     routes = parceRoutes(945,len(routesidx),routesidx,0)
    #
    #     f = open('res.json', 'w')
    #
    #     data = []
    #
    #     for route in routes:
    #         data.append(route.getData())
    #
    #     f.write(json.dumps(data, sort_keys=False,indent=4, separators=(',', ': ')).decode('unicode-escape').encode("utf-8"))
    #     f.close()
    #
    # print "Parcing time: " + str(time.time() - startTime)

def getPages(startPage,EndPage):

    for index in  range(startPage,EndPage):
        curIndex = index*25
        curUrl = "http://forum.poehali.net/index.php?board=12;action=messageindex;start="+ str(curIndex)
        html = html = parcer.get_html(curUrl)
        soup = BeautifulSoup(html, "html5lib")
        tds = soup.find_all("td",{"class":"windowbg"})
        aList = []
        print str(index)+"/"+str(EndPage)
        for td in tds:
            a = td.findNext("a")
            if a:
                if "threadid" in a["href"]:
                    print a.text.encode("cp1252", 'replace').decode("cp1251")

                    name = a.text.encode("cp1252", 'replace').decode("cp1251")
                    addPageToDB(a["href"],name)

        # print len(aList)
        # print aList


#getPages(0, 116)
# server.stop()

# getrouteByUrl('https://biketourist.club/route/4223')

# link = {}
# link["url"] = "http://forum.poehali.net/index.php?board=12;action=display;threadid=59313"
# link["name"] = "xxx"
# getrouteByUrl(link)