#!/usr/bin/env python
# -*- coding: utf-8 -*-
import parcer


from json import JSONEncoder
import json
import logging


from bs4 import BeautifulSoup


siteUrl = "http://forum.poehali.net/"
routeUrl = "http://forum.poehali.net/index.php?board=12"



import os
import time
from sshtunnel import SSHTunnelForwarder
import pymongo
import pprint
import urllib

MONGO_HOST = ""
MONGO_DB = "Hooked"
MONGO_USER = ""
MONGO_PASS = ""




# client = pymongo.MongoClient('127.0.0.1', server.local_bind_port)  # server.local_bind_port is assigned local port
# server.start()

client = pymongo.MongoClient()
def  addRouteToDB(route):

    print route._id
    data = route.getData()


    db = client[MONGO_DB]
    routes = db.routes

    routes.replace_one({'_id':data['_id']}, data,upsert= True)


# logging.basicConfig('log.txt', level=logging.DEBUG)

def  addBanLinkBanIdToDB(linkbanId):


    data = {"_id":linkbanId}


    db = client[MONGO_DB]
    routes = db.linkban

    routes.replace_one({'_id':data['_id']}, data,upsert= True)

def  addPageToDB(link,name):


    data = {}
    data["_id"] = link
    data["name"] = name
    data["url"] = link
    db = client[MONGO_DB]
    routes = db.pages

    routes.replace_one({'_id':data['_id']}, data,upsert= True)

def  getPagesFromDB():



    db = client[MONGO_DB]
    routes = db.pages

    Items = routes.find({})
    retSet = []
    for item in Items:
        retSet.append(item)
    return retSet


def getRoutesBanSet():
    db = client[MONGO_DB]
    routes = db.routes

    Items = routes.find({})
    retSet = set()
    for item in Items:
        retSet.add(item['_id'])
    return retSet




def getLinkBanSet():
    db = client[MONGO_DB]
    routes = db.linkban

    Items = routes.find({})
    retSet = set()
    for item in Items:
        retSet.add(item['_id'])
    return retSet

class Track:
    def __init__(self):
        self.url = ''
        self.routeurl = ''
        self.name = ''
        self.data = {}
    url = ''
    routeurl = ''
    name = ''
    downloadUrl = ''
    data = {}

    def getData(self):
        data = {}
        data['name'] = self.name
        data['url'] = self.url
        data["routeurl"] =self.routeurl
        data["data"] = self.data
        if not self.downloadUrl=="":
            data["downloadUrl"] = self.downloadUrl
        return data

class Route:
    def __init__(self):
        self._id = ''
        self.tracs = []
        self.name = ''
        self.url = ''

    _id = ""
    tracs = []
    name = ''
    url = ''

    def getData(self):
        data = {}
        data['_id'] = self._id
        data['name'] = self.name
        data['url'] = self.url
        data["tracks"] =[]
        for track in self.tracs:
            data["tracks"].append(track.getData())
        return data


import xmltodict
def getTrackByURL(link):

    link = link.replace('http://www.gpsies.com/map.do',"https://www.gpsies.com/download.do")
    track = Track()
    html = parcer.get_html(link)
    data1 = ""
    try:
        data1 = xmltodict.parse(html)
    except:

        print "parce error"
        print link
        return track

    track = Track()
    track.downloadUrl = link
    trackJson = json.dumps(data1, sort_keys=False, indent=4, separators=(',', ': '))
    trackJson = trackJson.replace("\"@", "\"")


    track.data = json.loads(trackJson)

    return track


def getrouteByUrl(link):
    route = Route()
    route.url=link["url"]
    route.name = link["name"]


    html = parcer.get_html(link["url"])

    tracsSet = set()

    soup = BeautifulSoup(html, "html5lib")

    # get pages

    # print html

    # gps_track_link = soup.find_all(text="\n              GPSies \u0442\u0440\u0435\u043a(\u0438):\xa0").parent.findNext('a').href
    gps_track_link = None

    refs = soup.find_all('a', href=True)
    for ref in refs:
        if "http://www.gpsies.com" in ref["href"]:
            print link["url"]
            print ref["href"]
            gps_track_link = ref["href"]
            if gps_track_link in tracsSet: continue
            tracsSet.add(gps_track_link)
            track = getTrackByURL(gps_track_link)

            track.url = gps_track_link
            track.name = route.name
            track.routeurl = route.url
            route.tracs.append(track)

    spanPages = soup.find("span", {"class": "imgpages"})
    pagecount = 0
    if spanPages:
        ass = spanPages.find_all("a")
        if (len(ass)>0):

            pageCount = int(ass[len(ass)-2].text)
            # ;start = 15
            for i in range(1,pageCount):
                PageLink = link["url"]+";start=" + str(i*15)

                html = parcer.get_html(PageLink)
                soup = BeautifulSoup(html, "html5lib")
                refs = soup.find_all('a', href=True)
                for ref in refs:
                    if "http://www.gpsies.com" in ref["href"]:

                        gps_track_link = ref["href"]
                        if gps_track_link in tracsSet: continue
                        print PageLink
                        print ref["href"]
                        tracsSet.add(gps_track_link)
                        track = getTrackByURL(gps_track_link)
                        track.url = gps_track_link
                        track.name = route.name
                        track.routeurl = PageLink
                        route.tracs.append(track)



    if not gps_track_link: return None


    return route



def parceRoutes(startIndex,endIndex,arr,process):
    # print startIndex
    # print endIndex
    # print process
    length = endIndex-startIndex
    # banSet = getRoutesBanSet()

    routes = []
    for i in range (startIndex,endIndex):




        print "process ["+str(process)+"] "+ str(i-startIndex) + "/" + str(length) + "(" + str(i) + ")"
        link = arr[i]
        route = getrouteByUrl(link)
        if route:
            route._id = link["_id"]
            routes.append(route)
            addRouteToDB(route)
        addBanLinkBanIdToDB(link["_id"])

    return routes


from multiprocessing import Process


def getParseParameters(processCount, arr):

    length = len(arr)
    print "subdirectory count: " + str(length)
    print "process count: " + str(processCount)

    parceParametersLength = int(length/processCount)
    Parseparameters = []
    for i in range(0,processCount):
        minmax = [i*parceParametersLength,(i+1)*parceParametersLength]
        Parseparameters.append(minmax)
        print minmax

    if not (length%processCount)==0:
        minmax = [processCount*parceParametersLength,length]
        Parseparameters.append(minmax)
        print minmax
    return Parseparameters


def getLinkArray():
    banSet = getLinkBanSet()
    retArr = []
    links  = getPagesFromDB()
    for i in links:
        if not i["_id"] in banSet:
            retArr.append(i)
    return retArr


def multiProcessParsing (processCount, startItem, endItem):
    processes = []
    arr = getLinkArray()
    parseParameters = getParseParameters(processCount, arr)

    for i in range(0,len(parseParameters)):
        parcer.time.sleep(1)
        p = Process(target=parceRoutes, args=(parseParameters[i][0],parseParameters[i][1],arr,i))
        p.start()
        processes.append(p)
    k = 0
    for p in processes:
        p.join()
        print 'process ' + str(k) + ' end '
        k+=1


import sys
isMulti = False
if __name__ == "__main__":
    startInex = 0
    endIndex = 0
    processCount = 0
    ind = 0

    startTime = time.time()

    print "Example: 0 1000 10"
    for param in sys.argv:

        if ind ==1:
            startInex = int(param)
            # print path

        if ind == 2:
            # print param
            endIndex = int(param)


        if ind == 3:
            # print param
            processCount = int(param)
            multiProcessParsing(processCount, startInex, endIndex)
            isMulti = True
        ind+=1

    # if not isMulti:
    #     startItem = 1
    #     endItem = 125
    #
    #     routesidx = list(getRoutesBanSet())
    #     routes = parceRoutes(945,len(routesidx),routesidx,0)
    #
    #     f = open('res.json', 'w')
    #
    #     data = []
    #
    #     for route in routes:
    #         data.append(route.getData())
    #
    #     f.write(json.dumps(data, sort_keys=False,indent=4, separators=(',', ': ')).decode('unicode-escape').encode("utf-8"))
    #     f.close()
    #
    # print "Parcing time: " + str(time.time() - startTime)

import requests


def get_genre():
    # url = 'https://e-dostavka.by/catalog/3963.html'

    headers = {
         'X-Parse-Application-Id': 'Vh382DiUoSheUIWSKkhhH7UV2e8KXeg0wtWh3W1i',
                  'X-Parse-OS-Version':'6.0.1',
                    'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36',
                    'Content-Type':'application/json'
    }
    # data = {"where":"{\"name\":{\"$exists\":true},\"ordinal\":{\"$exists\":true}}","order":"ordinal","limit":"10000","_method":"GET"}

    r = requests.get(genreURL, headers=headers, verify=False)

    return r.text


def get_message(storyId):

    #url = 'https://e-dostavka.by/catalog/3963.html'

    headers = {
                  'X-Parse-Application-Id': 'Vh382DiUoSheUIWSKkhhH7UV2e8KXeg0wtWh3W1i',
                  'X-Parse-OS-Version':'6.0.1',
                    'User-Agent':'Parse Android SDK 1.16.2 (tv.telepathic.hooked/43) API Level 23',
                    'Content-Type':'application/json'
               }
    data = {"where":"{\"story\":{\"__type\":\"Pointer\",\"className\":\"Story\",\"objectId\":\""+storyId+"\"}}","order":"ordinalInStory","limit":"1000","_method":"GET"}
    r = requests.post(messageURL, headers = headers,data=json.dumps(data), verify=False)

    return r.text



def get_nextretrive(num):

    #url = 'https://e-dostavka.by/catalog/3963.html'

    headers = {
                  'X-Parse-Application-Id': 'Vh382DiUoSheUIWSKkhhH7UV2e8KXeg0wtWh3W1i',
                  'X-Parse-OS-Version':'6.0.1',
                    'User-Agent':'Parse Android SDK 1.16.2 (tv.telepathic.hooked/43) API Level 23',
                    'Content-Type':'application/json'
               }

    data = {"channelUID": "Telepathic", "pageSpecification": {"ordinalInBatch": num, "limit": 10}, "language": "en"}
    r = requests.post(retriveNextStoryURL, headers = headers,data=json.dumps(data), verify=False)

    return r.text

def get_storiesByGenre(genre,last):
    headers = {
                    'X-Parse-Application-Id': 'Vh382DiUoSheUIWSKkhhH7UV2e8KXeg0wtWh3W1i',
                    'X-Parse-OS-Version': '6.0.1',
                    'User-Agent': 'Parse Android SDK 1.16.2 (tv.telepathic.hooked/43) API Level 23',
                    'Content-Type': 'application/json'
            }


    data = {"genre": {"genreUID": genre, "language": "en", "variant": 0}, "pageSpecification": {"last": last, "limit": 10}}
    r = requests.post(storiesByGenreURL, headers = headers,data=json.dumps(data), verify=False)
    return r.text

def get_html(url,id):
    # url = 'https://e-dostavka.by/catalog/3963.html'
    tempUrl = url + id
    headers = {
        'X-Parse-Application-Id': 'Vh382DiUoSheUIWSKkhhH7UV2e8KXeg0wtWh3W1i',
        'X-Parse-OS-Version': '6.0.1',
        'User-Agent': 'Parse Android SDK 1.16.2 (tv.telepathic.hooked/43) API Level 23',
        'Content-Type': 'application/json'
    }

    r = requests.get(tempUrl, headers=headers, verify=False)
    return r.text

def get_storyByIndex(epCount,epIdent,index):

    headers = {
        'X-Parse-Application-Id': 'Vh382DiUoSheUIWSKkhhH7UV2e8KXeg0wtWh3W1i',
        'X-Parse-OS-Version': '6.0.1',
        'User-Agent': 'Parse Android SDK 1.16.2 (tv.telepathic.hooked/43) API Level 23',
        'Content-Type': 'application/json'
    }
    data = {
        "where": "{\"seriesIdentifier\":\""+epIdent+"\",\"episodeIndex\":"+str(index)+"}",
        "limit": "1", "_method": "GET"}

    r = requests.post(storyURL, headers=headers, data=json.dumps(data), verify=False)
    return r.text


genreURL = "https://production.hooked.media/parse/classes/Genre"
messageURL = "https://production.hooked.media/parse/classes/Message"
characterURL = "https://production.hooked.media/parse/classes/Character/"
storyURL = "https://production.hooked.media/parse/classes/Story/"
retriveNextStoryURL = "https://production.hooked.media/parse/functions/retrieveAnonymousNextHomeStoryCovers"
storiesByGenreURL = "https://production.hooked.media/parse/functions/retrieveUnreadStories"
PathDirectory = "hookedContent"
# genre
# print get_genre()
# message
# print get_message("HckyD0tAAS")
# story
# print get_html(storyURL,"HckyD0tAAS")
# characters
# print get_html(characterURL,"rV37UM5evC")


def CreatePaths(storyId):
    parcer.createDir(PathDirectory)
    mainPath = os.path.dirname(os.path.abspath(__file__))+"/"+PathDirectory
    curcategoryName = str(storyId).translate(None, '!@#$\\:\/\"')
    cursubcategoryPath = mainPath + '/' + curcategoryName
    parcer.createDir(cursubcategoryPath)

    return cursubcategoryPath


def  addepisodeToDB(episode, epId):

    episode["_id"] = str(epId)
    data = episode

    db = client[MONGO_DB]
    routes = db.episodes

    routes.replace_one({'_id':data['_id']}, data,upsert= True)


def downloadEpisode(storyId,episodeId):

    url = siteUrl+ "story/"+str(storyId)+"/episode/"+str(episodeId)
    print url
    jsonEpisode = parcer.get_html(url)
    # print jsonEpisode
    episode = json.loads(jsonEpisode)
    # print episode["messages"][0]
    return episode


def  addstoryStatisticToDB(id,story):

    story["_id"] = id
    data = story

    db = client[MONGO_DB]
    routes = db.storyStatistic

    routes.replace_one({'_id':data['_id']}, data,upsert= True)

def  addstoryToDB(id,story):

    story["_id"] = id
    data = story

    db = client[MONGO_DB]
    routes = db.stories

    routes.replace_one({'_id':data['_id']}, data,upsert= True)

def  addseryToDB(sery):
    data = sery
    db = client[MONGO_DB]
    routes = db.series

    routes.replace_one({'_id':data['_id']}, data,upsert= True)

def downloadImage(imageUrl,imageName,folderPath):

    imUrl = imageUrl
    parcer.createDir(folderPath)
    imagePath = folderPath+"/"+imageName + ".jpg"
    print imUrl
    img = urllib.urlopen(imUrl)
    localFile = open(imagePath.decode('utf-8'), 'wb')
    localFile.write(img.read())
    localFile.close()


stories = dict()

def trim(url):
    num = url.find("/")
    return url[num+1:len(url)]





def getStoryBanSet():
    db = client[MONGO_DB]
    routes = db.stories

    Items = routes.find({})
    retSet = set()
    for item in Items:
        retSet.add(item['_id'])
    return retSet

def getStoriesDB():
    db = client[MONGO_DB]
    routes = db.stories

    return routes.find({})

def getSeriesDB():
    db = client[MONGO_DB]
    routes = db.series

    return routes.find({})

def downloadStory(story,banset={},count=1,stories=[] ):
    if story["_id"]:
        story["objectId"]=story["_id"]
    if story["objectId"] in banset:
       return
    print str(count) + "/" + str(len(stories)) + ' ' + str(story["objectId"])
    path = CreatePaths(story["objectId"])

    storyObj = json.loads(get_html(storyURL, story["objectId"]))

    if "coverImageFile" in storyObj:
        try:
            imUrl = storyObj["coverImageFile"]["url"]
            imName = storyObj["coverImageFile"]["name"]
            downloadImage(imUrl, imName, path)
        except:
            print storyObj
    messages = json.loads(get_message(story["objectId"]))

    people = set()
    for message in messages["results"]:
        try:
            people.add(message["sender"]["objectId"])
        except:
            people.add(message["objectId"])
        if "imageFile" in message:
            try:
                imUrl = message["imageFile"]["url"]
                imName = message["imageFile"]["name"]
                downloadImage(imUrl, imName, path)
            except:
                print message
    peoples = []
    for person in people:
        peoples.append(json.loads(get_html(characterURL, person)))

        storyObj["peoples"] = peoples
        storyObj["messages"] = messages["results"]
    addstoryToDB(story["objectId"], storyObj)

    fileName = path + "/" + str(story["objectId"]) + ".json"
    f = open(fileName, "w")
    f.write(
        json.dumps(story, sort_keys=True, indent=4, separators=(',', ': ')).decode('unicode-escape').encode("utf-8"))
    f.close()



def getStories():
    jsonGanres = get_genre()
    ganres = json.loads(jsonGanres)

    count = 0
    stories = []
    banset = getStoryBanSet()
    for ganre in ganres["results"]:
        stories.extend(ganre["topStories"])

    for story in stories:
        downloadStory(story, banset, count, stories)
        count+=1


def addstoryStatistic(story):
    print "try to add"
    try:
        print story["objectId"]
    except:
        print story
        return
    try:
        addstoryStatisticToDB(story["objectId"], story)
    except:
        print story

def addstory(story):
    print "try to add"
    path = CreatePaths(story["objectId"])

    storyObj = story

    if "coverImageFile" in storyObj:
        try:
            imUrl = storyObj["coverImageFile"]["url"]
            imName = storyObj["coverImageFile"]["name"]
            downloadImage(imUrl, imName, path)
        except:
            print storyObj
    print story
    messages = json.loads(get_message(story["objectId"]))

    people = set()
    for message in messages["results"]:
        try:
            people.add(message["sender"]["objectId"])
        except:
            people.add(message["objectId"])
        if "imageFile" in message:
            try:
                imUrl = message["imageFile"]["url"]
                imName = message["imageFile"]["name"]
                downloadImage(imUrl, imName, path)
            except:
                print message
    peoples = []
    for person in people:
        peoples.append(json.loads(get_html(characterURL, person)))

        storyObj["peoples"] = peoples
        storyObj["messages"] = messages["results"]
    addstoryToDB(story["objectId"], storyObj)

    print "ok"
    fileName = path + "/" + str(story["objectId"]) + ".json"
    f = open(fileName, "w")
    f.write(json.dumps(story, sort_keys=True, indent=4, separators=(',', ': ')).decode('unicode-escape').encode(
        "utf-8"))
    f.close()

def getNextStories():

    count = 0
    stories = []
    banset = getStoryBanSet()
    isContinue = True
    num = 0
    while isContinue:
        nextstories = json.loads(get_nextretrive(num))
        num += 10
        print num
        if len(nextstories["result"]["storyCovers"])==0:
            isContinue = False
            return

        for cover in nextstories["result"]["storyCovers"]:
            story = cover["story"]
            if story["objectId"] in banset:
                count += 1
                continue

            addstory(story)
            count += 1


def getMissedStories():

    count = 0
    stories = []
    banset = getStoryBanSet()
    stories = list(getStoriesDB())
    series = list(getSeriesDB())
    isContinue = True
    num = 0
    stdents = set()
    for sery in series:
        print str(count) + "/" + str(len(sery))
        count += 1
        if count>1000: continue
        stedent = sery["_id"]
        episodeCount = sery['stories'][0]["episodeCount"]
        if episodeCount == 1:
            continue
        for i in range(1,episodeCount+1):
            story = json.loads(get_storyByIndex(episodeCount,stedent,i))
            print i,episodeCount

            try:
                if story["results"][0]["objectId"] in banset:
                    continue
                addstory(story["results"][0])
            except:
                print story


def deleteStoryStatisticDB(id):
    db = client[MONGO_DB]
    routes = db.storyStatistic

    return routes.delete_many({'_id': id})

def getStoryStatisticDB():
    db = client[MONGO_DB]
    routes = db.storyStatistic

    return routes.find({})

def getStoriesByGenreStatistic():

    count = 0
    stories = []
    banset = getStoryBanSet()
    banset = []
    isContinue = True
    num = 0


    count = 0
    stories = []
    isContinue = True
    num = 0
    while isContinue:
        nextstories = json.loads(get_nextretrive(num))
        num += 10
        print num
        if len(nextstories["result"]["storyCovers"])==0:
            isContinue = False
            return

        for cover in nextstories["result"]["storyCovers"]:
            story = cover["story"]


            addstoryStatistic(story)
            count += 1
    jsonGanres = get_genre()
    genres = json.loads(jsonGanres)["results"]
    genreNames = [];
    for genre in genres:
        genreNames.append(genre["name"])

    for ganre in genres:
        stories.extend(ganre["topStories"])

    for story in stories:
        storyObj = json.loads(get_html(storyURL, story["objectId"]))
        addstoryStatistic(storyObj)
        continue

    # stories = getStoryStatisticDB()
    # for story in stories:
    #     try:
    #         if story["genre"]:
    #             continue
    #     except:
    #         print story
    #         deleteStoryStatisticDB(story["objectId"])
    #         # storyObj = json.loads(get_html(storyURL, story["objectId"]))
    #         # addstoryStatistic(storyObj)
    # return

    for genre in genreNames:
        print genre
        last = None
        num = 0
        isContinue = True
        count = 0
        while isContinue:
            nextstories = json.loads(get_storiesByGenre(genre,last))
            try:
                last = nextstories["result"]["nextPageSpecification"]["last"]
            except:
                isContinue = False
                break
            num+=10
            print num
            if len(nextstories["result"]["stories"]) == 0:
                isContinue = False
                break

            for cover in nextstories["result"]["stories"]:
                story = cover
                if story["objectId"] in banset:
                    count += 1
                    continue
                addstoryStatistic(story)

                count += 1


def getStoriesByGenre():

    count = 0
    stories = []
    banset = getStoryBanSet()
    isContinue = True
    num = 0
    ganres = ["Romance",
              "Drama",
              "Horror",
              "Fantasy",
              "Mystery",
              "Paranormal",
              "Thriller",
              "Comedy",
              "Sci-Fi"
              ]
    for genre in ganres:
        print genre
        last = None
        num = 0
        isContinue = True
        count = 0
        while isContinue:
            nextstories = json.loads(get_storiesByGenre(genre,last))
            try:
                last = nextstories["result"]["nextPageSpecification"]["last"]
            except:
                isContinue = False
                break
            num+=10
            print num
            if len(nextstories["result"]["stories"]) == 0:
                isContinue = False
                break

            for cover in nextstories["result"]["stories"]:
                story = cover
                if story["objectId"] in banset:
                    count += 1
                    continue

                addstory(story)
                count += 1
# print downloadEpisode(4,400)

def addStorySequences():
    storyFile = open(os.path.dirname(os.path.abspath(__file__))+"/ss.json", "r")
    data = storyFile.read()

    print data


    dataJson = json.loads( data)

    for seq in dataJson:
        seq["_id"] = str(seq["id"])

        data = seq
        db = client[MONGO_DB]
        routes = db.storysequenses

        routes.replace_one({'_id':data['_id']}, data,upsert= True)



# getStories()
# getNextStories()
# getStoriesByGenre()




#getStoriesByGenreStatistic()


def convertStoriesToSeries():
    stories = list(getStoriesDB())
    seriesDict = dict()
    series = []
    for story in stories:
        try:
            if story["seriesIdentifier"] in seriesDict:
                series[seriesDict[story["seriesIdentifier"]]]["stories"].append(story)
                continue
        except:
            # downloadStory(story, stories)
            continue
        seriesDict[story["seriesIdentifier"]] = len(series)
        sery = dict()
        sery["_id"] = story["seriesIdentifier"]
        stories_ = []
        stories_.append(story)
        sery["stories"] = stories_
        try:
          sery["name"] = story["seriesTitle"]
        except:
          return
        series.append(sery)
    for sery in series:
        addseryToDB(sery)

# convertStoriesToSeries()
# addStorySequences()

getMissedStories()