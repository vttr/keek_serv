#!/usr/bin/env python
# -*- coding: utf-8 -*-
import parcer


from json import JSONEncoder
import json
import logging


from bs4 import BeautifulSoup


siteUrl = "https://biketourist.club"
routeUrl = "https://biketourist.club/route"





from sshtunnel import SSHTunnelForwarder
import pymongo
import pprint

MONGO_HOST = "78.46.175.237"
MONGO_DB = "biketouristclub"
MONGO_USER = "root"
MONGO_PASS = "V3682450v"

server = SSHTunnelForwarder(
    MONGO_HOST,
    ssh_username=MONGO_USER,
    ssh_password=MONGO_PASS,
    remote_bind_address=('127.0.0.1', 27017)
)

server.start()

client = pymongo.MongoClient('127.0.0.1', server.local_bind_port)  # server.local_bind_port is assigned local port
#client = pymongo.MongoClient()
def  addRouteToDB(route):

    # print route._id
    data = route.getData()


    db = client[MONGO_DB]
    routes = db.routes

    routes.replace_one({'_id':data['_id']}, data,upsert= True)


# logging.basicConfig('log.txt', level=logging.DEBUG)

def getRoutesBanSet():
    db = client[MONGO_DB]
    routes = db.routes

    Items = routes.find({})
    retSet = set()
    for item in Items:
        retSet.add(item['_id'])
    return retSet

class Track:
    def __init__(self):
        self.url = ''
        self.routeurl = ''
        self.name = ''
        self.data = {}
    url = ''
    routeurl = ''
    name = ''
    data = {}

    def getData(self):
        data = {}
        data['name'] = self.name
        data['url'] = self.url
        data["routeurl"] =self.routeurl
        data["data"] = self.data
        return data

class Route:
    def __init__(self):
        self._id = ''
        self.tracs = []
        self.name = ''
        self.url = ''

    _id = ""
    tracs = []
    name = ''
    url = ''

    def getData(self):
        data = {}
        data['_id'] = self._id
        data['name'] = self.name
        data['url'] = self.url
        data["tracks"] =[]
        for track in self.tracs:
            data["tracks"].append(track.getData())
        return data



def getTrackByURL(link):

    track = Track()
    html = parcer.get_html(link)
    index = html.find('\'file\':\'')
    # print index
    index+=8
    curindex = index
    isFound = False
    while not isFound:
        curindex +=1
        if  html[curindex]=='\'':
            isFound = True

    trackSource = "https://biketourist.club/download/plan/" + html[index:curindex]
    print trackSource
    trackJson = parcer.get_html(trackSource)
    # print trackJson

    track.url = link
    track.data = json.loads(trackJson)

    return track


def getrouteByUrl(link):
    route = Route()
    route.url=link
    html = parcer.get_html(link)
    soup = BeautifulSoup(html, "html5lib")
    divTag = soup.find("div", {"class": "title"})
    if not divTag: return None
    route.name =  divTag.text
    if "Этот маршрут недоступен" in route.name: return None

    trackurl = soup.find("a", {"class": "button_4"})
    if not trackurl: return None

    trackurl = siteUrl + trackurl["href"]

    track = getTrackByURL(trackurl)

    track.name = route.name
    track.routeurl = route.url
    route.tracs.append(track)

    return route



def parceRoutes():
    banSet = getRoutesBanSet()
    routes = []
    for i in range (0,500):


        id = str(4000+i)
        if id in banSet: continue
        print i
        link = 'https://biketourist.club/route/'+id
        route = getrouteByUrl(link)
        if route:
            route._id = str(id)
            routes.append(route)
            addRouteToDB(route)
    return routes

routes = parceRoutes()

f = open('res.json', 'w')

data = []

for route in routes:
    data.append(route.getData())

f.write(json.dumps(data, sort_keys=False,indent=4, separators=(',', ': ')).decode('unicode-escape').encode("utf-8"))
f.close()




server.stop()

# getrouteByUrl('https://biketourist.club/route/4223')