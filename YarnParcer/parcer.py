# -*- coding: utf-8 -*-
"""
Created on Mon Jul 10 15:19:35 2017

@author: a.shiryaeva
"""




#import urllib.request
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
from pymongo import MongoClient
# from selenium import webdriver
#import time  
import os
from json import JSONEncoder
import json

from bs4 import BeautifulSoup

import urllib
import sys  

import time

import logging


WallmartUrl = 'https://www.walmart.com'


isCheckCategory = False 
isCheckSubCategory = False 
categoryList = [] 



siteUrl = "https://www.walmart.com"

def getSiteURL(url):
    if len(url) == 0: return url
    if url.find("https://")==-1:
        return siteUrl+url
    else: return url


def checkCategory(catName,_list):
    #print catName.encode(sys.stdout.encoding, errors='replace')
   
    if isCheckCategory == False: return True
    try:
        _list.index(catName)
        return True;
    except: return False
  

def checkSubCategory(catName,_list):
    #print catName.encode(sys.stdout.encoding, errors='replace')
   
    if isCheckSubCategory == False: return True
    try:
        _list.index(catName)
        return True;
    except: return False


reload(sys)  
sys.setdefaultencoding('utf8')

def removeLastLettersFromDigit(val):
    try:
        length = len(val)
        if length == 0: return ""
        lastPosition = 0
        val = val.replace(",", ".")
        #print val
        digits = "0123456789."
        for l in val:
            index = digits.find(l)
            if index == -1:
                #print val[0:lastPosition]
                return float(val[0:lastPosition])

            lastPosition+=1
            if length ==  lastPosition: return float(val[0:lastPosition])
    except:
        logging.error('\nwrong digit: '+ str(val))
        print "nwrong digit:" + str(val)
        return ""
ImageSize = "odnHeight=300&odnWidth=300&odnBg=ffffff"

def changeUrlImageSize(url):
    #print url
    try:
        position = url.find("jpeg?")
        if position == -1: return url
        firstposition = position +5;
        url = url.replace (url[firstposition:len(url)],ImageSize)
        return url

    except:
        logging.error('\nwrong image: '+ str(url))
        print "wrong image: " + str(url)
        return url

def addToDataIfNotNull(data,name,src):
    if src=="": return
    else: data[name] = src

class Good:
    image=None
    name=""
    _id=""
    barcode=""
    link=None
    productioncountry=""
    brand = ""
    composition=""
    description = ""
    price = ""
    weight = ""
    #"Nutritional value"
    proteins = ""
    fat = ""
    carbs = ""
    energy = ""
    dataDict = {}

    def downloadImage(self,path):
        #print (self.image)
        imagePath = path+"/"+self._id+".jpg"
        img = urllib.urlopen(self.image)
        localFile = open(imagePath.decode('utf-8'), 'wb')
        localFile.write(img.read())
        localFile.close()

    def getData(self):
        data = {}
        addToDataIfNotNull(data,"name", str(self.name).replace("\"", "\\\""))
        addToDataIfNotNull(data,"_id", str(self._id).replace("\"", "\\\""))
        addToDataIfNotNull(data,"barcode",  str(self.barcode).replace("\"", "\\\""))
        addToDataIfNotNull(data,"image",  str(self.image))
        addToDataIfNotNull(data,"link", str(self.link))
        addToDataIfNotNull(data,"productioncountry",str(self.productioncountry).replace("\"", "\\\""))
        addToDataIfNotNull(data,"brand", str(self.brand).replace("\"", "\\\""))
        addToDataIfNotNull(data,"composition", str(self.composition).replace("\"", "\\\""))
        addToDataIfNotNull(data,"description", str(self.description).replace("\"", "\\\""))
        addToDataIfNotNull(data,"price", self.price)
        addToDataIfNotNull(data,"weight", self.weight)
#        #"Nutritional value"
        addToDataIfNotNull(data,"proteins", self.proteins)
        addToDataIfNotNull(data,"fat", self.fat)
        addToDataIfNotNull(data,"carbs", self.carbs)
        addToDataIfNotNull(data,"energy", self.energy)
        return data




requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
def get_html(url):

    #url = 'https://e-dostavka.by/catalog/3963.html'

    headers = {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
               'X-Requested-With': 'XMLHttpRequest'


               }
    #headers = {'X-Requested-With': 'XMLHttpRequest'}
    r = requests.get(url, headers = headers, verify=False)
#    browser = webdriver.Firefox()  
#    browser.get('http://www.google.com')  
#    time.sleep(1)  
#    print (browser.title)  
#    browser.quit()
    return r.text

def get_html_subcategory(url):

    headers = {
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
    'X-Requested-With': 'XMLHttpRequest'
    }
    r = requests.get(url, headers = headers, verify=False)

    return r.text


def parseWordinList(_list,word):
    for row in _list:
        if row.strong.text.encode("utf-8")==word:
            return row.span.text.encode("utf-8")
    return ''


def parseTable(soup,propertyVal):
    res = soup.find("tr",class_=propertyVal)
    #print res
    if res == None: return ""
    res = res.find("td",class_="value").text
    return res

def parsePrice(soup,propertyVal):
    res = soup.find("div",class_=propertyVal)
   # print res
    if res == None: return ""
    price = removeLastLettersFromDigit(res.text)
    cent = removeLastLettersFromDigit(res.find("span",class_="cent").text)
    return str(int(price))+"."+str(int(cent))

def get_idFromHTML(good,link):
    html = get_html_subcategory(link)



    soup = BeautifulSoup(html,"html5lib")



    #description
    divTag = soup.find("ul", {"class": "description"})
    if divTag == None:
        logging.Error("\n Description Error"+str(soup)+"\n"+str(link))
        print "Description Error: " +str(soup)+"\n"+str(link)
    lis = divTag.find_all('li')
    good._id = parseWordinList(lis,'Артикул:')
    good.barcode = parseWordinList(lis,'Штрих-код:')
    good.productioncountry = parseWordinList(lis,'Страна производства:')
    good.weight = removeLastLettersFromDigit(parseWordinList(lis,'Масса:'))
    good.brand = parseWordinList(lis,'Торговая марка:')

    #Energy
    good.proteins = removeLastLettersFromDigit(parseTable(soup,'property_307'))
    good.fat = removeLastLettersFromDigit(parseTable(soup,'property_308'))
    good.carbs = removeLastLettersFromDigit(parseTable(soup,'property_317'))
    good.energy = removeLastLettersFromDigit(parseTable(soup,'property_313'))

    #Description
    good.description = parseTable(soup,'property_3221')
    #print "ok"
    good.composition = parseTable(soup,'property_3220')
   
    #Price
    good.price = removeLastLettersFromDigit(parsePrice(soup,'price'))

def get_goods_fromHTML(url):
    html = get_html_subcategory(url)
    soup = BeautifulSoup(html,"html5lib")
    divTag = soup.find_all("div", {"class": "form_wrapper"})
    goods = []
    #print len(divTag)
    for tag in divTag:
        good = Good()
        good.image = changeUrlImageSize(tag.find_all('img')[0].get('src'))


        #parce link
        divTitle = tag.find("div", {"class": "title"})
        #print divTitle
        good.link = divTitle.find("a").get('href').encode("utf-8")
        #print good.link
        good.name = divTitle.find("a").text.encode("utf-8")
        #print good.name
        get_idFromHTML(good,good.link)
        goods.append(good)
        #print good._id
        #print good.code

        removeLastLettersFromDigit
    return goods

#def parce_subCategory(url):
#    ret =true;
#    count = 0
#    while ret

# -*- coding: utf-8 -*-



class Category:

    def __init__(self):
        self.name = ""
        self.url = ""
        self.subcategories = []

    name = ""
    url = ""
    subcategories = []
    _id=""

    def getNameData(self):
        data = {}
        data["category"] = str(self.name).replace("\"", "\\\"")
#        data["url"] = str(self.url)
        data["subcategories"] = []
        for subcategory in self.subcategories:
            data["subcategories"].append(subcategory.getNameData())
        return data

    def getData(self):
        data = {}
        addToDataIfNotNull(data,"id", str(self._id).replace("\"", "\\\""))
        data["name"] = str(self.name).replace("\"", "\\\"")
        data["url"] = str(self.url)
        data["subcategories"] = []
        for subcategory in self.subcategories:
            data["subcategories"].append(subcategory.getData())
        return data

    def toJSON(self):
        return json.dumps(self.getData(), sort_keys=True,indent=4, separators=(',', ': ')).decode('unicode-escape').encode('utf8')
class Subcategory:
    def __init__(self):
        self.name = ""
        self.url = ""
        self.subsubcategories = []
    _id=""
    name = ""
    url = ""
    subsubcategories = []

    def getNameData(self):
        data = {}
        data["subcategory"] = str(self.name).replace("\"", "\\\"")
#        data["url"] = str(self.url)
        data["subsubcategories"] = []
        for subsubcategory in self.subsubcategories:
            data["subsubcategories"].append(subsubcategory.name)
        return data

    def getData(self):
        data = {}
        addToDataIfNotNull(data,"id", str(self._id).replace("\"", "\\\""))
        data["name"] = str(self.name).replace("\"", "\\\"")
        data["url"] = str(self.url)
        data["subsubcategories"] = []
        for subsubcategory in self.subsubcategories:
            data["subsubcategories"].append(subsubcategory.getData())
        return data

    def toJSON(self):
        return json.dumps(self.getData(), sort_keys=True,indent=4, separators=(',', ': ')).decode('unicode-escape').encode('utf8')

class Subsubcategory:
    def __init__(self):
        self.name = ""
        self.url = ""
        self.goods = []

    name = ""
    url = ""
    _id=""
    goods = []
    def downloadImage(self,path):
        for good in self.goods:
            good.downloadImage(path)

    def getData(self):
        data = {}
        addToDataIfNotNull(data,"id", str(self._id).replace("\"", "\\\""))
        data["name"] = str(self.name).replace("\"", "\\\"")
        data["url"] = str(self.url)
        data["goods"] = []
        for good in self.goods:
            data['goods'].append(good.getData())
        return data

    def toJSON(self):
        return json.dumps(self.getData(), sort_keys=True,indent=4, separators=(',', ': ')).decode('unicode-escape').encode('utf8')

class MyEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__

def parceCategories(url):
#    html = get_html(url)

    browser = webdriver.PhantomJS(os.path.abspath('modules/phantomjs-2.1.1-windows/bin/phantomjs.exe'))

    browser.get(url)

    from time import sleep # this should go at the top of the file

    sleep(1)

    html = browser.execute_script("return document.getElementsByTagName('html')[0].innerHTML")

    #print html

    soup = BeautifulSoup(html, "html5lib")
    #print html
    itemstable = soup.find('div',class_ = 'alldeps-DepartmentList')
    items = itemstable.find_all('div',class_ = 'alldeps-DepartmentLinks-superDepartment')
    categories = []
    #print len(items)

    for row in items:
        category = Category()
        category.url = getSiteURL(row.find('a')['href'].encode("utf-8"))
        category.name = row.find('a').text.encode("utf-8").translate(None, '!@#$\\:\/\"')

        print category.url
        print category.name
        subcategoryList = row.find_all('ul')
#		subcategoryList = subcategoriessoup.findChildren('li',class_=lambda x: len(x)>2 )
        #print len(subcategoryList)
        for subrow in subcategoryList:
            subcategory = Subcategory()
            subsubcategorieslist = subrow.find_all('li')
            subcategory.url = getSiteURL(subsubcategorieslist[0].find('a')['href'].encode("utf-8"))
            subcategory.name = subsubcategorieslist[0].find('a').text.encode("utf-8").translate(None, '!@#$\\:\/\"')

#			print subcategory.url 
#			print subcategory.name.encode("utf-8") 


            for i in range(1, len(subsubcategorieslist)):
                subsubrow=subsubcategorieslist[i]
                subsubcategory = Subsubcategory()
                subsubcategory.url = getSiteURL(subsubrow.find('a')['href'].encode("utf-8"))
                subsubcategory.name = subsubrow.find('a').text.encode("utf-8").translate(None, '!@#$\\:\/\"')
                print subsubcategory.url
                print subsubcategory.name.encode("utf-8")
                subcategory.subsubcategories.append(subsubcategory)
                #return
            category.subcategories.append(subcategory)
        categories.append(category)
    return categories


def getDictValue(dictionary,item):
    if not isinstance(dictionary, dict): return ''
    if item in dictionary: return dictionary[item]
    else: return ''

def getGoodsFromJson(items):
    goods = []
    for item in items:
        good = Good()
        good.link = WallmartUrl + item["productPageUrl"]
        good.image = changeUrlImageSize(item["imageUrl"])
        good.description = getDictValue(item,"description")
        good.barcode = getDictValue(item,"upc")
        good._id = getDictValue(item,"usItemId")

        if "offerPrice" in item["primaryOffer"]:
            good.price = getDictValue(item["primaryOffer"],"offerPrice")
        else:
            good.price = getDictValue(item["primaryOffer"],"minPrice")
        good.name = getDictValue(item,'title')
        goods.append(good)
    return goods


def returnGoodsWithoutDublicates(goodsset,goods):
    if len(goods) == 0: return []
    retGoods = []
    for good in goods:
        if good._id in goodsset:
            continue
        else:
            goodsset.add(good._id)
            retGoods.append(good)
    return retGoods

def parseSubSubCategory(subsubrow):
# old    
    minPrice = 0
    maxPrice = 1000000


    goods = []
    curGoods = []
    idSet = set()

    lastMinPrice = -1
    process = True
    lastPrice = 0
    maxQuantity = 0




    url = 'https://www.walmart.com/search/api/preso?cat_id='+subsubrow._id+'&prg=desktop'
#        print url
    listJson = get_html(url)
    listJson = json.loads(listJson)
    requestContext = listJson['requestContext']
    itemCount = requestContext['itemCount']
    pageSize = itemCount['pageSize']
    total = itemCount['total']
   
    pagecount = 0
    maxQuantity = max(maxQuantity,total)


    if (total%pageSize)==0:
        pagecount = int(total/pageSize)
    else:
        pagecount = int(total/pageSize)+1
    curGoods = getGoodsFromJson(listJson['items'])

    if pagecount>26:
        process= True
        pagecount = 26
    else: process = False


    goods.extend(returnGoodsWithoutDublicates(idSet,curGoods))

    for i in range(2,pagecount+1):
        url ='https://www.walmart.com/search/api/preso?cat_id='+subsubrow._id +'&prg=desktop&page='+str(i)
        listJson = get_html(url)
        listJson = json.loads(listJson)
        res = getGoodsFromJson(listJson['items'])
        goods.extend(returnGoodsWithoutDublicates(idSet,res))

    while process:


#        print 'min ' + str(minPrice)
#        print 'lastPrice ' + str(lastPrice) 

        if lastMinPrice >= lastPrice:
            lastPrice+=1

        lastMinPrice = lastPrice

        minPrice = lastPrice

        url = 'https://www.walmart.com/search/api/preso?cat_id='+subsubrow._id+'&prg=desktop&max_price='+str(maxPrice)+'&min_price='+str(minPrice)+'&sort=price_low'
#        print url
        listJson = get_html(url)
        listJson = json.loads(listJson)
        requestContext = listJson['requestContext']
        itemCount = requestContext['itemCount']
        pageSize = itemCount['pageSize']
        total = itemCount['total']
        pagecount = 0
        maxQuantity = max(maxQuantity,total)


        if (total%pageSize)==0:
            pagecount = int(total/pageSize)
        else:
            pagecount = int(total/pageSize)+1
        curGoods = getGoodsFromJson(listJson['items'])

        if pagecount>26:
            process= True
            pagecount = 26
        else: process = False


        goods.extend(returnGoodsWithoutDublicates(idSet,curGoods))
#        print itemCount['total']

        for i in range(2,pagecount+1):
            url ='https://www.walmart.com/search/api/preso?cat_id='+subsubrow._id+'&max_price='+str(maxPrice)+'&min_price='+str(minPrice)+'&sort=price_low' +'&prg=desktop&page='+str(i)
            listJson = get_html(url)
            listJson = json.loads(listJson)
#            print listJson
            res = getGoodsFromJson(listJson['items'])

            summ = 0
            count = 0
            for good in res:
                if not good.price=='':
                    summ += float(good.price)
                    count += 1
            if count>0:
                lastPrice = int(summ/count)
            goods.extend(returnGoodsWithoutDublicates(idSet,res))
    subsubrow.goods = goods

    return len(subsubrow.goods), maxQuantity - len(subsubrow.goods)
#    isEmpty = False
#    curIndex = 1;
#    while not isEmpty:
#        url = subsubrow.url+'?lazy_steep='+str(curIndex)
#        curIndex =curIndex+1
#        #print url
#        goods = get_goods_fromHTML(url)
#        if len(goods)>0:
#            subsubrow.goods.extend(goods)
#        else:
#            isEmpty= True

#new
#    url = subsubrow.url
#    while not url=="":
#        goods = get_goods_fromHTML(url)
#        for good in goods:
#            subsubrow.goods = subsubrow.goods

def createDir(dirName):
    #print dirName
    if not os.path.exists(dirName.decode("utf-8") ):
        os.makedirs(dirName.decode("utf-8") )




def addGoodToDB(good):
   
    client = MongoClient()
    db = client.wallmart
    goods = db.goods
    if goods.goods.find_one({'_id':good['_id']})!=None: print "db remove copy" + good['_id']
    goods.replace_one({'_id':good['_id']}, good,upsert= True)

   


def parseGoods():
    url = 'https://e-dostavka.by/'
    categories = parceCategories(url)

    logging.info('\n\t\t\t------start------\n'+ time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()))
#    parseSubSubCategory(categories[0].subcategories[0].subsubcategories[0])
#    print len(categories[0].subcategories[0].subsubcategories[0].goods)
    f = open('Folder structure.txt', 'w')
    mainPath = os.getcwd()+'\\'+'parse files'
    createDir(mainPath)
    rowNum = 0
    subrowNum = 0
    subsubrowNum = 0
    catdata = []

    begin_time = time.time()
    curtime = time.time()

    goodsCount = 0

    for row in categories:
        if checkCategory(row.name,categoryList):
            rowNum = rowNum+1
            curcategoryName = row.name.translate(None, '!@#$\\:\/\"')
            cursubcategoryPath = mainPath+'\\'+curcategoryName
            createDir(cursubcategoryPath)
            s =row.name+' '+row.url +'\n'
            f.write(s )
            for subrow in row.subcategories:
                if not checkSubCategory(subrow.name,categoryList): continue
                subrowNum = subrowNum+1
                cursubcategoryName = subrow.name.translate(None, '!@#$\\:\/\"')
                cursubsubcategoryPath = cursubcategoryPath+'\\'+cursubcategoryName
                createDir(cursubsubcategoryPath)
                s ='\t'+subrow.name+' '+subrow.url +'\n'

                f.write(s )

                for subsubrow in subrow.subsubcategories:
                    subsubrowNum = subsubrowNum+1
                    print "parse ["+str(rowNum)+":"+str(len(categories))+"]"+ "["+str(subrowNum)+':'+str(len(row.subcategories))+"]"+"["+str(subsubrowNum)+":"+str(len(subrow.subsubcategories))+"]"
                    logging.debug("parse ["+str(rowNum)+":"+str(len(categories))+"]"+ "["+str(subrowNum)+':'+str(len(row.subcategories))+"]"+"["+str(subsubrowNum)+":"+str(len(subrow.subsubcategories))+"]" )
                    curtime=time.time()
                    cursubsubcategoryName = subsubrow.name.translate(None, '!@#$\\:\/\"')
                    cursubsubsubcategoryPath = cursubsubcategoryPath+'\\'+cursubsubcategoryName
                    createDir(cursubsubsubcategoryPath)



                    print subsubrow.name.encode(sys.stdout.encoding, errors='replace')
                    logging.debug(subsubrow.name)
                    s ='\t\t'+subsubrow.name+' '+subsubrow.url +'\n'
                    f.write(s )
                    parseSubSubCategory(subsubrow)
                    infoabsolutePath = cursubsubsubcategoryPath+"\\info.json"
                    js = open(infoabsolutePath.decode("utf-8"), 'w')

                    subsubrow.downloadImage(cursubsubsubcategoryPath)
                    js.write(subsubrow.toJSON().encode("utf-8"))
                    js.close()

                    for gdb in subsubrow.goods:
                        gdbdata = gdb.getData()
                        gdbdata['category'] = row.name
                        gdbdata['subcategory'] = subrow.name
                        gdbdata['subsubcategory'] = subsubrow.name
                        addGoodToDB(gdbdata)
                    goodsCount+=len(subsubrow.goods)

                    print "parsing time: "+str(time.time()-curtime)
                    print "goods count: "+str(len(subsubrow.goods))
                    logging.info(time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()))
                    logging.debug("parsing time: "+str(time.time()-curtime))
                    print "total time: "+str(time.time()-begin_time)
                    print "total goods: "+str(goodsCount)
                    #return
                subsubrowNum = 0

            subrowNum = 0
        catdata.append(row.getData())
    f.closed
    logging.info("goods count: "+str(goodsCount))
    print "parsing end: " + str(time.time()-begin_time)
    print "goods count: "+str(goodsCount)
    logging.info(time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()))
    logging.debug("parsing time: "+str(time.time()-curtime))
    js = open('info.json', 'w')
    js.write(json.dumps(catdata, sort_keys=True,indent=4, separators=(',', ': ')).decode('unicode-escape').encode("utf-8"))
    js.close()





def CreateFolderStructureFile(categories,filename):
    f = open(filename, 'w')
    categoriesnames = []
    for row in categories:
        categoriesnames.append(row.getNameData())
    f.write(json.dumps(categoriesnames, sort_keys=True,indent=4, separators=(',', ': ')).decode('unicode-escape').encode("utf-8"))

def CreateInfoFile(categories,filename):
    f = open(filename, 'w')
    categoriesnames = []
    for row in categories:
        categoriesnames.append(row.getData())
    f.write(json.dumps(categoriesnames, sort_keys=True,indent=4, separators=(',', ': ')).decode('unicode-escape').encode("utf-8"))
    f.close()

def CreateConfigFile(categories,filename):
    f = open(filename, 'w')
    f.write('#Config File for parser\n#LEGEND:\n#\t\'\' - category\n#\t\'\\t\' - subcategory\n#\t\'\\t\\t\' - subsubcategory\n#\'#\' - comment\n ')
    for row in categories:


        f.write(''+row.name +'#\t'+  row.url+ '\n')
        for srow in row.subcategories:
            f.write('\t'+srow.name + '#\t'+ srow.url +'\n')

            for ssrow in srow.subsubcategories:
                f.write('\t\t'+ssrow.name +'#\t'+  ssrow.url+ '\n')


    f.close()


def GenerateTaxonomyWallmart(filename):
    url = 'http://api.walmartlabs.com/v1/taxonomy?format=json&apiKey=an33t7m99xzfjzrf5gm6syb9'
    taxonomy = get_html(url)
    tax_obj = json.loads(taxonomy)
    for category in tax_obj['categories']:
        print category['name']
        category['url'] = GetWallmartCategoryUrl(category['id'])
        for subcategory in category['children']:
            print "\t"+subcategory['name']
            subcategory['url'] = GetWallmartCategoryUrl(subcategory['id'])
            if not 'children' in subcategory.keys():
                continue
            for subsubcategory in subcategory['children']:
                subsubcategory['url'] = GetWallmartCategoryUrl(subsubcategory['id'])
                print "\t\t"+subsubcategory['name']


    f = open(filename, 'w')
    f.write(json.dumps(tax_obj, sort_keys=False,indent=4, separators=(',', ': ')).decode('unicode-escape').encode("utf-8"))
    f.close()

def GetWallmartCategoryUrl(_id):
    return 'https://www.walmart.com/browse/' + _id

def LoadTaxonomyWallmart(filename):
    banSet = getCategoryBanSet()
    f = open(filename, 'r')
    taxonomy = f.read()
    tax_obj = json.loads(taxonomy)
    categories = [];
    for category in tax_obj['categories']:
#        print category['name']

        categoryObj = Category()
        categoryObj._id = category['id']
        categoryObj.url = GetWallmartCategoryUrl(category['id'])
        categoryObj.name = category['name']

        for subcategory in category['children']:
#            print "\t"+subcategory['name']
            subCategoryObj = Subcategory()
            subCategoryObj._id = subcategory['id']
            subCategoryObj.url = GetWallmartCategoryUrl(subcategory['id'])
            subCategoryObj.name = subcategory['name']
            if not 'children' in subcategory.keys():
                subSubCategoryObj = Subsubcategory()
                subSubCategoryObj._id = subcategory['id']
                subSubCategoryObj.url = GetWallmartCategoryUrl(subcategory['id'])
                subSubCategoryObj.name = subcategory['name']

                if subSubCategoryObj.name in banSet: continue
                subCategoryObj.subsubcategories.append(subSubCategoryObj)
#                print "\t\t"+subcategory['name']
                continue
            for subsubcategory in subcategory['children']:
                subSubCategoryObj = Subsubcategory()
                subSubCategoryObj._id = subsubcategory['id']
                subSubCategoryObj.url = GetWallmartCategoryUrl(subsubcategory['id'])
                subSubCategoryObj.name = subsubcategory['name']

                if subSubCategoryObj.name in banSet: continue
                subCategoryObj.subsubcategories.append(subSubCategoryObj)
#                print "\t\t"+subsubcategory['name']
            categoryObj.subcategories.append(subCategoryObj)
        categories.append(categoryObj)
    return categories



#categories = parceCategories(url)

def getSubSubCategorieQuantity(categories):

    # print "getSubSubCategorieQuantity: " + str(len(categories))
    subSubCategoriesQuantity = 0
    for row in categories:
        for subrow in row.subcategories:
            for subsubrow in subrow.subsubcategories:
                subSubCategoriesQuantity+=1
    return   subSubCategoriesQuantity


def  getCategoryBanSet():
    client = MongoClient()
    db = client.wallmart
    subsubcategories = db.subsubcategories
    Items = subsubcategories.find({})
    retSet = set()
    for item in Items:
        retSet.add(item['name'])
    return retSet

def  getImageBanSet():
    client = MongoClient()
    db = client.wallmart
    goods = db.goods
    goodsItems = goods.find({})
    retSet = set()
    for item in goodsItems:
        retSet.add(item['_id'])
    return retSet

def  addSubSubCategoryToDB(subsubrow,curTotal,curLoss):


    data = {}
    data['_id'] = subsubrow._id
    data['name'] = subsubrow.name
    data['url'] = subsubrow.url
    data['total'] = curTotal
    data['loss'] = curLoss

    client = MongoClient()
    db = client.wallmart
    subsubcategories = db.subsubcategories


    subsubcategories.replace_one({'_id':data['_id']}, data,upsert= True)

def parseCategories(mainPath,categories,first = 0, last = -1, processnum = 0):
    global logging
#    logging = reload(logging)
    PATH = os.getcwd()
    logging.basicConfig(filename=PATH + '/usage_process_'+str(processnum)+'.log',level=logging.DEBUG)
    logging.info('\n\t\t\t------start------\n'+ time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()))
    if last == -1:
        last = getSubSubCategorieQuantity(categories)
    subSubCategoriesIndex = -1

    categoryBanSet = getCategoryBanSet()
    imageBanSet = getImageBanSet()
    progress = 0
    progressCount = last - first

    total = 0
    loss = 0

    start = time.time()
    curtime = time.time()
    for row in categories:
        for subrow in row.subcategories:
            for subsubrow in subrow.subsubcategories:
                subSubCategoriesIndex+=1;
                if not (subSubCategoriesIndex>=first and subSubCategoriesIndex<last):
                    continue
                progress +=1
                status = 'Parsing progress: ['+str(processnum)+'] ' + str(progress) + '/' + str(progressCount) +' ' + subsubrow.name + " Total time: "+str(time.time()-start)
                print status
                logging.info(status)
                if subsubrow.name in categoryBanSet:
                    status = 'Parsing progress: ['+str(processnum)+'] ' + ' skip'
                    print status
                    logging.info(status)
                    continue
                ##Create Folders
                curcategoryName = str(row.name).translate(None, '!@#$\\:\/\"')
                cursubcategoryPath = mainPath+'/'+curcategoryName
                createDir(cursubcategoryPath)

                cursubcategoryName = str(subrow.name).translate(None, '!@#$\\:\/\"')
                cursubsubcategoryPath = cursubcategoryPath+'/'+cursubcategoryName
                createDir(cursubsubcategoryPath)

                cursubsubcategoryName = str(subsubrow.name).translate(None, '!@#$\\:\/\"')
                cursubsubsubcategoryPath = cursubsubcategoryPath+'/'+cursubsubcategoryName
                createDir(cursubsubsubcategoryPath)

                ##parseSubSubCategory
                curTotal, curLoss = parseSubSubCategory(subsubrow)

                total+=curTotal
                loss+= curLoss

                status = 'Parsing progress: ['+str(processnum)+'] '+'total count: ' + str(curTotal)
                print status
                logging.info(status)
                status = 'Parsing progress: ['+str(processnum)+'] '+'loss count: ' + str(curLoss)
                print status
                logging.info(status)

#                add to db

                for gdb in subsubrow.goods:
                        if gdb._id in imageBanSet:
                            continue
                        gdbdata = gdb.getData()
                        gdbdata['category'] = row.name
                        gdbdata['subcategory'] = subrow.name
                        gdbdata['subsubcategory'] = subsubrow.name
                        gdb.downloadImage(cursubsubsubcategoryPath)
                        addGoodToDB(gdbdata)
                addSubSubCategoryToDB(subsubrow,curTotal,curLoss)

    status = 'Progress: ['+str(processnum)+'] ' + 'end' + "parsing time: "+str(time.time()-curtime)
    print status
    logging.info(status)
    
    status = 'Parsing progress: ['+str(processnum)+'] '+ 'end' + 'total count: ' + str(total)
    print status
    logging.info(status)
    
    status = 'Parsing progress: ['+str(processnum)+'] '+ 'end' + 'loss count: ' + str(loss)
    print status
    logging.info(status)
#                subsubrow.downloadImage(cursubsubsubcategoryPath)
#                f = open("test.json", 'w')
#                f.write(json.dumps(subsubrow.getData(), sort_keys=False,indent=4, separators=(',', ': ')).decode('unicode-escape').encode("utf-8"))  
#                f.close()      


def generateFolderStructure(categories,mainPath,first = 0, last = -1 ):
    if last == -1:
        last = getSubSubCategorieQuantity(categories)
    subSubCategoriesIndex = -1

    createDir(mainPath)
    for row in categories:
        for subrow in row.subcategories:
            for subsubrow in subrow.subsubcategories:
                subSubCategoriesIndex+=1;
                if not (subSubCategoriesIndex>=first and subSubCategoriesIndex<last):


                    curcategoryName = str(row.name).translate(None, '!@#$\\:\/\"')
                    cursubcategoryPath = mainPath+'/'+curcategoryName
                    createDir(cursubcategoryPath)

                    cursubcategoryName = str(subrow.name).translate(None, '!@#$\\:\/\"')
                    cursubsubcategoryPath = cursubcategoryPath+'/'+cursubcategoryName
                    createDir(cursubsubcategoryPath)

                    cursubsubcategoryName = str(subsubrow.name).translate(None, '!@#$\\:\/\"')
                    cursubsubsubcategoryPath = cursubsubcategoryPath+'/'+cursubsubcategoryName
                    createDir(cursubsubsubcategoryPath)




#GenerateTaxonomyWallmart("taxonomy_wallmart.json")

#print getSubSubCategorieQuantity(categories)



def getParseParameters(processCount,length):

    print "subdirectory count: " + str(length)
    print "process count: " + str(processCount)

    parceParametersLength = int(length/processCount)
    Parseparameters = []
    for i in range(0,processCount):
        minmax = [i*parceParametersLength,(i+1)*parceParametersLength]
        Parseparameters.append(minmax)
        print minmax

    if not (length%processCount)==0:
        minmax = [processCount*parceParametersLength,length]
        Parseparameters.append(minmax)
        print minmax
    return Parseparameters


from multiprocessing import Process   

def multiProcessParsing (processCount, fileName):
    categories = LoadTaxonomyWallmart(fileName)
    processes = []
    parseParameters = getParseParameters(processCount,getSubSubCategorieQuantity(categories))

    for i in range(0,len(parseParameters)):
        time.sleep(1)
        p = Process(target=parseCategories, args=('Wallmart parse goods',categories,parseParameters[i][0],parseParameters[i][1],i))
        p.start()
        processes.append(p)
    k = 0
    for p in processes:
        p.join()
        print 'process ' + str(k) + ' end '
        k+=1



if __name__ == "__main__":
    ind = 0
    path = 0
    processCount = 0
    print "usage Example: python \"parcer Wallmart.py\" \"taxonomy_wallmart.json\" 10"
    for param in sys.argv:

        if ind ==1:
            path = param
            # print path

        if ind == 2:
            # print param
            processCount = int(param)
            multiProcessParsing(processCount,path)
        ind+=1


startItem = 1
endItem = 125































# categories = LoadTaxonomyWallmart("taxonomy_wallmart.json")
# generateFolderStructure(categories,"Wallmart parse goods",startItem,endItem)

# parseCategories("Wallmart parse goods",categories,startItem,endItem)


# parseCategories("Wallmart parse goods",categories,startItem)
# CreateInfoFile(categories,"info.json")

# CreateFolderStructureFile(categories,"structure.json")
# CreateInfoFile(categories,"info.json")
# CreateConfigFile(categories,"wallmart_conf.txt")


# browser = webdriver.Chrome()
#browser.get(url)  
#time.sleep(1)  
#print (browser.title)  
#browser.quit()	

#System.setProperty("webdriver.gecko.driver", "C:\\Users\\v.triputin\\Documents\\Projects\\goodsParser\\modules");


#browser = webdriver.PhantomJS(os.path.abspath('modules/phantomjs-2.1.1-windows/bin/phantomjs.exe'))
#browser = webdriver.Firefox()
#browser.get(url)  
#
#from time import sleep # this should go at the top of the file
#
#sleep(15)
#
#html = browser.execute_script("return document.getElementsByTagName('html')[0].innerHTML")
#print html
 
#chromedriver = "/Users/adam/Downloads/chromedriver"
#os.environ["webdriver.chrome.driver"] = chromedriver
#driver = webdriver.Chrome(chromedriver)
#driver.get("http://stackoverflow.com")
#driver.quit()	

#url = "<a href=\"https://e-dostavka.by/catalog/item_668516.html\" class=\"fancy_ajax\">Язык отварной, 1 кг., <span>фасовка 0.8&nbsp;-&nbsp;1 кг</span></a>"
#soup = BeautifulSoup(url, "html5lib")
#	#print html
#itemstable = soup.find('ul',class_ = 'catalog_menu catalog_menu_visible')
#print soup.find("a").get('href').encode("utf-8")
#print soup.find("a").text


#parseGoods();

#g = Good()
#gg = Good()
#ggg = Good()
#
#g.link = 1
#gg.link = 1
#ggg.link = 2
#
#s = set([g,gg,ggg])
#print len(s)
#from bson.objectid import ObjectId
#
#search = { 'index': 123 }
#insert = {'index': 123,'name' : 'AN22Y' }
#'_id': '234'
#}
#
#


#client = MongoClient()
#db = client.euroopt5
#goods = db.goods
##print 'res:'
##if goods.find_one({"_id":123})!=None: print "db remove copy" 
##
##print goods.find_one({"_id":5002})
#
#
#goods.update_many({}, { "$rename" : { "categorecory" : "category" }})

#client = MongoClient()
#db = client.base2113
#goods = db.goods
#post = {"price": "2"}

#print goods.find_one()

#test = "https://img.e-dostavka.by/UserFiles/images/catalog/Goods/thumbs/4810/4810287010717_190x190.png.jpg?1494937006"
#test2 = "https://img.e-dostavka.by/UserFiles/images/catalog/Goods/thumbs/4810/4810287010717_60x60.png.jpg?1494937006"

#print changeUrlImageSize("https://img.e-dostavka.by/UserFiles/images/catalog/Goods/thumbs/4607/4607045216340_190x190.png.jpg?1497011395")

#print removeLastLettersFromDigit("")
#print changeUrlImageSize(test2)

#subsubrow = Subsubcategory()
#subsubrow.url = "https://e-dostavka.by/catalog/4541.html"
#parseSubSubCategory(subsubrow)
#infoabsolutePath = "info.json"
#js = open(infoabsolutePath.decode("utf-8"), 'w')
#subsubrow.downloadImage('.')
#js.write(subsubrow.toJSON())
#js.close()	

#print os.getcwd()
#def test(val):
#    val.x = 4
#
#    def __init__(self):
#        self.y = 0
#x = [n(),n()]
#
#for i in x:
#    test(i)
#print x[1].x