#!/usr/bin/env python
# -*- coding: utf-8 -*-
import parcer


from json import JSONEncoder
import json
import logging


from bs4 import BeautifulSoup


siteUrl = "https://veloby.net/"
routeUrl = "https://veloby.net/node/"




import time
from sshtunnel import SSHTunnelForwarder
import pymongo
import pprint

MONGO_HOST = "78.46.175.237"
MONGO_DB = "veloby"
MONGO_USER = "root"
MONGO_PASS = "V3682450v"

server = SSHTunnelForwarder(
    MONGO_HOST,
    ssh_username=MONGO_USER,
    ssh_password=MONGO_PASS,
    remote_bind_address=('127.0.0.1', 27017)
)



# client = pymongo.MongoClient('127.0.0.1', server.local_bind_port)  # server.local_bind_port is assigned local port
# server.start()

client = pymongo.MongoClient()
def  addRouteToDB(route):

    print route._id
    data = route.getData()


    db = client[MONGO_DB]
    routes = db.routes

    routes.replace_one({'_id':data['_id']}, data,upsert= True)


# logging.basicConfig('log.txt', level=logging.DEBUG)

def  addBanLinkBanIdToDB(linkbanId):


    data = {"_id":linkbanId}


    db = client[MONGO_DB]
    routes = db.linkban

    routes.replace_one({'_id':data['_id']}, data,upsert= True)

def getRoutesBanSet():
    db = client[MONGO_DB]
    routes = db.routes

    Items = routes.find({})
    retSet = set()
    for item in Items:
        retSet.add(item['_id'])
    return retSet




def getLinkBanSet():
    db = client[MONGO_DB]
    routes = db.linkban

    Items = routes.find({})
    retSet = set()
    for item in Items:
        retSet.add(item['_id'])
    return retSet

class Track:
    def __init__(self):
        self.url = ''
        self.routeurl = ''
        self.name = ''
        self.data = {}
    url = ''
    routeurl = ''
    name = ''
    downloadUrl = ''
    data = {}

    def getData(self):
        data = {}
        data['name'] = self.name
        data['url'] = self.url
        data["routeurl"] =self.routeurl
        data["data"] = self.data
        if not self.downloadUrl=="":
            data["downloadUrl"] = self.downloadUrl
        return data

class Route:
    def __init__(self):
        self._id = ''
        self.tracs = []
        self.name = ''
        self.url = ''

    _id = ""
    tracs = []
    name = ''
    url = ''

    def getData(self):
        data = {}
        data['_id'] = self._id
        data['name'] = self.name
        data['url'] = self.url
        data["tracks"] =[]
        for track in self.tracs:
            data["tracks"].append(track.getData())
        return data


import xmltodict
def getTrackByURL(link):

    link = link.replace('http://www.gpsies.com/map.do',"https://www.gpsies.com/download.do")
    track = Track()
    html = parcer.get_html(link)
    data1 = ""
    try:
        data1 = xmltodict.parse(html)
    except: return track

    track = Track()
    track.downloadUrl = link
    trackJson = json.dumps(data1, sort_keys=False, indent=4, separators=(',', ': '))
    trackJson = trackJson.replace("\"@", "\"")
    # print trackJson

    track.data = json.loads(trackJson)

    return track


def getrouteByUrl(link):
    route = Route()
    route.url=link
    route.name = "no name"

    tracsSet = set()
    html = parcer.get_html(link)
    soup = BeautifulSoup(html, "html5lib")
    name = soup.find("h1")

    if name:
        route.name = name.text
    # print html

    # gps_track_link = soup.find_all(text="\n              GPSies \u0442\u0440\u0435\u043a(\u0438):\xa0").parent.findNext('a').href
    gps_track_link = None

    refs = soup.find_all('a', href=True)
    for ref in refs:
        if "http://www.gpsies.com" in ref["href"]:
            print link
            print ref["href"]
            gps_track_link = ref["href"]

            if gps_track_link in tracsSet: continue

            tracsSet.add(gps_track_link)
            track = getTrackByURL(gps_track_link)
            track.url = gps_track_link
            # if route.name: return None

            # trackurl = soup.find("a", {"class": "button_4"})
            # if not trackurl: return None

            # trackurl = siteUrl + trackurl["href"]

            # track = getTrackByURL(trackurl)




            track.name = route.name
            track.routeurl = route.url
            route.tracs.append(track)



    # divs = soup.find_all("div", {"class": "field-label-inline-first"})
    # for div in divs:
    #     if "GPSies" in div.text:
    #         print link
    #         print div.findNext('a')["href"]
    #         gps_track_link = div.parent.findNext('a')["href"]
    #         break
    # gps_track_link = ""
    # print gps_track_link
    if not gps_track_link: return None


    return route



def parceRoutes(startIndex,endIndex,arr,process):
    # print startIndex
    # print endIndex
    # print process
    length = endIndex-startIndex
    # banSet = getRoutesBanSet()

    routes = []
    for i in range (startIndex,endIndex):


        id = arr[i]

        print "process ["+str(process)+"] "+ str(i-startIndex) + "/" + str(length) + "(" + id + ")"
        link = routeUrl+id
        route = getrouteByUrl(link)
        if route:
            route._id = str(id)
            routes.append(route)
            addRouteToDB(route)
        addBanLinkBanIdToDB(id)
    return routes


from multiprocessing import Process


def getParseParameters(processCount, arr):

    length = len(arr)
    print "subdirectory count: " + str(length)
    print "process count: " + str(processCount)

    parceParametersLength = int(length/processCount)
    Parseparameters = []
    for i in range(0,processCount):
        minmax = [i*parceParametersLength,(i+1)*parceParametersLength]
        Parseparameters.append(minmax)
        print minmax

    if not (length%processCount)==0:
        minmax = [processCount*parceParametersLength,length]
        Parseparameters.append(minmax)
        print minmax
    return Parseparameters


def getLinkArray(start,end):
    banSet = getLinkBanSet()
    retArr = []
    for i in range(start,end):
        if not str(i) in banSet:
            retArr.append(str(i))
    return retArr


def multiProcessParsing (processCount, startItem, endItem):
    processes = []
    arr = getLinkArray(startItem, endItem)
    parseParameters = getParseParameters(processCount, arr)

    for i in range(0,len(parseParameters)):
        parcer.time.sleep(1)
        p = Process(target=parceRoutes, args=(parseParameters[i][0],parseParameters[i][1],arr,i))
        p.start()
        processes.append(p)
    k = 0
    for p in processes:
        p.join()
        print 'process ' + str(k) + ' end '
        k+=1


import sys
isMulti = False
if __name__ == "__main__":
    startInex = 0
    endIndex = 0
    processCount = 0
    ind = 0

    startTime = time.time()

    print "Example: 0 1000 10"
    for param in sys.argv:

        if ind ==1:
            startInex = int(param)
            # print path

        if ind == 2:
            # print param
            endIndex = int(param)


        if ind == 3:
            # print param
            processCount = int(param)
            multiProcessParsing(processCount, startInex, endIndex)
            isMulti = True
        ind+=1

    if not isMulti:
        startItem = 1
        endItem = 125

        routesidx = list(getRoutesBanSet())
        routes = parceRoutes(945,len(routesidx),routesidx,0)

        f = open('res.json', 'w')

        data = []

        for route in routes:
            data.append(route.getData())

        f.write(json.dumps(data, sort_keys=False,indent=4, separators=(',', ': ')).decode('unicode-escape').encode("utf-8"))
        f.close()

    print "Parcing time: " + str(time.time() - startTime)

# server.stop()

# getrouteByUrl('https://biketourist.club/route/4223')