#!/usr/bin/env python
# -*- coding: utf-8 -*-
import parcer


from json import JSONEncoder
import json
import logging


from bs4 import BeautifulSoup


siteUrl = "http://forum.poehali.net/"
routeUrl = "http://forum.poehali.net/index.php?board=12"



import os
import time
from sshtunnel import SSHTunnelForwarder
import pymongo
import pprint
import urllib

MONGO_HOST = ""
MONGO_DB = "Hooked"
MONGO_USER = ""
MONGO_PASS = ""

client = pymongo.MongoClient()


def getStoriesDB():
    db = client[MONGO_DB]
    routes = db.stories

    return routes.find({})

def getStoryStatisticDB():
    db = client[MONGO_DB]
    routes = db.storyStatistic

    return routes.find({})

def getSeriesDB():
    db = client[MONGO_DB]
    routes = db.series

    return routes.find({})

def  addstoryToDB(story):
    data = story
    db = client[MONGO_DB]
    routes = db.storiesSimple

    routes.replace_one({'_id':data['_id']}, data,upsert= True)

def  addepisodeToDB(episode):
    data = episode
    db = client[MONGO_DB]
    routes = db.episodesSimple

    routes.replace_one({'_id':data['_id']}, data,upsert= True)

def getStory(storydb):
    story = dict()
    story["_id"] = storydb["_id"]
    try:
        story["name"] = storydb["stories"][0]["seriesTitle"]
    except:
        return False
    try:
         story["tags"] = {storydb["stories"][0]["genre"].lower():True}
    except:
        story["tags"] = None
    story["image"] = getCoverImageFile(storydb["stories"])
    story["episodes"] = []

    episodes = storydb["stories"]
    for episode in episodes:
        ep = dict()
        try:
            ep["description"] = episode["storyDescription"]
        except:
            ep["description"] = episode["title"]
        try:
            ep["number"] = episode["episodeIndex"]
            ep["id"] = episode["objectId"]
            story["episodes"].append(ep)
        except:
            print episode
    story["episodes"] = sorted(story["episodes"], key=lambda k: k["number"])
    story["minuserlevel"] = 'free'
    story["source"] = 'HookedExport'
    story["hidden"] = False
    return story

def getCoverImageFile(episodesDB):
    image = None
    for ep in episodesDB:
        try:
            image = ep["objectId"] +"/" + ep["coverImageFile"]["name"] + ".jpg"
            return image
        except:
            pass
    return None

def getImageFile(epId,msg):
    try:
        image = epId + "/" + msg["imageFile"]["name"] + ".jpg"
        return image
    except:
        return None


def getEpisode(epdb):
    episode = dict()
    episode["_id"] = epdb["objectId"]
    episode["people"] = []
    episode["messages"] = []
    try:
        episode["number"] = epdb["episodeIndex"]
    except:
        return False
    try:
        episode["description"] = epdb["storyDescription"]
    except:
        episode["description"] = epdb["title"]
    episode["storyId"] = epdb["seriesIdentifier"]

    try:
        messages = epdb["messages"]
        for mesDB in messages:
            episode["messages"].append(getMessage(mesDB))

        peolple = epdb["peoples"]
        for perDB in peolple:
            pers = getPerson(perDB)
            if pers:
                 episode["people"].append(pers)
    except:
        print epdb
    return episode

def getMessage(mesDB):
    message = dict()

    try:
        message["personId"] = mesDB["sender"]["objectId"]
    except:
       pass
    try:
         message["body"] = mesDB["text"]
    except:
        pass

    try:
        mesDB["isTypingIntroDuration"] > 1
        message["typing"] = False

    except:
        message["typing"] = False

    image = getImageFile(mesDB["story"]["objectId"],mesDB)
    if image:
        message["image"] = image

    return message

def getPerson(perDB):
    person = dict()

    try:
        person["personId"] = perDB["objectId"]
        person["name"] = perDB["name"]
    except:
        return None

    return person

def tryToGet(var, val):
    try:
        var[val]
        return  var[val]
    except:
        return 0

def getStatistic(epDB):
    statistic = dict()

    statistic["episodeIndex"] = tryToGet(epDB,"episodeIndex")
    statistic["loveRatio"] = tryToGet(epDB,"loveRatio")
    statistic["episodeCount"] = tryToGet(epDB,"episodeCount")
    statistic["completed_android"] = tryToGet(epDB,"completed_android")
    statistic["readVelocity"] = tryToGet(epDB,"readVelocity")
    statistic["updatedAt"] = tryToGet(epDB,"updatedAt")
    statistic["completed_ios"] = tryToGet(epDB,"completed_ios")
    statistic["seriesTitle"] = epDB["seriesTitle"]
    statistic["readCount"] = tryToGet(epDB,"readCount")
    statistic["risk"] = tryToGet(epDB,"risk")
    statistic["started_android"] = tryToGet(epDB,"started_android")
    statistic["loveCount"] = tryToGet(epDB,"loveCount")
    statistic["commentRatio"] =  tryToGet(epDB,"commentRatio")
    statistic["started_ios"] = tryToGet(epDB,"started_ios")
    statistic["genre"] = tryToGet(epDB,"genre")
    statistic["previousReadVelocity"] = tryToGet(epDB,"previousReadVelocity")
    statistic["readAcceleration"] = tryToGet(epDB,"readAcceleration")
    statistic["createdAt"] = epDB["createdAt"]
    statistic["searchable"] = epDB["searchable"]
    statistic["sentimentCount"] = tryToGet(epDB,"sentimentCount")

    return statistic

PathDirectory = "hookedContent"
ExportDirectory = "hookedExportContent"

def CreateExportStoryPath(storyName):
    parcer.createDir(PathDirectory)
    mainPath = os.path.dirname(os.path.abspath(__file__))+"/"+ExportDirectory
    curcategoryName = str(storyName).translate(None, '!@#$\\:\/\" ')
    cursubcategoryPath = mainPath + '/' + curcategoryName
    parcer.createDir(cursubcategoryPath)

    return cursubcategoryPath

import shutil
def CopyImage(imagedir,dstdir):
    try:
        shutil.copy(imagedir, dstdir)
    except:
        pass

def trim(url):
    num = url.find("/")
    return url[num+1:len(url)]

def saveIntoFile(dictEnt,path,fileName):
    fileName = path + "/" + fileName + ".json"
    f = open(fileName, "w")
    f.write(json.dumps(dictEnt, sort_keys=True, indent=4, separators=(',', ': ')).decode('unicode-escape').encode(
        "utf-8"))
    f.close()

def saveStatistics(statistic,path):
    text = ""

    text += ("series Title" + ";")
    text += ("episode Index" + ";")
    text += ("episode Count" + ";")
    # text += ("episode Download " + ";")
    text += ("love Ratio" + ";")
    text += ("started_android" + ";")
    text += ("completed_android" + ";")
    text += ("started_ios" + ";")
    text += ("completed_ios" + ";")
    text += ("read Velocity" + ";")

    text += ("read Count" + ";")
    text += ("risk" + ";")

    text += ("comment Ratio" + ";")

    text += ("genre" + ";")
    text += ("previous Read Velocity" + ";")
    text += ("read Acceleration" + ";")
    text += ("created At" + ";")
    text += ("updated At" + ";")
    text += ("searchable" + ";")
    text += ("sentiment Count" + "\n")

    for statistic in statistic:
        try:
            text += (str(statistic["seriesTitle"]) + ";")
            text += (str(statistic["episodeIndex"]) + ";")
            text += (str(statistic["episodeCount"]) + ";")
            # text += (str(statistic["downloadedEpisodes"]) + ";")
            text += (str(statistic["loveRatio"]) + ";")
            text += (str(statistic["started_android"]) + ";")
            text += (str(statistic["completed_android"]) + ";")
            text += (str(statistic["started_ios"]) + ";")
            text += (str(statistic["completed_ios"]) + ";")
            text += (str(statistic["readVelocity"]) + ";")

            text += (str(statistic["readCount"]) + ";")
            text += (str(statistic["risk"]) + ";")

            text += (str(statistic["commentRatio"]) + ";")

            text += (str(statistic["genre"]) + ";")
            text += (str(statistic["previousReadVelocity"]) + ";")
            text += (str(statistic["readAcceleration"]) + ";")
            text += (str(statistic["createdAt"]) + ";")
            text += (str(statistic["updatedAt"]) + ";")
            text += (str(statistic["searchable"]) + ";")
            try:
                text += (str(statistic["sentimentCount"]) + "\n")
            except:
                text += "\n"
        except:
            print statistic
    fileName = path + "/" + "statistic" + ".csv"
    f = open(fileName, "w")
    f.write(text)
    f.close()

def saveDescriptions(stories,path):
    text = ''
    text += ("number" + ";")
    text += ("name" + ";")
    text += ("episodeCount" + "\n")
    num = 0
    for story in stories:
        text += (str(story["number"]) + ";")
        text += (str(story["name"]) + ";")
        text += (str(len(story["episodes"])) + "\n")

    fileName = path + "/" + "descriptions" + ".csv"
    f = open(fileName, "w")
    f.write(text)
    f.close()

def exportStories():
    series = list(getSeriesDB())
    count = 0
    statistic = []
    mainPath = os.path.dirname(os.path.abspath(__file__)) + "/" + ExportDirectory
    storiesF = []
    for serie in series:
        print str(count) + "/" + str(len(series))

        story = getStory(serie)
        if not story: continue
        dstdir = CreateExportStoryPath(serie['_id'])
        # copy files

        imagedir = story["image"]
        if imagedir:
            imagedir = os.path.dirname(os.path.abspath(__file__)) + "/" + PathDirectory  + "/" + imagedir
            story["image"] = trim(story["image"])

            imagedirdstdir = dstdir + "/" + story["image"]
            try:
                CopyImage(imagedir, imagedirdstdir)
            except:
                pass


        for ep in serie["stories"]:
            episode = getEpisode(ep)
            if not episode:
                continue
            epDir = dstdir + "/" + str(episode["number"])
            parcer.createDir(epDir)
            stat = getStatistic(ep)
            stat["downloadedEpisodes"] = len (serie["stories"])
            statistic.append(stat)
            for message in episode["messages"]:
                try:
                    if message["image"]:

                        imagedir = os.path.dirname(os.path.abspath(__file__)) + "/" + PathDirectory + "/" + message["image"]
                        message["image"] = trim(message["image"])
                        imagedirdstdir = epDir + "/" + message["image"]
                        CopyImage(imagedir, imagedirdstdir)
                except:
                    "noimage"
                    pass

            saveIntoFile(episode, epDir, str(episode["number"]))
            addepisodeToDB(episode)
        story["number"] = count
        addstoryToDB(story)
        storiesF.append(story)
        count += 1
        if imagedir:
            CopyImage(imagedir, dstdir)

    saveDescriptions(storiesF, mainPath)
    saveStatistics(statistic, mainPath)
exportStories()


def exportStoriesStatistic():
    statistic = getStoryStatisticDB()
    mainPath = os.path.dirname(os.path.abspath(__file__)) + "/" + ExportDirectory
    saveStatistics(statistic, mainPath)

#exportStoriesStatistic()