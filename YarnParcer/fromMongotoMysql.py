
import os
import time
from sshtunnel import SSHTunnelForwarder
import pymongo
import requests
import urllib
import json

MONGO_HOST = "78.46.175.237"
MONGO_DB = "Yarn"
MONGO_USER = "root"
MONGO_PASS = "V3682450v"

server = SSHTunnelForwarder(
    MONGO_HOST,
    ssh_username=MONGO_USER,
    ssh_password=MONGO_PASS,
    remote_bind_address=('127.0.0.1', 27017)
)



# client = pymongo.MongoClient('127.0.0.1', server.local_bind_port)  # server.local_bind_port is assigned local port
# server.start()

client = pymongo.MongoClient()


storyUrl = "http://localhost:11111/api/stories/add"
episodeUrl = "http://localhost:11111/api/episodes/add"
recalccolorsUrl = "http://localhost:11111/admin/recalculatecolorsforstory"


def postRequest(data,url):
    headers ={
        'content-type': 'application/json'
    }
    data =json.dumps(data)

    r = requests.post(url, data=data,headers=headers)
    try:
        return (json.loads(r.text))
    except:
        return r.text
def  getStoriesFromDB():



    db = client[MONGO_DB]
    routes = db.stories

    Items = routes.find({})
    retSet = []
    for item in Items:
        retSet.append(item)
    return retSet

def  getEpisodeFromDB(id):
    db = client[MONGO_DB]
    routes = db.episodes

    Items = routes.find({"_id":id})
    try:
        return  Items[0]
    except:
        return []

from shutil import copyfile
import time


imPath = "C:/Projects/yarnserver/public/content/images"
stPath = "C:/Projects/yarnserver/YarnParcer/YarnContent"

def trim(url):
    num = url.find("/")
    return url[num+1:len(url)]

def loadStories():
    stories = getStoriesFromDB()
    for story in stories:
        time.sleep(0.001)
        print story["id"]
        story["id"] = story["_id"]
        try:
            story["image"] = trim(story["image"])
        except: story["image"] = None
        req = postRequest(story, storyUrl)
        print req
        if type(req) == int:
            storyId = req
            pathst = stPath + "/" + story["_id"] + "/" + story["image"]
            pathim = imPath + "/" + str(storyId) + "/" + story["image"]

            try:
                # print pathst, pathim
                copyfile(pathst, pathim)

            except:
                print "no image" + story["image"]
            print "add Story: " + str(storyId)
            print "Episode count: " + str(len (story["episodes"]))
            for episode in story["episodes"]:
                # print episode["id"]
                description = episode["description"]
                episode = getEpisodeFromDB(str(episode["id"]))
                try:
                    episode["description"] = description
                except:
                    print episode
                    continue
                for message in episode["messages"]:
                    if "image" in message:
                        imUrl = message["image"]

                        pathst = stPath + "/"+story["_id"]+"/"+imUrl
                        pathim = imPath + "/" +  str(storyId)+"/"+imUrl

                        try:
                            # print pathst, pathim
                            copyfile(pathst, pathim)

                        except:
                            print "no image" + imUrl

                    if "obscuredImage" in message:
                        del message["obscuredImage"]


                episode["storyId"]= storyId
                del episode["_id"] 

                req = postRequest(episode, episodeUrl)
                print req
            postRequest({"id":storyId}, recalccolorsUrl)

    print "end"
    return
loadStories()