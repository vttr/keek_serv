#!/usr/bin/env python
# -*- coding: utf-8 -*-
import parcer


from json import JSONEncoder
import json
import logging


from bs4 import BeautifulSoup


siteUrl = "http://forum.poehali.net/"
routeUrl = "http://forum.poehali.net/index.php?board=12"




import time
from sshtunnel import SSHTunnelForwarder
import pymongo
import pprint

MONGO_HOST = "78.46.175.237"
MONGO_DB = "treks"
MONGO_USER = "root"
MONGO_PASS = "V3682450v"

server = SSHTunnelForwarder(
    MONGO_HOST,
    ssh_username=MONGO_USER,
    ssh_password=MONGO_PASS,
    remote_bind_address=('127.0.0.1', 27017)
)



# client = pymongo.MongoClient('127.0.0.1', server.local_bind_port)  # server.local_bind_port is assigned local port
# server.start()

tracks = []
client = pymongo.MongoClient()
foundCount = 0
precision = 4
class TRK:
    def __init__(self):
        self.positions = []
        self.names = []
        self.links = []
        self.url = []
        self.bb = []

    def getData(self):
        data = {}
        data["_id"] = self.url
        data["positions"] = self.positions
        data["names"] = self.names
        data["links"] = self.links
        data["url"] = self.url
        data["bb"] = self.bb
        return (data)


def getTrackIndex(track,tracks):
    global foundCount
    index = -1
    curIndex = 0
    for item in tracks:
        if item.url == track.url:
            foundCount+=1
            return curIndex
        curIndex+=1
    return index


def  getTRKFromBikeTourist():
    global tracks
    db = client["biketouristclub"]
    routesdb = db.routes
    print "biketorist count: " + str(routesdb.count())
    routes = routesdb.find({})

    for route in  routes:
        for trek in route["tracks"]:
            track = TRK()
            track.url = trek["url"]
            track.links.append(route["url"])
            track.names.append(route["name"])
            trackindex = getTrackIndex(track,tracks)
            bb = [100000, 100000, 0, 0]
            if not trackindex==-1:
                tracks[trackindex].links.append(route["url"])
                tracks[trackindex].names.append(route["name"])

            else:
                for trackdata in trek["data"]:
                    if trackdata["type"]== "LineString":
                        coordinates = []

                        for coordinate in trackdata["coordinates"]:
                            l = round(coordinate[0],precision)
                            y = round(coordinate[1],precision)
                            bb[0]=min(bb[0],l)
                            bb[2] = max(bb[2], l)
                            bb[1] = min(bb[1], y)
                            bb[3] = max(bb[3], y)
                            coordinates.append([l,y])
                        track.positions.append(coordinates)
                track.bb = bb
                tracks.append(track)

    return tracks



def  getTRKFromVeloBy():
    global tracks
    db = client["veloby"]
    routesdb = db.routes
    print "veloby count: " + str(routesdb.count())
    routes = routesdb.find({})

    for route in  routes:
        for trek in route["tracks"]:
            track = TRK()
            track.url = trek["url"]
            track.links.append(route["url"])
            track.names.append(route["name"])
            trackindex = getTrackIndex(track,tracks)
            if not trackindex==-1:
                tracks[trackindex].links.append(route["url"])
                tracks[trackindex].names.append(route["name"])

            else:

                    currr = -1

                    try:
                            trekpoints = trek["data"]["gpx"]["trk"]["trkseg"]["trkpt"]
                            currr = route["_id"]
                            bb = [100000, 100000, 0, 0]
                            coordinates = []
                            for coordinate in trekpoints:
                                l = round(float(coordinate["lat"]),precision)
                                y = round(float(coordinate["lon"]),precision)
                                bb[0] = min(bb[0], l)
                                bb[2] = max(bb[2], l)
                                bb[1] = min(bb[1], y)
                                bb[3] = max(bb[3], y)
                                coordinates.append([l, y])


                            track.positions.append(coordinates)
                            track.bb = bb
                            tracks.append(track)
                    except:
                        if currr == route["_id"]:
                             print "empty " + route["_id"]

    return tracks




def getTRKFromPoehali():
    global tracks
    db = client["poehali"]
    routesdb = db.routes
    print "poehali count: " + str(routesdb.count())
    routes = routesdb.find({})

    for route in routes:
        for trek in route["tracks"]:
            track = TRK()
            track.url = trek["url"]
            track.links.append(route["url"])
            track.names.append(route["name"])
            trackindex = getTrackIndex(track, tracks)
            if not trackindex == -1:
                tracks[trackindex].links.append(route["url"])
                tracks[trackindex].names.append(route["name"])

            else:

                currr = -1

                try:
                    trekpoints = trek["data"]["gpx"]["trk"]["trkseg"]["trkpt"]
                    currr = route["_id"]
                    bb = [100000, 100000, 0, 0]
                    coordinates = []
                    for coordinate in trekpoints:
                        l = round(float(coordinate["lat"]), precision)
                        y = round(float(coordinate["lon"]), precision)
                        bb[0] = min(bb[0], l)
                        bb[2] = max(bb[2], l)
                        bb[1] = min(bb[1], y)
                        bb[3] = max(bb[3], y)
                        coordinates.append([l, y])

                    track.positions.append(coordinates)
                    track.bb = bb
                    tracks.append(track)
                except:
                    if currr == route["_id"]:
                        print "empty " + route["_id"]

    return tracks

def createSimpleDB():
    print len(getTRKFromBikeTourist())
    print len(getTRKFromVeloBy())
    print len(getTRKFromPoehali())
    print foundCount
    db = client["simple"]
    routes = db.routes
    for item in tracks:
        data = item.getData()
        routes.replace_one({'_id': data['_id']}, data, upsert=True)

createSimpleDB()