var Person = function(){
    this.bubbleColor = "#ebeff4";
    this.textColor = "#000000";
    this.id = 0;
    this.name = "Lindsay";
    this.nameColor = "#25aef4";
};

var Message = function(){
    this.typing = false
};

Message.prototype.addRow = function(row) {
    if (!this.body){
        this.body = row.trim();
        return;
    }
    this.body +="\n" + row.trim();
};

function isNonBreakingSpace( character ) {
    return character == '\u00A0';
}

var Story = function () {
    this.description = "description";
    this.people = [];
    this.messages = [];
};

var Block = function (id) {
    this.id = id;
    this.messages = [];
};

Story.prototype.getPersonIndexByName = function (name) {
    var nameTemp = name.replace(/ /g, "");
    for (var i=0; i < this.people.length; i++) {
        var nameTemp2 = this.people[i].name.replace(/ /g, "");
        if (nameTemp == nameTemp2) return i;
    }
    var person = new Person();
    person.name = name;
    person.id = this.people.length;
    this.people.push(person);
    return person.id
};


var getAllIds = function (text) {
    var myList = text.split(",");
    var newList = [];
    var newtext="";
    for (var i in myList) {
        var row = myList[i].trim();
        if (row==""){
            continue
        }
       newList.push(row)
    }
    return newList
};

var removeAllEmptyRows = function (text) {
    var myList = text.split('\n');
    var newtext="";
    for (var i in myList) {
        var row = myList[i].trim();
        if (row==""){
            continue
        }
        if (newtext=="") {
            newtext += row
        }else{
            newtext=newtext + "\n" + row;
        }
    }
    return newtext;
};

var convertTextToJsontest = function (text) {
    var Message = function(i){
        this.i=i;
    };
    var message = new Message(3);
    var messages = [];
    messages.push(message);
    message = new Message(4);

return messages[0].i
};

var convertTextToJson = function (text){
    var myList = removeAllEmptyRows(text).split('\n');


    var story = new Story();
    var curMessageBlock = new Block();
    var lastPerson = undefined;
    for (var i in myList)
    {
        var row = myList[i];

        //branching message
        if (row.indexOf("###goto#")==0){
            var message = new Message();
            var temp = row.substr(8);
            var BlockIds = getAllIds(temp);
            message.goto = BlockIds;
            if (!story.blocks) story.blocks = [];
            curMessageBlock.messages.push(message);
            continue
        }

        //new block
        if (row.indexOf("###")==0){
            var temp = row.substr(3).trim();
            if(!curMessageBlock.id){
                story.messages = curMessageBlock.messages;
            }else{
                story.blocks.push(curMessageBlock)
            }
            curMessageBlock = new Block(temp);
            continue
        }

        //system message
        if (row.indexOf("##%%")==0)
        {
            var message = new Message();
            var temp = row.substr(2).trim().replace(/[^\w,.]/gi, '');
            message.image=temp;
            curMessageBlock.messages.push(message);
            continue
        }

        //system message
        if (row.indexOf("##")==0)
        {
            var message = new Message();
            var temp = row.substr(2);
            message.addRow(temp);
            curMessageBlock.messages.push(message);
            continue
        }

        //person name
        if (row.indexOf("#")==0)
        {
            var temp = row.substr(1).trim();
            var index = story.getPersonIndexByName(temp);
            lastPerson = index;
            continue

        }

        //image message
        if (row.indexOf("%%")==0)
        {
            var temp = row.substr(2).trim().replace(/[^\w,.]/gi, '');
            var message = new Message();
            if (lastPerson!==undefined){
                message.personId = lastPerson;
            }
            message.image = temp;
            curMessageBlock.messages.push(message);
            continue
        }

        //typing message
        if (row.indexOf("%")==0)
        {
            var message = new Message();
            if (lastPerson!==undefined){
                message.personId = lastPerson;
            }
            var temp = row.substr(1).trim();
            if(temp!=""){
                message.addRow(temp)
            }
            message.typing = true;
            curMessageBlock.messages.push(message);
            continue
        }

        //add message Body
        var message = new Message();
        if (lastPerson!==undefined){
            message.personId = lastPerson;
        }
        message.addRow(row)
        curMessageBlock.messages.push(message);
    }
    if(curMessageBlock.id){
        story.blocks.push(curMessageBlock)
    }else{
        story.messages = curMessageBlock.messages
    }
    var jsonDock = JSON.stringify(story,null,"\t");
    console.log(jsonDock);
    jsonDock = jsonDock.replace(/ \"/g,'\"');

    return jsonDock


};


var getPersonName = function (people, id){
    try{
        for (var i in people){
            if (id==people[i].id) return people[i].name
        }
        return null
    }catch (err) {
        return null
    }
};

var messagesToText = function (messages,jsonModel) {
    var textOutput = ""
    var lastPersonId = false;
    for (var i in messages)
    {
        var message = messages[i];
        if (message.personId===undefined && message.body!== undefined)
        {
            textOutput += "\n" +"##" + message.body +"\n";
            continue
        }

        if (message.goto!== undefined){
            textOutput +=  "\n###goto#";
            for(var ind in message.goto){
                textOutput +=message.goto[ind]+","
            }
            textOutput +="\n"
            continue
        }

        if (message.personId!==lastPersonId){
            var name =  getPersonName(jsonModel.people,message.personId);

            if (name!==null){
                lastPersonId = message.personId;
                textOutput += "\n" + "#" + name + "\n";
            }

        }

        if(message.typing!== false){
            textOutput += "%";
        }

        if(message.image!== undefined){

            if(message.personId==undefined){
                textOutput += "##%%" + message.image +"\n";
            }else{
                textOutput += "%%" + message.image +"\n";
            }
        }

        if (message.body!== undefined){
            textOutput +=  message.body + "\n";

        }

    }
    return textOutput;
};

var convertJsonToText = function (textJson) {
    var jsonModel = {}
    try {

        jsonModel = JSON.parse(textJson)

    } catch (err) {

        return "invalidJSON: " + err

    }
    //remove personId
    for (var index in jsonModel.people)
    {
        if (jsonModel.people[index].personId!==undefined){
            jsonModel.people[index].id = jsonModel.people[index].personId;
            delete jsonModel.people[index].personId
        }
    }
    var textOutput = "";
    textOutput += messagesToText(jsonModel.messages,jsonModel);
    if (jsonModel.blocks){
        for (var ind in jsonModel.blocks)
        {
            textOutput += "\n###"+jsonModel.blocks[ind].id + "\n";
            textOutput += messagesToText(jsonModel.blocks[ind].messages,jsonModel);
        }
    }

    return textOutput;
};

exports.convertJsonToText = convertJsonToText;
exports.convertTextToJson  = convertTextToJson;