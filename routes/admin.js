var express = require('express');
var router = express.Router();
var dbManager = require("../app/dbManager");
var standartRequest = require("../app/standartRequest");
var isLoggedIn = require("../app/helpers").isLoggedIn;
var roleAuthorization = require("../app/helpers").roleAuthorization
var passport ={};
var app = {};
var roles = require("../app/roles");
var googleTableReader = require("../app/googleTableReader");
var iosVerification = require("../app/iosVerification")

module.exports = function(App,Passport,apicache)
{
    var isProd= false;
    if (process.env.NODE_ENV=='production') {
        isProd = true;
    }
    var cache     = apicache.options({
        enabled: isProd
    }).middleware;
    app = App;
    passport = Passport;


/* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('index', { title: 'Express' });
// });

router.get('/',isLoggedIn, roleAuthorization([roles.admin,roles.writer,roles.reader]), function(req,res,next){

    res.render('../views/admin',{title:"Admin Page",user : req.user});

});


var St = require("../app/controllers/stories");
var Stories = new St();

    var bcrypt = require('bcryptjs');
router.get('/stories',isLoggedIn, roleAuthorization([roles.admin,roles.writer,roles.reader]), function(req,res,next){
    var page = req.query.page;
    if (page===undefined){
        page = 1;
    }
try {
    var self = res;
    //var query ="select * from spookytalks.stories where (source is null or source != 'Yarn')";
    var query ="select * from spookytalks.stories";
    dbManager.dbquery(query,function (err, resp) {
        if (err !== null) {
            (standartRequest(self, null, true, err));
            return
        }

        //create dict
        var dict = {};

        for (var ind in resp) {
            resp[ind].episodes = [];
            dict[resp[ind].id] = ind
        }


        dbManager.getSroriesEpisodesAll(resp[ind].id, function (err, result) {
            if (err !== null) {
                (standartRequest(self, null, true, err));
                return
            }

            for (var ind in result) {

                var storyId = result[ind].storyId;

                delete result[ind].storyId;
                if (dict[storyId] !== undefined)
                    resp[dict[storyId]].episodes.push(result[ind]);
            }
            // var txt = JSON.stringify(resp);
            res.render('stories', {title: "Stories", stories: resp,user : req.user});
        });


        return;

    })

}catch (err){
    next(err)
}


});

var Ep = require("../app/controllers/episodes");
var Episodes = new Ep();
router.get('/episodes',isLoggedIn,roleAuthorization([roles.admin,roles.writer,roles.reader]),function(req,res,next){
    try {
        var storyId = req.query.id;
        Episodes.getByParamDB({storyId : storyId},function (err, resp) {
            if (err !== null) {
                (standartRequest(self, null, true, err));
                return
            }

            var epArray = resp.sort(function(a, b) {
                return a.number > b.number;
            });

            //console.log(epArray);

            // var txt = JSON.stringify(resp);
            res.render('episodes', {title: "Episodes", episodes: epArray,storyId : storyId,user : req.user});


            return;

        })

    }catch (err){
        next(err)
    }


});


router.get('/episode',isLoggedIn,roleAuthorization([roles.admin,roles.writer,roles.reader]),function(req,res,next){
    try {
        var episodeId = req.query.id;
        Episodes.getByIdDB(episodeId,function (err, resp) {
            if (err !== null) {
                (standartRequest(self, null, true, err));
                return
            }
            res.render('episode', {title: "Episode", episode: resp[0],user : req.user});

            return;

        })

    }catch (err){
        next(err)
    }
});

router.post('/updateepisode',isLoggedIn,roleAuthorization([roles.admin,roles.writer]),function(req,res,next){
    try {
        console.log(req.body.id)
        dbManager.update(Episodes.name, req.body.id, req.body.data, function(err,result) {
            if (err !== null) {
                (standartRequest(res, null, true, err));
                return
            }
            apicache.clear("getAll");
            Stories.refreshAll();
            apicache.clear("/api/episodes/get?id="+req.body.id);
            standartRequest(res,"ok")
            return;

        })
    }catch (err){
        next(err)
    }

});

router.get('/updateepisodes',isLoggedIn,roleAuthorization([roles.admin]),function(req,res,next){
    try {
        Episodes.getAllDB(function (err,data) {
            for (var epIndex in data){
                var episode =  data[epIndex]
                var textJson = decodeURIComponent(episode.textjson)
                episode.textjson = textJson;
                var id = episode.id;
                delete episode.id;
                Episodes.updateDB({id:id,data:episode},function (err,data) {
                    
                });

            }
            res.send(data)
            return
        })


    }catch (err){
        next(err)
    }

});


router.get('/findByWord',isLoggedIn,roleAuthorization([roles.admin,roles.writer,roles.reader]),function(req,res,next){
    try {
        if (req.query.word){
            var word = req.query.word
        dbManager.getEpisodesByWord(req.query.word, function(err,result) {
            if (err !== null) {
                (standartRequest(res, null, true, err));
                return
            }
            res.render('findByWord',{title: "findByWord", episodes: result, word:word,user : req.user})
            return;

        })}else{
            res.render('findByWord',{title: "findByWord", episodes: [],word:"",user : req.user})
            return
        }
    }catch (err){
        next(err)
    }
});

var Us = require("../app/controllers/users");
var Users = new Us();

router.get('/settings',isLoggedIn,roleAuthorization([roles.admin,roles.writer]),function(req,res,next){
    try {
        var testData = Users.getTestData()
        var settingsData = Users.getSettingsData()
        var data = {
            testData: testData,
            settingsData: settingsData
        }
        res.render('settings',{title: "Settings", data: data,user : req.user})
            return

    }catch (err){
        next(err)
    }
});

router.post('/settings',isLoggedIn,roleAuthorization([roles.admin]),function(req,res,next){
    try {
        if (req.body.testData && req.body.settingsData){
            Users.setTestData(req.body.testData);
            Users.setSettingsData(req.body.settingsData);
            res.send("ok")
        }
    }catch (err){
        next(err)
    }
});

    router.post('/updateTestData',isLoggedIn,roleAuthorization([roles.admin]),function(req,res,next){
        try {
            if (req.body[0] && req.body[1]){

                var query = "UPDATE `users` SET `testData` = REPLACE( testData, '"+JSON.stringify(req.body[0])+"', '"+JSON.stringify(req.body[1])+"' ) ;"
                dbManager.dbquery(query,function (err,data) {
                    if(err){
                        res.send(false)
                    }else{
                        res.send(true)
                    }
                })
            }
        }catch (err){
            next(err)
        }
    });

router.get('/post',isLoggedIn,roleAuthorization([roles.admin,roles.writer,roles.reader]),function(req,res,next){
    try {
        if (req.query.word){
            var word = req.query.word;
            dbManager.getEpisodesByWord(req.query.word, function(err,result) {
                if (err !== null) {
                    (standartRequest(res, null, true, err));
                    return
                }


                res.render('findByWord',{title: "findByWord", episodes: result, word:word})
                return;

            })}else{
            res.render('findByWord',{title: "findByWord", episodes: [],word:"",user : req.user})
            return
        }
    }catch (err){
        next(err)
    }

});

function search(nameKey, myArray){
    for (var i=0; i < myArray.length; i++) {
        if (myArray[i].name === nameKey) {
            return myArray[i];
        }
    }
}

router.get('/story',isLoggedIn,roleAuthorization([roles.admin,roles.writer,roles.reader]),function(req,res,next){
    try {
         var id = req.query.id;
        dbManager.getStories(function (err, resp) {
            if (err !== null) {
                (standartRequest(self, null, true, err));
                return
            }

            //create dict
            var dict = {}
            for (var ind in resp) {
                resp[ind].episodes = []
                dict[resp[ind].id] = ind
            }


            dbManager.getSroriesEpisodes(resp[ind].id, function (err, result) {
                if (err !== null) {
                    (standartRequest(self, null, true, err));
                    return
                }
                // console.log(result);
                for (var ind in result) {
                    var storyId = result[ind].storyId;
                    delete result[ind].storyId;
                    resp[dict[storyId]].episodes.push(result[ind]);
                }

                // var txt = JSON.stringify(resp);
                res.render('story', {title: "Story", story: search("id", resp),user : req.user});
            });

            return;
        })
    }catch (err){
        next(err)
    }

});


var fs = require('fs'),
    request = require('request');
var url = require("url");

var path = require("path");
var helpers = require("../app/helpers");

var download = function(uri, filename, callback){
    request.head(uri, function(err, res, body){
        console.log('content-type:', res.headers['content-type']);
        console.log('content-length:', res.headers['content-length']);

        request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
    });
};

    router.post('/updatestory',isLoggedIn,roleAuthorization([roles.admin,roles.writer]),function(req,res,next){
    try {
        console.log(req.body.data.image);

        //copy image
        var parsed = decodeURI(url.parse(req.body.data.image).pathname);
        var startImagePath = path.resolve(__dirname,'../public'+parsed);
        var imageName = path.basename(startImagePath);
        let dir = path.resolve(__dirname, '../public/content/images/' + req.body.id);
        if (!fs.existsSync(dir)){
            fs.mkdirSync(dir);
        }
        var endImagePath = path.resolve(__dirname, '../public/content/images/' + req.body.id + "/"+ imageName);
        if(startImagePath!=endImagePath){
            helpers.copyImage(startImagePath, endImagePath);
        }
        req.body.data.image = imageName;
        apicache.clear("getAll");

        Stories.refreshAll();
        dbManager.update(Stories.name, req.body.id, req.body.data, function(err,result) {
            if (err !== null) {
                (standartRequest(res, null, true, err));
                return
            }
            standartRequest(res,"ok")
                return;

        })
    }catch (err){
        next(err)
    }

});

router.post('/recalculatecolorsforstory',function(req,res,next){
    var self = res;
    try {
        Stories.recalculateColors(req.body.id, function (err, data) {
            if (err) {
                console.log(err);
                next(err);
                return
            }
            self.send(data)
        })
    }catch (err){
        console.log(err);
        next(err)
    }

});

router.get('/recalculatecolorsofallstories',isLoggedIn,roleAuthorization([roles.admin]),function(req,res,next){
    var self = res;
    try {
        Stories.getAllDB(function (err,data) {
            if (err) {
                next(err);
                return
            }

            for (var storyIndex in data){
                var id = data[storyIndex].id;
                Stories.recalculateColors(id, function (err, data) {
                    if (err) {
                        console.log(err)

                    }
                })
            }
            self.send("ok")
        })

    }catch (err){
        next(err)
    }

});


    router.get('/recalculatemessages',function(req,res,next){
        var self = res;
        try {
            Episodes.getAllDB(function (err,data) {
                if (err) {
                    next(err);
                    return
                }

                for (var index in data){

                        var id = data[index].id;
                    if (id>433) {
                        console.log("start")
                        Episodes.recalculateNewLinesToMessageBiId(id)
                    }
                }
                self.send("ok")
            })

        }catch (err){
            next(err)
        }

    });



var Ad = require("../app/controllers/admins");
var Admins = new Ad();

    router.get('/admins',isLoggedIn,roleAuthorization([roles.admin]),function(req,res,next){
        try {
            Admins.getAllDB(function (err,data) {
                if(err){
                    next(err)
                }else{
                    res.render('adminUsers', {title: "Admins", admins: data,user : req.user});
                }
            });

        }catch (err){
            next(err)
        }

    });

    router.get('/admins/getAll',isLoggedIn,roleAuthorization([roles.admin]),function(req,res,next) {
        Admins.getAll(req,res,next);
    });

    router.post('/admins/remove',isLoggedIn,roleAuthorization([roles.admin]),function(req,res,next) {
        Admins.remove(req,res,next);
    });

    router.post('/admins/update',isLoggedIn,roleAuthorization([roles.admin]),function(req,res,next) {
        Admins.update(req,res,next);
    });




    router.get('/login', function(req, res) {

    // render the page and pass in any flash data if it exists
    res.render('login', { message: req.flash('loginMessage'),user : req.user  });
});

    router.get('/users',isLoggedIn,roleAuthorization([roles.admin]),function(req,res,next){
        try {
            var query = "select isFree,count(*) FROM users Group by isFree;"
            dbManager.dbquery(query,function (err, data) {
                if (err) {
                    next(err)
                } else {
                    res.render('users', {title: "Users", users:data, user: req.user, verificationStatus : Us.prototype.getVerificationStatus() });

                }
            })
        }catch (err){
            next(err)
        }

    });


    router.get('/users/verifyAll',isLoggedIn,roleAuthorization([roles.admin]), helpers.asyncMiddleware( async function(req,res,next)  {
            res.send('ok');
            var data = await Users.verifyAllUsers()
            console.log(data)

    })
    );


    router.post('/users/getInfoById',isLoggedIn,roleAuthorization([roles.admin]), helpers.asyncMiddleware( async function(req,res,next)  {
        try{
            let query = `select * from users where id = "${req.body.id}"`;
            let user = await dbManager.dbqueryAsync(query);
            user = user[0];

            Users.verifyUser(user)
            let data = "";
             if (user){
                 if (user.appleToken){
                     data  = await iosVerification.verifyAsync(user);
                 }
             }else{
                 user = "not found"
             }

            res.send({user:user, receipt: data})
        }catch (error){
            res.send({error:error})
        }
        })
    );
    router.get('/users/download',isLoggedIn,roleAuthorization([roles.admin]),function(req,res,next) {
        try {
            Users.getAllDB(function (err, data) {
                if (err) {
                    next(err)
                } else {
                    var usersTableText='sep=\t\n';
                    //id;scenarioId;testData;register;lastActivity;subscriptionUntil;isFree;appleToken;curEpisode;curMessage;locale;productId;pushToken;timeZone;isTest;country;paidSumm;idfa;
                        usersTableText+="id" + "\t";
                    usersTableText+="testData" + "\t";
                    usersTableText+="register" + "\t";
                    usersTableText+="last Activity" + "\t";
                    usersTableText+="isFree" + "\t";
                    usersTableText+="locale" + "\t";
                    usersTableText+="isTest" + "\t";
                    usersTableText+="country" + "\t";
                    usersTableText+="stories Ended" + "\t";
                    usersTableText+="adjustData" + "\t";
                    usersTableText+="paidSumm" + "\t";
                    usersTableText+="productId" + "\t";
                    usersTableText+="purchaseCount" + "\t";
                    usersTableText+="subscription From" + "\t";
                    usersTableText+="subscription Until" + "\t";
                    usersTableText+="idfa" + "\t";

                    usersTableText+="\n";
                    for (var ind in data) {

                        usersTableText+=data[ind]["id"] + "\t";
                        usersTableText+=data[ind]["testData"] + "\t";
                        usersTableText+=data[ind]["register"] + "\t";
                        usersTableText+=data[ind]["lastActivity"] + "\t";
                        usersTableText+=data[ind]["isFree"] + "\t";
                        usersTableText+=data[ind]["locale"] + "\t";
                        usersTableText+=data[ind]["isTest"] + "\t";
                        usersTableText+=data[ind]["country"] + "\t";
                        usersTableText+=data[ind]["storiesEnded"] + "\t";
                        usersTableText+=data[ind]["adjustData"] + "\t";
                        usersTableText+=data[ind]["paidSumm"] + "\t";
                        usersTableText+=data[ind]["productId"] + "\t";
                        usersTableText+=data[ind]["purchaseCount"] + "\t";
                        usersTableText+=data[ind]["subscriptionFrom"] + "\t";
                        usersTableText+=data[ind]["subscriptionUntil"] + "\t";
                        usersTableText+=data[ind]["idfa"] + "\t";
                        usersTableText+="\n";
                    }

                    res.setHeader('Content-disposition', 'attachment; filename=Users.csv');
                    res.setHeader('Content-type', 'text/plain');
                    res.charset = 'UTF-8';
                    res.write(usersTableText);
                    res.end();


                }
            })
        }catch (err){
            next(err)
        }
    });

    router.get('/deeplinks/download',isLoggedIn,roleAuthorization([roles.admin]),helpers.asyncMiddleware( async function(req,res,next)  {

        var query ="select d.id,d.time,d.data,d.isAdjust,d.userId,users.register,users.testData,users.lastActivity,users.isFree,users.locale,users.country,users.storiesEnded,users.paidSumm,users.productId,users.subscriptionFrom,users.subscriptionUntil,users.idfa\
            from deeplinks as d\
                join users as users on  d.userId COLLATE utf8mb4_general_ci = users.id\
                    JOIN\
                    (SELECT userId, MAX(time) AS max_dt\
                        FROM deeplinks\
                            GROUP BY userId) sel ON d.userId = sel.userId AND d.time = sel.max_dt;"


            let data = await dbManager.dbqueryAsync(query) ;
                    var usersTableText = 'sep=\t\n';
                    //id;scenarioId;testData;register;lastActivity;subscriptionUntil;isFree;appleToken;curEpisode;curMessage;locale;productId;pushToken;timeZone;isTest;country;paidSumm;idfa;
                    usersTableText += "id" + "\t";
                    usersTableText += "time" + "\t";
                    usersTableText += "data" + "\t";
                    usersTableText += "isAdjust" + "\t";
                    usersTableText += "userId" + "\t";
                    usersTableText += "testData" + "\t";
                    usersTableText += "register" + "\t";
                    usersTableText += "lastActivity" + "\t";
                    usersTableText += "isFree" + "\t";
                    usersTableText += "locale" + "\t";
                    usersTableText += "country" + "\t";
                    usersTableText += "storiesEnded" + "\t";
                    usersTableText += "paidSumm" + "\t";
                    usersTableText += "productId" + "\t";
                    usersTableText+="subscription From" + "\t";
                    usersTableText += "subscriptionUntil" + "\t";
                    usersTableText += "idfa" + "\t";

                    usersTableText += "\n";
                    for (var ind in data) {
                        usersTableText += data[ind]["id"] + "\t";
                        usersTableText += data[ind]["time"] + "\t";
                        usersTableText += data[ind]["data"] + "\t";
                        usersTableText += data[ind]["isAdjust"] + "\t";
                        usersTableText += data[ind]["userId"] + "\t";
                        usersTableText += data[ind]["testData"] + "\t";
                        usersTableText += data[ind]["register"] + "\t";
                        usersTableText += data[ind]["lastActivity"] + "\t";
                        usersTableText += data[ind]["isFree"] + "\t";
                        usersTableText += data[ind]["locale"] + "\t";
                        usersTableText += data[ind]["country"] + "\t";
                        usersTableText += data[ind]["storiesEnded"] + "\t";
                        usersTableText += data[ind]["paidSumm"] + "\t";
                        usersTableText += data[ind]["productId"] + "\t";
                        usersTableText+=data[ind]["subscriptionFrom"] + "\t";
                        usersTableText += data[ind]["subscriptionUntil"] + "\t";
                        usersTableText += data[ind]["idfa"] + "\t";
                        usersTableText += "\n";
                    }

                    res.setHeader('Content-disposition', 'attachment; filename=Deeplinks.csv');
                    res.setHeader('Content-type', 'text/plain');
                    res.charset = 'UTF-8';
                    res.write(usersTableText);
                    res.end();



            })
    );
    var fb= require("../app/controllers/feedbacks");
    var Feedbacks = new fb();
    router.get('/feedbacks',isLoggedIn,roleAuthorization([roles.admin]),function(req,res,next){
        try {
            var query = "SELECT ep.id,  ep.number,s.`name` FROM episodes ep JOIN stories s ON s.id = ep.storyId ;";
            dbManager.dbquery(query,function (err,stories) {
                if (err){
                    next(err);
                    return
                }
                //console.log(stories)
                Feedbacks.getAllDB(function (err,data) {
                    if(err){
                        next(err)
                    }else{
                        res.render('feedbacks', {title: "FeedBacks", feedbacks: data,user : req.user,stories:stories});
                    }
                });
            })

        }catch (err){
            next(err)
        }

    });




    var checkImage = function (id,image) {

        var imagePath = path.resolve(__dirname, '../public/content/images/' + id + "/"+ image);
        if (fs.existsSync(imagePath )) {
            return true;
        }else{
            return false;
        }
    };
    var checkImages = function(storiesImages){
        var failImages = []
        for (var idx in storiesImages) {
            var storyImages = {
                name: storiesImages[idx].name,
                id: storiesImages[idx].id,
                images: []
            };
            for (var imgIdx in storiesImages[idx].images) {
                if (checkImage(storiesImages[idx].id,storiesImages[idx].images[imgIdx])===false){
                    storyImages.images.push(storiesImages[idx].images[imgIdx])
                }

            }
            if (storyImages.images.length>0)
                failImages.push(storyImages)
        }
        return failImages;
    }

    var analiseStories = function(stories){
        var problems = [];
        //parse episodes
        for (var idx in stories){
            for (var epIdx in stories[idx].episodes){
                var id = stories[idx].episodes[epIdx].id;
                stories[idx].episodes[epIdx] = JSON.parse(stories[idx].episodes[epIdx].textjson);
                if (stories[idx].episodes[epIdx]===undefined) stories[idx].episodes[epIdx] = {};
                stories[idx].episodes[epIdx].id = id;
            }
        }
        // create list of all images

        var storiesImages = [];
        var distances = [];
        var maxVal = 0;
        var maxEp = 0;
        var distLen = 0;
        for (var idx in stories) {
            //1 check story image
            var  story = stories[idx]
            var storyImages = {
                id:story.id,
                name:story.name,
                images : [],
                emptyEpisodes:[],
            };
            storyImages.images.push(story.image)
            for(var epIdx in story.episodes){

                if(story.episodes[epIdx].messages===undefined){

                    storyImages.emptyEpisodes.push(story.episodes[epIdx].id);
                    continue;
                }

                if(story.episodes[epIdx].messages.length===0){

                    storyImages.emptyEpisodes.push(story.episodes[epIdx].id);
                    continue;
                }

                //check blocks
                //ToDo: check

                var distInfo = {
                    storyId: story.id,
                    storyName: story.name,
                    epId: story.episodes[epIdx].id,
                    lengths:[]
                };
                var curLen = 0;
                for(var mesgIdx in story.episodes[epIdx].messages){
                    if (story.episodes[epIdx].messages[mesgIdx]==true){
                        if (curLen>maxVal){
                            maxVal = curLen;
                            maxEp = story.episodes[epIdx].id;
                        }
                        distLen++;
                        distInfo.lengths.push(curLen);
                        curLen = 0;
                    }else{
                        curLen ++
                    }
                    if (story.episodes[epIdx].messages[mesgIdx].image !== undefined){
                        storyImages.images.push(story.episodes[epIdx].messages[mesgIdx].image)
                    }
                }
                distInfo.lengths.push(curLen)
                distances.push(distInfo);
                distLen++;
                if (curLen>maxVal){
                    maxVal = curLen;
                    maxEp = story.episodes[epIdx].id;
                }
            }
            storiesImages.push(storyImages);
        }

        var image_problems = checkImages(storiesImages);
        var episode_problems = [];
        for(idx in storiesImages){
            if ( storiesImages[idx].emptyEpisodes.length >0 ){
                episode_problems.push({
                    name: storiesImages[idx].name,
                    id:storiesImages[idx].id,
                    episodes: storiesImages[idx].emptyEpisodes
                })
            }
        }
        var avSum = 0;

        for (var i in distances){
            for (var j in distances[i].lengths) {
                avSum += distances[i].lengths[j] / distLen;

            }
        }


        distances.sort(function(a, b) {
            if (Math.max(a.lengths)  < Math.max(b.lengths)) return 1;
            if (Math.max(a.lengths)  > Math.max(b.lengths)) return -1;
            return 0;
        });

        return {
            distances: {
                bigEp: distances,
                maxEp : maxEp,
                maxVal :  maxVal,
                avSum : avSum
            },
            image_problems:image_problems,
             episode_problems:episode_problems
        }

    };

    router.get('/checkStories',isLoggedIn,roleAuthorization([roles.admin,roles.writer]),function(req,res,next){
        try {


                var self = res;
                //var query ="select * from spookytalks.stories where (source is null or source != 'Yarn')";
            var query ="select * from spookytalks.stories;"
                dbManager.dbquery(query,function (err, resp) {
                    if (err !== null) {
                        (standartRequest(self, null, true, err));
                        return
                    }

                    //create dict
                    var dict = {};

                    for (var ind in resp) {
                        resp[ind].episodes = [];
                        dict[resp[ind].id] = ind
                    }


                    Episodes.getAllDB(function (err, result) {
                        if (err !== null) {
                            (standartRequest(self, null, true, err));
                            return
                        }

                        for (var ind in result) {

                            var storyId = result[ind].storyId;

                            delete result[ind].storyId;
                            if (dict[storyId] !== undefined)
                                resp[dict[storyId]].episodes.push(result[ind]);
                        }
                        var data =analiseStories(resp);
                        // var txt = JSON.stringify(resp);
                        res.render('checkStories', {title: "Check Stories", problems:data, user: req.user});
                    });


                    return;

                })

        }catch (err){
            next(err)
        }

    });




// process the login form
router.post('/login', passport.authenticate('local-login', {
        successRedirect : '/admin/', // redirect to the secure profile section
        failureRedirect : '/admin/login', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }),
    function(req, res) {
        console.log("hello");

        if (req.body.remember) {
            req.session.cookie.maxAge = 1000 * 60 * 3;
        } else {
            req.session.cookie.expires = false;
        }
        res.redirect('/admin/');
    });

    var mysqlDump = require('mysqldump');
    router.get('/getDump', function(req, res) {

            if (req.query.password=="root"){
                console.log("hello");
                mysqlDump({
                    host: 'localhost',
                    user: 'root',
                    password: 'root',
                    database: 'spookytalks',
                    dropTable:true,
                    dest:path.join(__dirname, '../dumps/lastDump.sql')// destination file
                },function(err){
                    if (err!==null){
                        res.send(err);
                        return;
                    }
                    // create data.sql file;
                    var filePath = path.join(__dirname, '../dumps/lastDump.sql');
                    var stat = fs.statSync(filePath);

                    res.writeHead(200, {
                        'Content-Type': 'text/html',
                        'Content-Length': stat.size
                    });

                    var readStream = fs.createReadStream(filePath);
                    // We replaced all the event handlers with a simple call to readStream.pipe()
                    readStream.pipe(res);
                })
            }else{
                res.send('ok');
            }


        });





// =====================================
// SIGNUP ==============================
// =====================================
// show the signup form
router.get('/register', function(req, res) {
    // render the page and pass in any flash data if it exists
    res.render('register', { message: req.flash('signupMessage'),user : req.user  });
});

// process the signup form
router.post('/register', passport.authenticate('local-signup', {
    successRedirect : '/admin', // redirect to the secure profile section
    failureRedirect : '/admin/register', // redirect back to the signup page if there is an error
    failureFlash : true // allow flash messages
}));

// =====================================
// PROFILE SECTION =========================
// =====================================
// we will want this protected so you have to be logged in to visit
// we will use route middleware to verify this (the isLoggedIn function)
router.get('/profile', isLoggedIn, function(req, res) {
    res.render('profile', {
        user : req.user // get the user out of session and pass to template
    });
});

// =====================================
// LOGOUT ==============================
// =====================================
router.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/admin/login/');
});


    router.get('/permissions', function(req, res) {
        res.render('permissions', {
            user : req.user // get the user out of session and pass to template
        });
    });

    return router;
};






