var express = require('express');
var router = express.Router();
var dbManager = require("../app/dbManager");
var standartRequest = require("../app/standartRequest");
var isLoggedIn = require("../app/helpers").isLoggedIn;
var roleAuthorization = require("../app/helpers").roleAuthorization
var passport ={};
var app = {};
var roles = require("../app/roles");
var fb= require("../app/controllers/feedbacks");
var Feedbacks = new fb();
module.exports = function(App,Passport,apicache) {
    var isProd= false;
    if (process.env.NODE_ENV=='production') {
        isProd = true;
    }
    var cache     = apicache.options({
        enabled: isProd
    }).middleware;
    app = App;
    passport = Passport;

    router.get('/getAll',isLoggedIn,roleAuthorization([roles.admin]),function(req,res,next) {
        Feedbacks.getAll(req,res,next);
    });

    router.post('/remove',isLoggedIn,roleAuthorization([roles.admin]),function(req,res,next) {
        Feedbacks.remove(req,res,next);
    });

    router.post('/add',function(req,res,next) {
        var userId = req.body.userId;
        var message = req.body.message;

        var episodeId = req.body.episodeId;
        //console.log("episodeId",episodeId);

        if (!episodeId || episodeId==0){
            episodeId = null;
        }
        if (!message||!userId)
        {
            (standartRequest(res,null, true, "invalid id or message"));
            return
        }
        var userData = {
            userId: userId,
            message: message,
            createdAt: new Date().toMysqlFormat(),
            episodeId:episodeId
        };
        Feedbacks.addDB(userData,(err,data)=>{
            if (err !== null) {
            (standartRequest(res, null, true, err));
            return
        }else{
            (standartRequest(res, "ok"));
        }


        })
    });

    return router;

};