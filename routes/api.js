/**
 * Created by v.Triputin on 01.09.2017.
 */

var express = require('express');
var router = express.Router();
var Story = require("../app/entties/story");
var dbManager = require("../app/dbManager");
var helpers = require("../app/helpers");
var fs = require('fs');
var passport;
var app;
var roleAuthorization = require("../app/helpers").roleAuthorization;
var isLoggedIn = require("../app/helpers").isLoggedIn;
var roles = require("../app/roles");


var standartRequest = require("../app/standartRequest");

isObject = function(a) {
    return (!!a) && (a.constructor === Object);
};

var path = require('path');
var mkdirp = require('mkdirp');

/**
 * You first need to create a formatting function to pad numbers to two digits…
 **/
function twoDigits(d) {
    if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
}

/**
 * …and then create the method to output the date string as desired.
 * Some people hate using prototypes this way, but if you are going
 * to apply this to more than one Date object, having it as a prototype
 * makes sense.
 **/
Date.prototype.toMysqlFormat = function() {
    return this.getUTCFullYear() + "-" + twoDigits(1 + this.getUTCMonth()) + "-" + twoDigits(this.getUTCDate()) + " " + twoDigits(this.getUTCHours()) + ":" + twoDigits(this.getUTCMinutes()) + ":" + twoDigits(this.getUTCSeconds());
};

var getSequence = function (req, res)
{

    var mas = [22,45,67,78,34,56,23,67];
    req = standartRequest(mas)
    res.send(req)
};

var createArchive = function(req, res)
{



//get stories
    var self = res;

    dbManager.getStories(function (err, resp) {
        if (err !== null) {
            (standartRequest(self,null, true, err));
            return
        }

        //create dict
        var dict = {}
        for (var ind in resp) {
            resp[ind].episodes = []
            dict[resp[ind].id] = ind
        }


        dbManager.getSroriesEpisodes(resp[ind].id, function (err, result) {
            if (err !== null) {
                (standartRequest(self,null, true, err));
                return
            }
            // console.log(result);
            for (var ind in result) {
                var storyId = result[ind].storyId;
                delete result[ind].storyId;
                resp[dict[storyId]].episodes.push(result[ind]);
            }


            //create folder
            mkdirp(path.resolve(__dirname, '../public/content/ExportStory'), function(err) {});

            fs.writeFile(path.resolve(__dirname, "../public/content/ExportStory/stories.json"), JSON.stringify(resp, null, "\t"), function(err){})
            //
            for (var stotyIndex in resp)
            {
                var story = resp[stotyIndex]

                mkdirp(path.resolve(__dirname, '../public/content/ExportStory/' + story.id), function(err) {});


                   if (story.image) {
                       var startImagePath = path.resolve(__dirname, '../public/content/images/' + story.id + "/"+ story.image);

                       var endImagePath = path.resolve(__dirname, '../public/content/ExportStory/' + story.id + "/" + story.image);
                       helpers.copyImage(startImagePath, endImagePath);
                   }
                 for (var episodeIndex in story.episodes)
                {
                    var episode = story.episodes[episodeIndex];

                    saveEpisode(episode.number,episode.id,story.id)

                }
            }




            //var startImagePath = path.resolve(__dirname, '../public/content/stories/' + idSection + reqStory.image);



            res.send("ok")

        });

    });

};



var iosNotification = require("../app/iosVerification")
var Us = require("../app/controllers/users");
var Users = new Us();

var verifyReceipt = function (req, res, next)
{
    var self = res;

    let user = {};
    user.appleToken = req.body.reseipt;
    user.id = req.body.id;
    dbManager.dbqueryAsync(`select * FROM users where id='${req.body.id}';`)
        .then((data)=>{
        if (data.length > 0){
            let user = data[0];
            user.appleToken = req.body.reseipt;
            Users.verifyUser(user)
                .then((data)=>{
                if(data.isFree === 0){
                    self.send("ok");
                    return
                }else{
                    self.statusCode = 400;
                    self.send("false");
                    return
                }
            },(error)=>{
                next(error);
                return
            })
        }else{
            self.statusCode = 400;
            self.send("invalid Id");
            return
        }

    },(error)=>{
            next(error);
            return
        })

};


var verifyByid = function (req,res,next)
{

    var id = req.query.id;
    Users.getByIdDB(id,function (err,users) {
        if (err) {
            next(err);
            return
        }
        if (users.length == 0)
        {
            res.send("notFound")
            return
        }
        iosNotification.verify(users[0].appleToken,function (err,receipts,all) {
            if (err) {
                console.error(err);
                res.statusCode = 400;
                res.send(err);
                return
            }
            // ok!

                // console.log(id);
                res.send(all)
                return



        });

    })


};

module.exports = function(App,Passport,apicache) {
    var isProd= false;
    if (process.env.NODE_ENV=='production') {
        isProd = true;
    }
    var cache     = apicache.options({
        enabled: isProd
    }).middleware;
    app = App;
    passport = Passport;

    router.post('/verifyReceipt', verifyReceipt);

    router.get('/createarchive', createArchive);

    router.put('/getsequence', getSequence);

//Users

    console.log(Users);
    router.post('/users/add', function (req, res, next) {
        Users.add(req, res, next)
    });
    router.get('/users/adjustVariable', function (req, res, next) {
        Users.adjustVariable(req, res, next)
    });
    router.put('/users/get', function (req, res, next) {
        Users.getById(req, res, next)
    });
    router.get('/users/getAll',isLoggedIn, roleAuthorization([roles.admin]), function (req, res, next) {
        Users.getAll(req, res, next)
    });
    router.post('/users/remove',isLoggedIn, roleAuthorization([roles.admin]),function (req, res, next) {
        Users.remove(req, res, next)
    });

    router.post('/users/adjustData',helpers.asyncMiddleware( async function(req,res,next)  {
            var data = await Users.addAdjustData(req,res,next)
        })
    );

    router.post('/users/update',isLoggedIn, roleAuthorization([roles.admin]), function (req, res, next) {
        Users.update(req, res, next)
    });
    router.post('/users/setToken', function (req, res, next) {
        Users.setToken(req, res, next)
    });
    router.post('/users/updateActivity', function (req, res, next) {
        Users.updateActivity(req, res, next)
    });
    router.post('/users/register', function (req, res, next) {
        Users.register(req, res, next)
    });
    router.put('/users/getSettings', function (req, res, next) {
        Users.getSettings(req, res, next)
    });
    router.post('/users/verifyStatus', function (req, res, next) {
        Users.verifyStatus(req, res, next)
    });
    router.post('/users/updateUser', function (req, res, next) {
        Users.updateUser(req, res, next)
    });

    router.get('/users/verifyById',isLoggedIn, roleAuthorization([roles.admin]), function (req, res, next) {
        verifyByid(req, res, next)
    });

    router.post('/users/loadSettings', function (req, res, next) {
        Users.loadSettings(req, res, next)
    });

    router.get('/users/verifyAll',isLoggedIn,roleAuthorization([roles.admin]), helpers.asyncMiddleware( async function(req,res,next)  {
            var data = await Users.verifyAllUsers()
            console.log(data)
            res.send('free: ' +data.free + ' nonFree: '+ data.nonFree)
        })
    );
    //Progresses
    var pr =  require("../app/controllers/progresses");
    var Progrsses = new pr();
    router.post('/progresses/add', helpers.asyncMiddleware( async function(req,res,next)  {
        Progrsses.add(req, res, next)
    }));

//Stories
    var St = require("../app/controllers/stories");
    var Stories = new St();
    console.log(Stories);
    router.post('/stories/add', function (req, res, next) {
        Stories.add(req, res, next)
    });
    router.put('/stories/get', function (req, res, next) {
        Stories.getById(req, res, next)
    });

    router.post('/stories/like', function (req, res, next) {
        Stories.like(req, res, next)
    });

    router.post('/stories/rateStoryLike', function (req, res, next) {
        Stories.rateStoryLike(req, res, next)
    });

    router.post('/stories/rateStoryDislike', function (req, res, next) {
        Stories.rateStoryDislike(req, res, next)
    });

    router.post('/stories/remove',isLoggedIn,roleAuthorization([roles.admin,roles.writer]), function (req, res, next) {
        Stories.remove(req, res, next)
    });
    router.post('/stories/update',isLoggedIn,roleAuthorization([roles.admin,roles.writer]), function (req, res, next) {
        Stories.update(req, res, next)
    });
    router.get('/stories/getAll',cache('20 hour'), function (req, res, next) {
        Stories.getAll(req, res, next)
    });


//Episodes
    var Ep = require("../app/controllers/episodes");
    var Episodes = new Ep();
    console.log(Episodes);
    router.post('/episodes/add', function (req, res, next) {
        Episodes.add(req, res, next)
    });
    router.put('/episodes/get', function (req, res, next) {
        Episodes.getById(req, res, next)
    });
    router.post('/episodes/startedIos', function (req, res, next) {
        Episodes.startedIos(req, res, next)
    });
    router.post('/episodes/endedIos', function (req, res, next) {
        Episodes.endedIos(req, res, next)
    });

    router.get('/episodes/get',cache('21 hour'), function (req, res, next) {
        Episodes.getByIdGet(req, res, next)
    });

    router.get('/episodes/getAll',isLoggedIn,roleAuthorization([roles.admin,roles.writer,roles.reader]), function (req, res, next) {
        Episodes.getAll(req, res, next)
    });
    router.post('/episodes/remove',isLoggedIn,roleAuthorization([roles.admin,roles.writer]), function (req, res, next) {
        Episodes.remove(req, res, next)
    });
    router.post('/episodes/update',isLoggedIn,roleAuthorization([roles.admin,roles.writer]), function (req, res, next) {
        Episodes.update(req, res, next)
    });


//files
    var multer = require('multer');

    var Storage = multer.diskStorage({
        destination: function (req, file, callback) {
            callback(null, "./public/storage/");
        },
        filename: function (req, file, callback) {
            callback(null, Date.now() + "_" + file.originalname);
        }
    });

    var upload = multer({
        storage: Storage
    });


    router.post("/upload", upload.single('imgUploader'), function (req, res, next) {
        if (req.body.url) {

            let dir = path.resolve(__dirname, '../public/storage/');
            if (!fs.existsSync(dir)){
                fs.mkdirSync(dir);
            }
            var startImagePath = path.resolve(__dirname, '../public/storage/' + req.file.filename);

            var endImagePath = path.resolve(__dirname, '../public' + req.body.url);
            if (startImagePath != endImagePath) {
                helpers.copyImage(startImagePath, endImagePath);
            }
            var i = 0
        }

        res.send(req.file.filename)
    });


    return router;

}