var express = require('express');
var fs = require('fs');
var path = require('path')
var router = express.Router();
var passport;
var app;
var isLoggedIn = require("../app/helpers").isLoggedIn;


router.get('/',function(req,res,next){

    res.render('../views/index');

});

router.get('/terms.html',function(req,res,next){

    res.render('../views/terms');

});

router.get('/terms',function(req,res,next){

    res.render('../views/terms');

});

router.get('/support.html',function(req,res,next){

    res.render('../views/support');

});

router.get('/support',function(req,res,next){

    res.render('../views/support');

});

router.get('/privacy.html',function(req,res,next){

    res.render('../views/privacy');

});

router.get('/privacy',function(req,res,next){

    res.render('../views/privacy');

});

router.get('/storyconverter',function(req,res,next){

    res.render('../views/storyconverter',{title:"Story Converter"});

});


function getSiteAssociation(req,res,next){
    var filePath = path.join(__dirname, '../bin/apple-app-site-association.json');
    var stat = fs.statSync(filePath);

    res.writeHead(200, {
        //'Content-Type': 'application/pkcs7-mime',
        'Content-Length': stat.size
    });

    var readStream = fs.createReadStream(filePath);
    // We replaced all the event handlers with a simple call to readStream.pipe()
    readStream.pipe(res);
};
router.get("/.well-known/apple-app-site-association",function(req,res,next){
    getSiteAssociation(req,res,next)
});
router.get('/apple-app-site-association',function(req,res,next) {
    getSiteAssociation(req, res, next)
});


router.get('/subscribtion',function(req,res,next){

    res.render('../views/subscribtion',{title:"Subscribtion"});

});

router.get('/test',function(req,res,next){

    res.render('../views/test',{title:"test"});

});

router.get('/subscribtionru',function(req,res,next){

    res.render('../views/subscribtionru',{title:"Подписка"});

});
router.get('/redirect',function(req,res,next){

    res.render('../views/redirectPage.jade',{title:"Redirect",locale:"en"});

});

router.get('/redirectru',function(req,res,next){

    res.render('../views/redirectPage.jade',{title:"Redirect",locale:"ru"});

});

router.get('/contest',function(req,res,next){

    res.render('../views/contest.jade',{title:"Contest"});

});
module.exports = function(App,Passport)
{
    app = App;
    passport = Passport;
    return router;
};

