var express = require('express');
var router = express.Router();
var dbManager = require("../app/dbManager");
var standartRequest = require("../app/standartRequest");
var isLoggedIn = require("../app/helpers").isLoggedIn;
var roleAuthorization = require("../app/helpers").roleAuthorization
var passport ={};
var app = {};
var roles = require("../app/roles");
module.exports = function(App,Passport,apicache)
{
    app = App;
    passport = Passport;
    var isProd= false;
    if (process.env.NODE_ENV=='production') {
        isProd = true;
    }
    var cache     = apicache.options({
        enabled: isProd
    }).middleware;
    var Ps = require("../app/controllers/pushes");
    var Pushes = new Ps();

    router.get('/',isLoggedIn,roleAuthorization([roles.admin]), function(req,res,next){
        try {
            Pushes.getAllDB(function (err, resp) {
                if (err !== null) {
                    (standartRequest(self, null, true, err));
                    return
                }

                res.render('pushes', {title: "Pushes", pushes: resp ,user : req.user});
                return;

            })

        }catch (err){
            next(err)
        }
    });
    //isLoggedIn,roleAuthorization([roles.admin])
    router.post('/add',isLoggedIn,roleAuthorization([roles.admin]), function (req, res, next) {
        Pushes.add(req, res, next)
    });
    router.post('/update',isLoggedIn,roleAuthorization([roles.admin]), function (req, res, next) {
        Pushes.update(req, res, next)
    });
    router.post('/remove',isLoggedIn,roleAuthorization([roles.admin]), function (req, res, next) {
        Pushes.remove(req, res, next)
    });

    return router;
};
